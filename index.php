<?php

require(dirname(__FILE__) . '/php/config-login.php');



if(isset($_SESSION['UserLoggedIn']['Company_ID']) && isset($_SESSION['UserLoggedIn']['User_ID'])) {

    redirect(BASE_URL.'playbooks');

}



$page = 'Login';

require(dirname(__FILE__) . '/inc/html-header.php'); ?>



  <div class="page login-page">

    <div class="container d-flex align-items-center">

      <div class="form-holder has-shadow">

        <div class="row">

          <div class="col-lg-6">

            <div class="info d-flex align-items-center">

              <div class="content">

                <div class="logo">

                  <h1>Tactical Planning Cycle</h1>

                </div>

              </div>

            </div>

          </div>

          <div class="col-lg-6 bg-white">

            <div class="form d-flex align-items-center">

              <div class="content">

                <form id="login-form" method="post" action="<?php echo BASE_URL; ?>php/CompanyLoginProcess">

                  <div class="form-group">

                    <input id="login-username" type="email" name="username" required="" class="input-material">

                    <label for="login-username" class="label-material">User Name or Email</label>

                  </div>

                  <div class="form-group">

                    <input id="login-password" type="password" name="password" required="" class="input-material">

                    <label for="login-password" class="label-material">Password</label>

                  </div>

                  <button type="submit" id="login" class="btn btn-primary">Login</button>

                </form>

                <a href="<?php echo BASE_URL; ?>forgot" class="forgot-pass">Forgot Password?</a>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="copyrights text-center">

      <p>Design by <a href="<?php echo BASE_URL; ?>" class="external">Sealed Mindset</a></p>

    </div>

  </div>



<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>