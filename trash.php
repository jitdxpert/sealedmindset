<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID']) && ($_SESSION['UserLoggedIn']['User_Type'] == 'request')) {
  redirect(BASE_URL);
}
$page = 'Users';
require(dirname(__FILE__) . '/inc/html-header.php');
?>
<div class="page">
  <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
  <div class="page-content d-flex align-items-stretch">
    <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
    <div class="content-inner">
      <header class="page-header">
        <div class="container-fluid">
          <h2 class="no-margin-bottom">Department / User</h2>
        </div>
      </header>
      <?php if($_SESSION['UserLoggedIn']['User_Type']=='company'){ ?>
        <section class="companies">
          <div class="container-fluid">
            <div class="row">
              <?php
              $Company = DB::table('companies')
              ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
              ->first();

              $Users = DB::table('users')
              ->where('Trash', '=', 1)
              ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
              ->where('User_Email', '!=', $Company->Company_Email)
              ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
              ->where('User_Type', '!=', 'request')
              ->count();
              if(!empty($Users)) {
                // Pagination
                $paginationObj = new Pagination;
                $targetpage = 'users';
                $total_pages = $Users;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                $UsersFetch = DB::table('users')
                ->where('Trash', '=', 1)
                ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
                ->where('User_Email', '!=', $Company->Company_Email)
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where('User_Type', '!=', 'request')
                ->offset($start)
                ->limit($limit)
                ->orderBy('User_ID', 'DESC')
                ->get();
                foreach($UsersFetch as $User) {
                  if($User->User_Type == 'department') {
                    $actionModal = '#TrashDepartmentModal';
                    $actionClass = 'TrashDepartment';
                    $restoreClass = 'RestoreDepartment';
                    $restoreModal = '#RestoreDepartmentModal';
                  } elseif($User->User_Type == 'user') {
                    $actionModal = '#TrashUserModal';
                    $actionClass = 'TrashUser';
                    $restoreClass = 'RestoreUser';
                    $restoreModal = '#RestoreUserModal';
                  }
                  ?>
                  <div class="col-lg-3">
                    <div class="daily-feeds card">
                      <div class="card-header d-flex align-items-center">
                        <h4 class="h4">
                          <?php echo $User->User_Name; ?>
                          <button class="btnEdit <?php echo $restoreClass; ?>" data-toggle="modal" data-target=<?php echo $restoreModal; ?> data-id="<?php echo $User->User_ID; ?>" title="Restore User / Department" style="right:0;">
                            <i class="fa fa-undo"></i>
                          </button>
                          <!-- <button class="btnTrash <?php //echo $actionClass; ?>" data-toggle="modal" data-target=<?php //echo $actionModal; ?> data-id="<?php //echo $User->User_ID; ?>" title="Remove User / Department">
                          <i class="fa fa-trash"></i>
                        </button> -->
                      </h4>
                    </div>
                    <div class="card-body no-padding">
                      <div class="item">
                        <div class="feed d-flex justify-content-between">
                          <div class="feed-body d-flex justify-content-between">
                            <a class="feed-profile"><img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($User->User_Email))) . "?s=55"; ?>" alt="<?php echo $User->User_Name; ?>" class="img-fluid rounded-circle"></a>
                            <div class="content">
                              <h5><?php echo $User->User_Name; ?></h5>
                              <span><i class="fa fa-envelope"></i> <?php echo $User->User_Email; ?></span>
                              <span>
                                <i class="fa fa-user"></i>
                                <?php
                                if($User->User_Type == 'department') {
                                  echo 'Department';
                                } elseif($User->User_Type == 'user') {
                                  echo 'User';
                                }
                                ?>
                              </span>
                              <div class="full-date">
                                <small>
                                  <i class="fa fa-calendar"></i>
                                  <?php echo date('jS F, Y \a\t g:i A', strtotime($User->User_CreatedOn)); ?>
                                </small>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
            <!-- ........................PAGINATION.............................. -->
            <div style="margin-top:60px"> <?=$pagination?></div>
            <!-- ........................PAGINATION.............................. -->
          <?php } else { ?>
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">No Users / Department are available.</div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </section>
    <?php	} ?>

    <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
  </div>
</div>
</div>


<div class="modal center fade" id="TrashUserModal" tabindex="-1" role="dialog" aria-labelledby="TrashUserModalLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <form id="delete-user-form" method="post" action="<?php echo BASE_URL; ?>php/DeleteUser">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Remove User</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you would like to parmanently remove this user?</p>
        </div>
        <div class="modal-footer">
          <div class="col">
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
          </div>
          <div class="col">
            <button type="submit" class="btn btn-primary btn-lg" id="RemoveUser">YES, REMOVE IT</button>
          </div>
          <input type="hidden" name="removeUserID" id="removeUserID" />
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal center fade" id="TrashDepartmentModal" tabindex="-1" role="dialog" aria-labelledby="TrashUserModalLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <form id="delete-department-form" method="post" action="<?php echo BASE_URL; ?>php/DeleteDepartment">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Remove Department</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you would like to parmanently remove this Department?</p>
        </div>
        <div class="modal-footer">
          <div class="col">
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
          </div>
          <div class="col">
            <button type="submit" class="btn btn-primary btn-lg" id="RemoveUser">YES, REMOVE IT</button>
          </div>
          <input type="hidden" name="removeDepartmentID" id="removeDepartmentID" />
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal center fade" id="RestoreUserModal" tabindex="-1" role="dialog" aria-labelledby="RestoreUserModalLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <form id="restore-user-form" method="post" action="<?php echo BASE_URL; ?>php/RestoreUser">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Restore User</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you would like to restore this user?</p>
        </div>
        <div class="modal-footer">
          <div class="col">
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
          </div>
          <div class="col">
            <button type="submit" class="btn btn-primary btn-lg" id="RemoveUser">YES, RESTORE IT</button>
          </div>
          <input type="hidden" name="restoreUserID" id="restoreUserID" />
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal center fade" id="RestoreDepartmentModal" tabindex="-1" role="dialog" aria-labelledby="RestoreDepartmentModalLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <form id="restore-department-form" method="post" action="<?php echo BASE_URL; ?>php/RestoreDepartment">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Restore Department</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you would like to restore this Department?</p>
        </div>
        <div class="modal-footer">
          <div class="col">
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
          </div>
          <div class="col">
            <button type="submit" class="btn btn-primary btn-lg" id="RemoveUser">YES, RESTORE IT</button>
          </div>
          <input type="hidden" name="restoreDepartmentID" id="restoreDepartmentID" />
        </div>
      </div>
    </form>
  </div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>
