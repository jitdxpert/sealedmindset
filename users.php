<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID']) && ($_SESSION['UserLoggedIn']['User_Type'] == 'request')) {
  redirect(BASE_URL);
}
$page = 'Users';
require(dirname(__FILE__) . '/inc/html-header.php');
?>
<div class="page">
  <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
  <div class="page-content d-flex align-items-stretch">
    <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
    <div class="content-inner">
      <div class="loader hide"></div>
      <header class="page-header">
        <div class="container-fluid">
          <h2 class="no-margin-bottom"><?php if($_SESSION['UserLoggedIn']['User_Type'] == 'individual' || $_SESSION['UserLoggedIn']['User_Type'] == 'user'){ ?>Contributor<?php } elseif($_SESSION['UserLoggedIn']['User_Type'] == 'department'){ ?>User / Contributor<?php } elseif($_SESSION['UserLoggedIn']['User_Type'] == 'company'){ ?>Department / User <?php }?>
            <?php if($_SESSION['UserLoggedIn']['User_Type'] != 'request') {?>
              <div class="newPlaybook">
                <a data-toggle="modal" data-target="#NewUserModal"><i class="fa fa-plus"></i></a>
              </div>
            <?php } ?>
          </h2>
        </div>
      </header>
      <?php if($_SESSION['UserLoggedIn']['User_Type']=='individual'){ ?>
        <section class="companies">
          <div class="container-fluid">
            <div class="row">
              <?php
              $Users = DB::table('users')
              ->where('Trash', '=', 0)
              ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
              ->where('Master_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])
              ->count();
              if(!empty($Users)) {
                // Pagination
                $paginationObj = new Pagination;
                $targetpage = 'users';
                $total_pages = $Users;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                $UsersFetch = DB::table('users')
                ->where('Trash', '=', 0)
                ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
                ->where('Master_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])
                ->orderBy('User_ID', 'DESC')
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($UsersFetch as $User) { ?>
                  <div class="col-lg-3">
                    <div class="daily-feeds card">
                      <div class="card-header d-flex align-items-center">
                        <h4 class="h4">
                          <?php echo $User->User_Name; ?>
                          <!-- <button class="btnTrash TrashUser" data-toggle="modal" data-target="#TrashUserModal" data-id="<?php // echo $User->User_ID; ?>" title="Remove User">
                            <i class="fa fa-trash"></i>
                          </button> -->
                          <button class="btnEdit EditUser" data-toggle="modal" data-target="#EditUserModal" data-id="<?php echo $User->User_ID; ?>" data-name="<?php echo $User->User_Name; ?>" data-type="<?php echo $User->User_Type; ?>" title="Edit User" style="right:0">
                            <i class="fa fa-pencil"></i>
                          </button>
                        </h4>
                      </div>
                      <div class="card-body no-padding">
                        <div class="item">
                          <div class="feed d-flex justify-content-between">
                            <div class="feed-body d-flex justify-content-between">
                              <a class="feed-profile"><img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($User->User_Email))) . "?s=55"; ?>" alt="<?php echo $User->User_Name; ?>" class="img-fluid rounded-circle"></a>
                              <div class="content">
                                <h5><?php echo $User->User_Name; ?></h5>
                                <span><i class="fa fa-envelope"></i> <?php echo $User->User_Email; ?></span>
                                <span>
                                  <i class="fa fa-user"></i>
                                  Contributor
                                </span>
                                <div class="full-date">
                                  <small>
                                    <i class="fa fa-calendar"></i>
                                    <?php echo date('jS F, Y \a\t g:i A', strtotime($User->User_CreatedOn)); ?>
                                  </small>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <!-- ........................PAGINATION.............................. -->
              <div style="margin-top:60px"> <?=$pagination?></div>
              <!-- ........................PAGINATION.............................. -->
            <?php } else { ?>
              <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">No Users are available.</div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </section>
      <?php } else {
        if($_SESSION['UserLoggedIn']['User_Type']=='company'){ ?>
          <section class="companies">
            <div class="container-fluid">
              <div class="row">
                <?php
                $Company = DB::table('companies')
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->first();

                $Users = DB::table('users')
                ->where('Trash', '=', 0)
                ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
                ->where('User_Email', '!=', $Company->Company_Email)
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where('User_Type', '!=', 'request')
                ->count();
                if(!empty($Users)) {
                  // Pagination
                  $paginationObj = new Pagination;
                  $targetpage = 'users';
                  $total_pages = $Users;
                  $limit = 10;
                  $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                  $pagination = $paginationOutput['Pagination'];
                  $start = $paginationOutput['Start'];
                  // Pagination
                  $UsersFetch = DB::table('users')
                  ->where('Trash', '=', 0)
                  ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
                  ->where('User_Email', '!=', $Company->Company_Email)
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->where('User_Type', '!=', 'request')
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy('User_ID', 'DESC')
                  ->get();
                  foreach($UsersFetch as $User) { ?>
                    <div class="col-lg-3">
                      <div class="daily-feeds card">
                        <div class="card-header d-flex align-items-center">
                          <h4 class="h4">
                            <?php echo $User->User_Name; ?>
                            <button class="btnEdit EditUser" data-toggle="modal" data-target="#EditUserModal" data-id="<?php echo $User->User_ID; ?>" data-name="<?php echo $User->User_Name; ?>" data-type="<?php echo $User->User_Type; ?>" title="Edit User">
                              <i class="fa fa-pencil"></i>
                            </button>
                            <button class="btnTrash TrashUser" data-toggle="modal" data-target="#TrashUserModal" data-id="<?php echo $User->User_ID; ?>" title="Trash User">
                              <i class="fa fa-user-times"></i>
                            </button>
                          </h4>
                        </div>
                        <div class="card-body no-padding">
                          <div class="item">
                            <div class="feed d-flex justify-content-between">
                              <div class="feed-body d-flex justify-content-between">
                                <a class="feed-profile"><img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($User->User_Email))) . "?s=55"; ?>" alt="<?php echo $User->User_Name; ?>" class="img-fluid rounded-circle"></a>
                                <div class="content">
                                  <h5><?php echo $User->User_Name; ?></h5>
                                  <span><i class="fa fa-envelope"></i> <?php echo $User->User_Email; ?></span>
                                  <span>
                                    <i class="fa fa-user"></i>
                                    <?php
                                    if($User->User_Type == 'department') {
                                      echo 'Department';
                                    } elseif($User->User_Type == 'user') {
                                      echo 'User';
                                    }
                                    ?>
                                  </span>
                                  <div class="full-date">
                                    <small>
                                      <i class="fa fa-calendar"></i>
                                      <?php echo date('jS F, Y \a\t g:i A', strtotime($User->User_CreatedOn)); ?>
                                    </small>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                </div>
                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:60px"> <?=$pagination?></div>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No Users are available.</div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </section>
        <?php	} elseif($_SESSION['UserLoggedIn']['User_Type']=='department' || $_SESSION['UserLoggedIn']['User_Type']=='user'){ ?>
          <section class="companies">
            <div class="container-fluid">
              <div class="row">
                <?php
                $Users = DB::table('users')
                ->where('Trash', '=', 0)
                ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
                ->where('Master_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->count();
                if(!empty($Users)) {
                  // Pagination
                  $paginationObj = new Pagination;
                  $targetpage = 'users';
                  $total_pages = $Users;
                  $limit = 10;
                  $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                  $pagination = $paginationOutput['Pagination'];
                  $start = $paginationOutput['Start'];
                  // Pagination
                  $UsersFetch = DB::table('users')
                  ->where('Trash', '=', 0)
                  ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
                  ->where('Master_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy('User_ID', 'DESC')
                  ->get();
                  foreach($UsersFetch as $User) { ?>
                    <div class="col-lg-3">
                      <div class="daily-feeds card">
                        <div class="card-header d-flex align-items-center">
                          <h4 class="h4">
                            <?php echo $User->User_Name; ?>
                            <!-- <button class="btnTrash TrashUser" data-toggle="modal" data-target="#TrashUserModal" data-id="<?php //echo $User->User_ID; ?>" title="Remove User">
                              <i class="fa fa-trash"></i>
                            </button> -->
                            <button class="btnEdit EditUser" data-toggle="modal" data-target="#EditUserModal" data-id="<?php echo $User->User_ID; ?>" data-name="<?php echo $User->User_Name; ?>" data-type="<?php echo $User->User_Type; ?>" title="Edit User" style="right:0px;">
                              <i class="fa fa-pencil"></i>
                            </button>
                          </h4>
                        </div>
                        <div class="card-body no-padding">
                          <div class="item">
                            <div class="feed d-flex justify-content-between">
                              <div class="feed-body d-flex justify-content-between">
                                <a class="feed-profile"><img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($User->User_Email))) . "?s=55"; ?>" alt="<?php echo $User->User_Name; ?>" class="img-fluid rounded-circle"></a>
                                <div class="content">
                                  <h5><?php echo $User->User_Name; ?></h5>
                                  <span><i class="fa fa-envelope"></i> <?php echo $User->User_Email; ?></span>
                                  <span>
                                    <i class="fa fa-user"></i>
                                    <?php
                                    if($User->User_Type == 'department') {
                                      echo 'Department';
                                    } elseif($User->User_Type == 'user') {
                                      echo 'User';
                                    }elseif($User->User_Type == 'request') {
                                      echo 'Contributor';
                                    }
                                    ?>
                                  </span>
                                  <div class="full-date">
                                    <small>
                                      <i class="fa fa-calendar"></i>
                                      <?php echo date('jS F, Y \a\t g:i A', strtotime($User->User_CreatedOn)); ?>
                                    </small>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                </div>
                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:60px"> <?=$pagination?></div>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No Users are available.</div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </section>
        <?php	}
      } ?>

      <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
    </div>
  </div>
</div>

<div class="modal right fade" id="NewUserModal" tabindex="-1" role="dialog" aria-labelledby="NewUserModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php
        if($_SESSION['UserLoggedIn']['User_Type']=='company'){
          $title = 'New Department / User';
          $options = '<option value="department">Department</option><option value="user">User</option>';
        } elseif($_SESSION['UserLoggedIn']['User_Type']=='department'){
          $title = 'New User / Contributor';
          $options = '<option value="user">User</option><option value="request">Contributor</option>';
        } elseif($_SESSION['UserLoggedIn']['User_Type']=='user' || $_SESSION['UserLoggedIn']['User_Type']=='individual'){
          $title = 'New Contributor';
          $options = '<option value="request">Contributor</option>';
        }
        ?>
        <h5 class="modal-title" id="RequestModalLabel"><?php echo $title; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="user-form" method="post" action="<?php echo BASE_URL; ?>php/NewUserProcess" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Name<span class="text-danger">*</span></label>
            <input type="text" placeholder="Name" name="memberName" id="memberName" class="form-control" required />
          </div>
          <div class="form-group">
            <label class="form-control-label">Email<span class="text-danger">*</span></label>
            <input type="email" placeholder="Email" name="memberEmail" id="memberEmail" class="form-control" required />
          </div>
          <div class="form-group">
            <label class="form-control-label">Type<span class="text-danger">*</span></label>
            <select name="memberType" id="memberType" class="form-control" style="height:45px;" required>
              <option value="">Select Type</option>
              <?php echo $options; ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary requestButton">ADD</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal center fade" id="TrashUserModal" tabindex="-1" role="dialog" aria-labelledby="TrashUserModalLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <form id="remove-user-form" method="post" action="<?php echo BASE_URL; ?>php/RemoveUser">
      <div class="modal-content">
        <div class="modal-header">
          <?php
          if($_SESSION['UserLoggedIn']['User_Type']=='company'){
            $trashTitle = 'Trash Department / User';
            $trashSubTitle = 'Department / User';
          } elseif($_SESSION['UserLoggedIn']['User_Type']=='department'){
            $trashTitle = 'Trash User / Contributor';
            $trashSubTitle = 'User / Contributor';
          } elseif($_SESSION['UserLoggedIn']['User_Type']=='user' || $_SESSION['UserLoggedIn']['User_Type']=='individual'){
            $trashTitle = 'Trash Contributor';
            $trashSubTitle = 'Contributor';
          }
          ?>
          <h4 class="modal-title"><?php echo $trashTitle; ?></h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you would like to move this <?php echo $trashSubTitle; ?> to trash?</p>
        </div>
        <div class="modal-footer">
          <div class="col">
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
          </div>
          <div class="col">
            <button type="submit" class="btn btn-primary btn-lg" id="RemoveUser">YES, TRASH IT</button>
          </div>
          <input type="hidden" name="removeUserID" id="removeUserID" />
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal right fade" id="EditUserModal" tabindex="-1" role="dialog" aria-labelledby="EditUserModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php
        if($_SESSION['UserLoggedIn']['User_Type']=='company'){
          $Edittitle = 'Edit Department / User';
        } elseif($_SESSION['UserLoggedIn']['User_Type']=='department'){
          $Edittitle = 'Edit User / Contributor';
        } elseif($_SESSION['UserLoggedIn']['User_Type']=='user' || $_SESSION['UserLoggedIn']['User_Type']=='individual'){
          $Edittitle = 'Edit Contributor';
        }
        ?>
        <h5 class="modal-title" id="RequestModalLabel"><?php echo $Edittitle; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="edit-user-form" method="post" action="<?php echo BASE_URL; ?>php/EditUserProcess" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Full Name<span class="text-danger">*</span></label>
            <input type="text" placeholder="Name" name="editMemberName" id="editMemberName" class="form-control" required />
          </div>
          <!-- <div class="form-group">
          <label class="form-control-label">Type<span class="text-danger">*</span></label>
          <select name="editMemberType" id="editMemberType" class="form-control" style="height:45px;" required>
          <option value="">Select Type</option>
          <option value="department">Department</option>
          <option value="user">User</option>
        </select>
      </div> -->
      <?php if($_SESSION['UserLoggedIn']['User_Type']=='company'){ ?>
        <div class="form-group">
          <label class="form-control-label">New Password </label>
          <input type="password" placeholder="Password" name="editMemberPassword" id="editMemberPassword" class="form-control" />
        </div>
      <?php } ?>
    </div>
    <div class="modal-footer">
      <input type="hidden" name="editMemberID" id="editMemberID" />
      <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
      <button type="submit" class="btn btn-primary editButton">UPDATE</button>
    </div>
  </form>
</div>
</div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>
