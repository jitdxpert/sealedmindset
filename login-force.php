<?php
require(dirname(__FILE__) . '/php/config-login.php');

if(isset($_SESSION['UserLoggedIn']['Company_ID']) && isset($_SESSION['UserLoggedIn']['User_ID'])) {
    redirect(BASE_URL.'playbooks');
}

if(!$_GET['id']) {
    redirect(BASE_URL);
}

$user = DB::table('users')->where('User_ID', '=', $_GET['id'])->first();
if(!$user) {
    echo 'No user exists.';
    exit;
}

$company = DB::table('companies')->where('Company_ID', '=', $user->Company_ID)->first();
if(!$company) {
    echo 'No company exists.';
    exit;
} else {
    $_SESSION['UserLoggedIn'] = [
        'Company_ID'   => isset($company->Company_ID) ? $company->Company_ID : '',
        'Company_Name' => isset($company->Company_Name) ? $company->Company_Name : '',
        'User_ID'      => $user->User_ID,
        'User_Email'   => $user->User_Email,
        'User_Name'    => $user->User_Name,
        'User_Type'    => $user->User_Type
    ];
    redirect(BASE_URL.'playbooks');
}