<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID'])) {
    redirect(BASE_URL);
}

$page = 'Settings';
require(dirname(__FILE__) . '/inc/html-header.php');
?>

<div class="page">
    <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
    <div class="page-content d-flex align-items-stretch">
        <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
        <div class="content-inner">
            <ul class="breadcrumb">
                <div class="container-fluid">
                    <li class="breadcrumb-item"><a href="<?php echo BASE_URL; ?>">Home</a></li>
                    <li class="breadcrumb-item active">Settings</li>
                </div>
            </ul>
            <section class="forms"> 
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header d-flex align-items-center">
                                    <h3 class="h4">Change Password</h3>
                                </div>
                                <div class="card-body">
                                    <form class="form-horizontal" id="change-password-form" method="post" action="<?php echo BASE_URL; ?>php/ChangePassword">
                                        <div class="form-group row">
                                            <label class="col-sm-4 form-control-label">Old Password</label>
                                            <div class="col-sm-8">
                                                <input id="old_password" name="old_password" type="password" placeholder="Old Password" class="form-control form-control-success" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 form-control-label">New Password</label>
                                            <div class="col-sm-8">
                                                <input id="new_password" name="new_password" type="password" placeholder="New Password" class="form-control form-control-warning" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-4 form-control-label">Confirm Password</label>
                                            <div class="col-sm-8">
                                                <input id="con_password" name="con_password" type="password" placeholder="Confirm Password" class="form-control form-control-warning" required="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-8 offset-sm-4">
                                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['UserLoggedIn']['User_ID']; ?>" />
                                                <button type="submit" id="update_password" name="update_password" class="btn btn-primary">UPDATE</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>        
        <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
        </div>
    </div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>