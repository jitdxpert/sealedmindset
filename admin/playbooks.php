<?php require(dirname(dirname(__FILE__)) . '/php/config.php'); ?>
<?php
if ( !isset($_SESSION['Admin_ID']) ) {
  header('location: ' . ADMIN_URL);
} ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Playbooks - Sealed Mindset</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap-datepicker.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.default.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/modal.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/custom.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/icons.css" />
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="page">
    <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
    <div class="page-content d-flex align-items-stretch">
      <?php
        require(dirname(__FILE__) . '/inc/navbar.php');
        if(isset($_GET["q"])){
          $q = $_GET["q"];
        } else {
          $q= '';
        }
      ?>
      <div class="content-inner">
        <ul class="breadcrumb">
          <div class="container-fluid">
            <li class="breadcrumb-item"><a href="<?php echo ADMIN_URL; ?>">Home</a></li>
            <li class="breadcrumb-item active">Playbooks</li>
          </div>
        </ul>
        <section class="playbooks">
          <div class="container-fluid">
            <?php
            $ProjectSQL = DB::table('projects')
            ->where('Trash', '=', 0)
            ->where(function($searchquery) use($q){
              $searchquery->where('Project_Name', 'like', '%'.$q.'%');
            })
            ->orderBy('Project_ID', 'DESC')
            ->count();
            if(!empty($ProjectSQL)) {
              // Pagination
              $paginationObj = new Pagination;
              if($q){
                $targetpage = 'playbooks?q='.$q;
              } else {
                $targetpage = 'playbooks';
              }
              $total_pages = $ProjectSQL;
              $limit = 10;
              $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
              $pagination = $paginationOutput['Pagination'];
              $start = $paginationOutput['Start'];
              // Pagination
              $ProjectFetchSQL = DB::table('projects')
              ->where('Trash', '=', 0)
              ->where(function($searchquery) use($q){
                $searchquery->where('Project_Name', 'like', '%'.$q.'%');
              })
              ->orderBy('Project_ID', 'DESC')
              ->offset($start)
              ->limit($limit)
              ->get();
              foreach($ProjectFetchSQL as $Project) { ?>
                <div class="parent">
                  <div class="child-row">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="articles card left">
                          <div class="card-close">
                            <?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?>
                            <!-- <span class="pull-right">
                            <a data-toggle="modal" data-target="#PlayRequestModal" class="playRequestBtn" title="Share Playbook" data-project="<?php //echo $Project->Project_ID; ?>">
                            <i class="fa fa-share-alt"></i>
                          </a>
                        </span> -->
                      </div>
                      <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                      <div class="card-header d-flex">
                        <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                      </div>
                      <div class="card-body no-padding">
                        <div class="item d-flex project-goal">
                          <div class="text">
                            <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                          </div>
                        </div>
                        <?php echo GetShared($Project->Project_ID); ?>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="col-md-3">
                  <div class="articles card right">
                  <div class="card-body no-padding">
                  <?php //echo GetAllTabFull($Project->Project_ID); ?>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
      <?php
    } ?>
    <!-- ........................PAGINATION.............................. -->
    <div style="margin-top:60px"> <?=$pagination?></div>
    <!-- ........................PAGINATION.............................. -->
    <?php
  } else { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">No projects are available.</div>
        </div>
      </div>
    </div>
    <?php
  }	?>
</div>
</section>
<?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
</div>
</div>
</div>

<div class="modal right fade" id="PlayRequestModal" tabindex="-1" role="dialog" aria-labelledby="PlayRequestModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="PlayRequestModalLabel">Send Playbook Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="play-request-form" method="post" action="<?php echo BASE_URL; ?>php/PlaybookRequest" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Editor Name<span class="text-danger">*</span></label>
            <input type="text" placeholder="Member Name" name="memberName" id="memberName" class="form-control" required />
          </div>
          <div class="form-group">
            <label class="form-control-label">Editor Email<span class="text-danger">*</span></label>
            <input type="email" placeholder="Member Email" name="memberEmail" id="memberEmail" class="form-control" required />
          </div>
          <input type="hidden" id="project" name="project" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary requestButton">SEND REQUEST</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="<?php echo BASE_URL; ?>js/jquery.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/tether.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/bootstrap-notify.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/jquery.validate.js"></script>
<script src="<?php echo BASE_URL; ?>js/front.js"></script>
</body>
</html>
