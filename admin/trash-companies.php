<?php require(dirname(dirname(__FILE__)) . '/php/config.php'); ?>

<?php

if ( !isset($_SESSION['Admin_ID']) ) {

  header('location: ' . ADMIN_URL);

} ?>


<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8" />

  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <title>Playbooks - Sealed Mindset</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap-datepicker.min.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/font-awesome.min.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.default.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/modal.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/custom.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/icons.css" />

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->

</head>

<body>
  <div class="page">
    <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
    <div class="page-content d-flex align-items-stretch">
      <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
      <div class="content-inner">
        <ul class="breadcrumb">
          <div class="container-fluid">
            <li class="breadcrumb-item"><a href="<?php echo ADMIN_URL; ?>">Home</a></li>
            <li class="breadcrumb-item active">Companies</li>
          </div>
        </ul>
        <section class="companies">
          <div class="container-fluid">
            <div class="row">
              <?php
              $CompanySQL = DB::table('companies')
              ->where('Trash', '=', 1)
              ->orderBy('Company_ID', 'DESC')
              ->count();
              if(!empty($CompanySQL)) {
                // Pagination
                $paginationObj = new Pagination;
                $targetpage = 'companies';
                $total_pages = $CompanySQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                $CompanyFetchSQL = DB::table('companies')
                ->where('Trash', '=', 1)
                ->orderBy('Company_ID', 'DESC')
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($CompanyFetchSQL as $Company) {
                  $User = DB::table('users')
                  ->where('Trash', 1)
                  ->where('User_Email', $Company->Company_Email)
                  ->where('Company_ID', $Company->Company_ID)
                  ->where('User_Type', '=', 'company')
                  ->first();
                  ?>
                  <div class="col-lg-3">
                    <div class="daily-feeds card">
                      <div class="card-header d-flex align-items-center">
                        <h4 class="h4">
                          <?php echo $Company->Company_Name; ?>
                          <button class="btnEdit RestoreCompany" data-toggle="modal" data-target="#RestoreCompanyModal" data-id="<?php echo $Company->Company_ID; ?>" title="Restore Company" style="right:0">
                            <i class="fa fa-undo"></i>
                          </button>
                          <!-- <button class="btnTrash RemoveCompany" data-toggle="modal" data-target="#TrashCompanyModal" data-id="<?php //echo $Company->Company_ID; ?>" title="Remove Company">
                            <i class="fa fa-trash"></i>
                          </button> -->
                        </h4>
                      </div>
                      <div class="card-body no-padding">
                        <div class="item">
                          <div class="feed d-flex justify-content-between">
                            <div class="feed-body d-flex justify-content-between">
                              <a class="feed-profile"><img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($Company->Company_Email))) . "?s=55"; ?>" alt="<?php echo $Company->Company_Owner; ?>" class="img-fluid rounded-circle"></a>
                              <div class="content">
                                <h5><?php echo $Company->Company_Owner; ?></h5>
                                <span><i class="fa fa-envelope"></i> <?php echo $Company->Company_Email; ?></span>
                                <div class="full-date">
                                  <small>
                                    <i class="fa fa-calendar"></i>
                                    <?php echo date('jS F, Y \a\t g:i A', strtotime($Company->Company_CreatedOn)); ?>
                                  </small>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <!-- ........................PAGINATION.............................. -->
              <div style="margin-top:60px"> <?=$pagination?></div>
              <!-- ........................PAGINATION.............................. -->
            <?php } else { ?>

              <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">No companies are available.</div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </section>
        <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
      </div>
    </div>
  </div>



  <div class="modal center fade" id="TrashCompanyModal" tabindex="-1" role="dialog" aria-labelledby="TrashCompanyModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <form id="delete-company-form" method="post" action="<?php echo BASE_URL; ?>php/DeleteCompany">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Remove Company</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you would like to remove this Company?</p>
          </div>
          <div class="modal-footer">
            <div class="col">
              <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
            </div>
            <div class="col">
              <button type="submit" class="btn btn-primary btn-lg" id="RemoveCompany">YES, REMOVE IT</button>
            </div>
            <input type="hidden" name="deleteCompanyID" id="deleteCompanyID" />
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="modal center fade" id="RestoreCompanyModal" tabindex="-1" role="dialog" aria-labelledby="TrashCompanyModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <form id="restore-company-form" method="post" action="<?php echo BASE_URL; ?>php/RestoreCompany">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Restore Company</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you would like to restore this Company?</p>
          </div>
          <div class="modal-footer">
            <div class="col">
              <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
            </div>
            <div class="col">
              <button type="submit" class="btn btn-primary btn-lg" id="RemoveCompany">YES, RESTORE IT</button>
            </div>
            <input type="hidden" name="restoreCompanyID" id="restoreCompanyID" />
          </div>
        </div>
      </form>
    </div>
  </div>


  <script src="<?php echo BASE_URL; ?>js/jquery.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/tether.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap-notify.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/jquery.validate.js"></script>
  <script src="<?php echo BASE_URL; ?>js/front.js"></script>

</body>
</html>
