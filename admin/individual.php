<?php require(dirname(dirname(__FILE__)) . '/php/config.php'); ?>
<?php
if ( !isset($_SESSION['Admin_ID']) ) {
  header('location: ' . ADMIN_URL);
} ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Playbooks - Sealed Mindset</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap-datepicker.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.default.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/modal.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/custom.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/icons.css" />
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="page">
    <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
    <div class="page-content d-flex align-items-stretch">
    <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
      <div class="content-inner">
        <ul class="breadcrumb">
          <div class="container-fluid">
            <li class="breadcrumb-item"><a href="<?php echo ADMIN_URL; ?>">Home</a></li>
            <li class="breadcrumb-item active">Individual Users</li>
          </div>
        </ul>
        <section class="companies">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">
                    <a class="newCompany" data-toggle="modal" data-target="#IndividualModal">
                      <i class="fa fa-plus"></i> Add New Individual User
                    </a>
                  </div>
                </div>
              </div>
              <div class="loader hide"></div>
              <?php
              $UserSQL = DB::table('users')
              ->where('Trash', '=', 0)
              ->where('User_Type', '=', 'individual')
              ->count();
              if(!empty($UserSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                $targetpage = 'individual';
                $total_pages = $UserSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                $UserFetchSQL = DB::table('users')
                ->where('Trash', '=', 0)
                ->where('User_Type', '=', 'individual')
                ->orderBy('User_ID', 'DESC')
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($UserFetchSQL as $User) {
                  ?>
                  <div class="col-lg-3">
                    <div class="daily-feeds card">
                      <div class="card-header d-flex align-items-center">
                        <h4 class="h4">
                          <?php echo $User->User_Name; ?>
                          <button class="btnEdit EditIndividual" data-toggle="modal" data-target="#EditIndividualModal" data-uid="<?php echo $User->User_ID; ?>" data-name="<?php echo $User->User_Name; ?>" data-type="<?php echo $User->Profile_Type; ?>" title="Edit User">
                            <i class="fa fa-pencil"></i>
                          </button>
                          <button class="btnTrash TrashIndividual" data-toggle="modal" data-target="#TrashIndividualModal" data-id="<?php echo $User->User_ID; ?>" title="Trash User">
                            <i class="fa fa-user-times"></i>
                          </button>
                        </h4>
                      </div>
                      <div class="card-body no-padding">
                        <div class="item">
                          <div class="feed d-flex justify-content-between">
                            <div class="feed-body d-flex justify-content-between">
                              <a class="feed-profile"><img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($User->User_Email))) . "?s=55"; ?>" alt="<?php echo $User->User_Name; ?>" class="img-fluid rounded-circle"></a>
                              <div class="content">
                                <h5><?php echo $User->User_Name; ?></h5>
                                <span><i class="fa fa-envelope"></i> <?php echo $User->User_Email; ?></span>
                                <div class="full-date">
                                  <small>
                                    <i class="fa fa-calendar"></i>
                                    <?php echo date('jS F, Y \a\t g:i A', strtotime($User->User_CreatedOn)); ?>
                                  </small>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
              <!-- ........................PAGINATION.............................. -->
              <div style="margin-top:60px"> <?=$pagination?></div>
              <!-- ........................PAGINATION.............................. -->
            <?php } else { ?>
              <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">No individual users are available.</div>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
          </div>
        </section>
        <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
      </div>
    </div>
  </div>

  <div class="modal right fade" id="IndividualModal" tabindex="-1" role="dialog" aria-labelledby="IndividualModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="IndividualModalLabel">Create New Individual User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="individual-form" method="post" action="<?php echo BASE_URL; ?>php/NewIndividualProcess">
          <div class="modal-body">
            <div class="form-group">
              <label class="form-control-label">Full Name<span class="text-danger">*</span></label>
              <input type="text" placeholder="Full Name" name="name" id="name" class="form-control" required="" />
            </div>
            <div class="form-group">
              <label class="form-control-label">Email Address<span class="text-danger">*</span></label>
              <input type="email" placeholder="Email Address" name="email" id="email" class="form-control" required="" />
            </div>
            <div class="form-group">
              <label class="form-control-label">Profile Type<span class="text-danger">*</span></label>
              <select name="profile_type" id="profile_type" class="form-control" style="height:45px;" required>
                <option value="">Select Type</option>
                <option value="demo">Demo</option>
                <option value="full">Full</option>
              </select>
            </div>
            <div class="form-group">
              <label class="form-control-label">Password<span class="text-danger">*</span></label>
              <input type="password" placeholder="Password" name="pass" id="pass" class="form-control" required="" />
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            <button type="submit" class="btn btn-primary">CREATE</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal center fade" id="TrashIndividualModal" tabindex="-1" role="dialog" aria-labelledby="TrashIndividualModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <form id="remove-individual-form" method="post" action="<?php echo BASE_URL; ?>php/RemoveIndividualUser">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Trash Individual User</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you would like to move this User to Trash?</p>
          </div>
          <div class="modal-footer">
            <div class="col">
              <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
            </div>
            <div class="col">
              <button type="submit" class="btn btn-primary btn-lg" id="RemoveIndividualUser">YES, TRASH IT</button>
            </div>
            <input type="hidden" name="removeIndividualUserID" id="removeIndividualUserID" />
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="modal right fade" id="EditIndividualModal" tabindex="-1" role="dialog" aria-labelledby="EditIndividualModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="RequestModalLabel">Edit Individual User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="edit-individual-form" method="post" action="<?php echo BASE_URL; ?>php/EditIndividualProcess" >
          <div class="modal-body">
            <div class="form-group">
              <label class="form-control-label">Name<span class="text-danger">*</span></label>
              <input type="text" placeholder="Full Name" name="editIndividualName" id="editIndividualName" class="form-control" required />
            </div>
            <div class="form-group">
              <label class="form-control-label">Profile Type<span class="text-danger">*</span></label>
              <select name="editIndividualProfile_type" id="editIndividualProfile_type" class="form-control" style="height:45px;" required>
                <option value="">Select Type</option>
                <option value="demo">Demo</option>
                <option value="full">Full</option>
              </select>
            </div>
            <div class="form-group">
              <label class="form-control-label">New Password </label>
              <input type="password" placeholder="Password" name="editIndividualPassword" id="editIndividualPassword" class="form-control" />
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="editIndividualID" id="editIndividualID" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            <button type="submit" class="btn btn-primary requestButton">UPDATE</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="<?php echo BASE_URL; ?>js/jquery.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/tether.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap-notify.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/jquery.validate.js"></script>
  <script src="<?php echo BASE_URL; ?>js/front.js"></script>
</body>
</html>
