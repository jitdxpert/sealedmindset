<?php
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$params = explode('/', $actual_link);
$string = $params[6];
$find   = '?';
$pos = strpos($string, $find);
?>
<nav class="side-navbar">
	<div class="sidebar-header d-flex align-items-center">
		<div class="avatar">
			<img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($_SESSION['Admin_Email']))) . "?s=55"; ?>" alt="<?php echo $_SESSION['Admin_Name']; ?>" class="img-fluid rounded-circle">
		</div>
		<div class="title">
	  		<h1 class="h4"><?php echo $_SESSION['Admin_Name']; ?></h1>
	  		<p>Administrator</p>
		</div>
	</div>
	<span class="heading">Main</span>
	<ul class="list-unstyled">
	  	<li class="<?php echo ($params[6] == 'companies') ? 'active' : '';?>"> <a href="<?php echo ADMIN_URL; ?>companies"><i class="icon-home"></i>Companies</a></li>
			<li class="<?php echo ($params[6] == 'individual') ? 'active' : '';?>"> <a href="<?php echo ADMIN_URL; ?>individual"><i class="icon-user"></i>Individual Users</a></li>
			<li class="<?php echo (($params[6] == 'playbooks') || ($pos !== false)) ? 'active' : '';?>"> <a href="<?php echo ADMIN_URL; ?>playbooks"><i class="icon-padnote"></i>Playbooks</a></li>
			<li class="<?php echo ($params[6] == 'trash-companies') ? 'active' : '';?>"> <a href="<?php echo ADMIN_URL; ?>trash-companies"><i class="fa fa-trash"></i>Suspended Companies</a></li>
			<li class="<?php echo ($params[6] == 'trash-individuals') ? 'active' : '';?>"> <a href="<?php echo ADMIN_URL; ?>trash-individuals"><i class="fa fa-trash"></i>Suspended Individuals</a></li>
	</ul>
</nav>
