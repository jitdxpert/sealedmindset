  <footer class="main-footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6">
          <p>Sealed Mindset &copy; 2017-2019</p>
        </div>
        <div class="col-sm-6 text-right">
          <p>Design by <a href="<?php echo ADMIN_URL; ?>" class="external">Sealed Mindset</a></p>
        </div>
      </div>
    </div>
  </footer>