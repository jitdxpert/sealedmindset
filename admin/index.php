<?php require(dirname(dirname(__FILE__)) . '/php/config.php'); ?>

<?php 

if ( isset($_SESSION['Admin_ID']) ) {

  header('location: ' . ADMIN_URL . 'companies');

} ?>



<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8" />

  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <title>Login - Sealed Mindset</title>



  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap-datepicker.min.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/font-awesome.min.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.default.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/modal.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/custom.css" />

  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/icons.css" />

  

  <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->

</head>

<body>

  <div class="page login-page">

    <div class="container d-flex align-items-center">

      <div class="form-holder has-shadow">

        <div class="row">

          <div class="col-lg-6">

            <div class="info d-flex align-items-center">

              <div class="content">

                <div class="logo">

                  <h1>Tactical Planning Cycle</h1>

                </div>

              </div>

            </div>

          </div>

          <div class="col-lg-6 bg-white">

            <div class="form d-flex align-items-center">

              <div class="content">

                <form id="login-form" method="post" action="<?php echo BASE_URL; ?>php/AdminLoginProcess">

                  <div class="form-group">

                    <input id="login-username" type="email" name="username" required="" class="input-material">

                    <label for="login-username" class="label-material">User Name or Email</label>

                  </div>

                  <div class="form-group">

                    <input id="login-password" type="password" name="password" required="" class="input-material">

                    <label for="login-password" class="label-material">Password</label>

                  </div>

                  <button type="submit" id="login" class="btn btn-primary">Login</button>

                </form>

                <a href="<?php echo ADMIN_URL; ?>forgot" class="forgot-pass">Forgot Password?</a>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="copyrights text-center">

      <p>Design by <a href="<?php echo ADMIN_URL; ?>" class="external">Sealed Mindset</a></p>

    </div>

  </div>



  <script src="<?php echo BASE_URL; ?>js/jquery.min.js"></script>

  <script src="<?php echo BASE_URL; ?>js/tether.min.js"></script>

  <script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>

  <script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.min.js"></script>

  <script src="<?php echo BASE_URL; ?>js/bootstrap-notify.min.js"></script>

  <script src="<?php echo BASE_URL; ?>js/jquery.validate.js"></script>

  <script src="<?php echo BASE_URL; ?>js/front.js"></script>

</body>

</html>