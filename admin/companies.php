<?php require(dirname(dirname(__FILE__)) . '/php/config.php'); ?>
<?php
if ( !isset($_SESSION['Admin_ID']) ) {
  header('location: ' . ADMIN_URL);
} ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Playbooks - Sealed Mindset</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap-datepicker.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.default.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/modal.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/custom.css" />
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/icons.css" />
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <div class="page">
    <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
    <div class="page-content d-flex align-items-stretch">
      <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
      <div class="content-inner">
        <ul class="breadcrumb">
          <div class="container-fluid">
            <li class="breadcrumb-item"><a href="<?php echo ADMIN_URL; ?>">Home</a></li>
            <li class="breadcrumb-item active">Companies</li>
          </div>
        </ul>
        <section class="companies">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">
                    <a class="newCompany" data-toggle="modal" data-target="#CompanyModal">
                      <i class="fa fa-plus"></i> Add New Company
                    </a>
                  </div>
                </div>
              </div>
              <div class="loader hide"></div>
              <!-- <div class="col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <a class="newCompany" data-toggle="modal" data-target="#CompanyModalCSV">
                    <a class="newCompany">
                    <i class="fa fa-upload"></i> Upload CSV
                    </a>
                  </div>
                </div>
              </div> -->
              <?php
              $CompanySQL = DB::table('companies')
              ->where('Trash', '=', 0)
              ->orderBy('Company_ID', 'DESC')
              ->count();
              if(!empty($CompanySQL)) {
                // Pagination
                $paginationObj = new Pagination;
                $targetpage = 'companies';
                $total_pages = $CompanySQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination

                $CompanyFetchSQL = DB::table('companies')
                ->where('Trash', '=', 0)
                ->orderBy('Company_ID', 'DESC')
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($CompanyFetchSQL as $Company) {
                  $User = DB::table('users')
                  ->where('Trash', 0)
                  ->where('User_Email', $Company->Company_Email)
                  ->where('Company_ID', $Company->Company_ID)
                  ->where('User_Type', '=', 'company')
                  ->first();
                  ?>
                  <div class="col-lg-3">
                    <div class="daily-feeds card">
                      <div class="card-header d-flex align-items-center">
                        <h4 class="h4">
                          <?php echo $Company->Company_Name; ?>
                          <button class="btnEdit EditCompany" data-toggle="modal" data-target="#EditCompanyModal" data-uid="<?php echo $User->User_ID; ?>" data-cid="<?php echo $Company->Company_ID; ?>" data-name="<?php echo $Company->Company_Name; ?>" data-owner="<?php echo $Company->Company_Owner; ?>" data-type="<?php echo $Company->Profile_Type; ?>" title="Edit Company">
                            <i class="fa fa-pencil"></i>
                          </button>
                          <button class="btnTrash TrashCompany" data-toggle="modal" data-target="#TrashCompanyModal" data-id="<?php echo $Company->Company_ID; ?>" title="Trash Company">
                            <i class="fa fa-user-times"></i>
                          </button>
                        </h4>
                      </div>
                      <div class="card-body no-padding">
                        <div class="item">
                          <div class="feed d-flex justify-content-between">
                            <div class="feed-body d-flex justify-content-between">
                              <a class="feed-profile"><img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($Company->Company_Email))) . "?s=55"; ?>" alt="<?php echo $Company->Company_Owner; ?>" class="img-fluid rounded-circle"></a>
                              <div class="content">
                                <h5><?php echo $Company->Company_Owner; ?></h5>
                                <span><i class="fa fa-envelope"></i> <?php echo $Company->Company_Email; ?></span>
                                <div class="full-date">
                                  <small>
                                    <i class="fa fa-calendar"></i>
                                    <?php echo date('jS F, Y \a\t g:i A', strtotime($Company->Company_CreatedOn)); ?>
                                  </small>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
              <!-- ........................PAGINATION.............................. -->
              <div style="margin-top:60px"> <?=$pagination?></div>
              <!-- ........................PAGINATION.............................. -->
            <?php } else { ?>

              <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">No companies are available.</div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </section>
        <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
      </div>
    </div>
  </div>

  <div class="modal right fade" id="CompanyModal" tabindex="-1" role="dialog" aria-labelledby="CompanyModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="CompanyModalLabel">Create New Company</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="company-form" method="post" action="<?php echo BASE_URL; ?>php/NewCompanyProcess">
          <div class="modal-body">
            <div class="form-group">
              <label class="form-control-label">Company Name<span class="text-danger">*</span></label>
              <input type="text" placeholder="Company Name" name="name" id="name" class="form-control" required="" />
            </div>
            <div class="form-group">
              <label class="form-control-label">Company Owner<span class="text-danger">*</span></label>
              <input type="text" placeholder="Company Owner" name="owner" id="owner" class="form-control" required="" />
            </div>
            <div class="form-group">
              <label class="form-control-label">Company Email<span class="text-danger">*</span></label>
              <input type="email" placeholder="Company Email" name="email" id="email" class="form-control" required="" />
            </div>
            <div class="form-group">
              <label class="form-control-label">Profile Type<span class="text-danger">*</span></label>
              <select name="type" id="type" class="form-control" style="height:45px;" required>
                <option value="">Select Type</option>
                <option value="demo">Demo</option>
                <option value="full">Full</option>
              </select>
            </div>
            <div class="form-group">
              <label class="form-control-label">Company Password<span class="text-danger">*</span></label>
              <input type="password" placeholder="Company Password" name="pass" id="pass" class="form-control" required="" />
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            <button type="submit" class="btn btn-primary">CREATE</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal center fade" id="TrashCompanyModal" tabindex="-1" role="dialog" aria-labelledby="TrashCompanyModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <form id="remove-company-form" method="post" action="<?php echo BASE_URL; ?>php/RemoveCompany">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Trash Company</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you would like to move this Company to Trash?</p>
          </div>
          <div class="modal-footer">
            <div class="col">
              <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">NO</button>
            </div>
            <div class="col">
              <button type="submit" class="btn btn-primary btn-lg" id="RemoveCompany">YES, TRASH IT</button>
            </div>
            <input type="hidden" name="removeCompanyID" id="removeCompanyID" />
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="modal right fade" id="CompanyModalCSV" tabindex="-1" role="dialog" aria-labelledby="CompanyModalCSVLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="CompanyModalLabel">Upload Company Lists</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="company-csv-form" method="post" action="<?php echo BASE_URL; ?>php/NewCompanyProcessCSV" enctype="multipart/form-data" target="upload_target">
          <div class="modal-body">
            <div class="form-group">
              <label class="form-control-label">Select Company<span class="text-danger">*</span></label>
              <select name="company" id="company" class="form-control" required="">
                <option></option>
                <option></option>
              </select>
            </div>
            <div class="form-group">
              <label class="form-control-label">Password<span class="text-danger">*</span></label>
              <input type="text" name="password" id="password" class="form-control" required="" />
            </div>
            <div class="form-group">
              <label class="form-control-label">Select File<span class="text-danger">*</span></label>
              <input type="file" placeholder="Select CSV" name="csv" id="csv" class="form-control" required="" />
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            <button type="submit" class="btn btn-primary">UPLOAD</button>
          </div>
        </form>
        <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
      </div>
    </div>
  </div>

  <div class="modal right fade" id="EditCompanyModal" tabindex="-1" role="dialog" aria-labelledby="EditCompanyModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="RequestModalLabel">Edit Company</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="edit-company-form" method="post" action="<?php echo BASE_URL; ?>php/EditCompanyProcess" >
          <div class="modal-body">
            <div class="form-group">
              <label class="form-control-label">Company Name<span class="text-danger">*</span></label>
              <input type="text" placeholder="Company Name" name="editCompanyName" id="editCompanyName" class="form-control" required />
            </div>
            <div class="form-group">
              <label class="form-control-label">Company Owner<span class="text-danger">*</span></label>
              <input type="text" placeholder="Company Owner" name="editCompanyOwner" id="editCompanyOwner" class="form-control" required="" />
            </div>
            <div class="form-group">
              <label class="form-control-label">Profile Type<span class="text-danger">*</span></label>
              <select name="editCompanyType" id="editCompanyType" class="form-control" style="height:45px;" required>
                <option value="">Select Type</option>
                <option value="demo">Demo</option>
                <option value="full">Full</option>
              </select>
            </div>
            <div class="form-group">
              <label class="form-control-label">New Password </label>
              <input type="password" placeholder="Company Password" name="editUserPassword" id="editUserPassword" class="form-control" />
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="editCompanyID" id="editCompanyID" />
            <input type="hidden" name="editUserID" id="editUserID" />
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            <button type="submit" class="btn btn-primary requestButton">UPDATE</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="<?php echo BASE_URL; ?>js/jquery.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/tether.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/bootstrap-notify.min.js"></script>
  <script src="<?php echo BASE_URL; ?>js/jquery.validate.js"></script>
  <script src="<?php echo BASE_URL; ?>js/front.js"></script>
</body>
</html>
