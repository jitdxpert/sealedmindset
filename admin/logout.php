<?php require(dirname(dirname(__FILE__)) . '/php/config.php');

unset($_SESSION['Admin_ID']);
unset($_SESSION['Admin_Name']);
unset($_SESSION['Admin_Email']);

header('location:' . ADMIN_URL);