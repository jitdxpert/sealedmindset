<?php
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$params = explode('/', $actual_link);
$string = $params[5];
$find1   = 'playbooks?';
$find2   = 'request?';
$find3   = 'shared?';
$pos1 = strpos($string, $find1);
$pos2 = strpos($string, $find2);
$pos3 = strpos($string, $find3);
?>
<nav class="side-navbar">
	<div class="sidebar-header d-flex align-items-center">
		<div class="avatar">
			<img src="<?php echo "https://www.gravatar.com/avatar/" . md5(strtolower(trim($_SESSION['UserLoggedIn']['User_Email']))) . "?s=55"; ?>" alt="<?php echo $_SESSION['UserLoggedIn']['User_Name']; ?>" class="img-fluid rounded-circle">
		</div>
		<div class="title">
	  		<h1 class="h4"><?php echo $_SESSION['UserLoggedIn']['User_Name']; ?></h1>
	  		<p><?php echo $_SESSION['UserLoggedIn']['Company_Name']; ?></p>
		</div>
	</div>
	<span class="heading">Main</span>
	<ul class="list-unstyled">
	  	<li class="<?php echo (($params[5] == 'playbooks') || ($params[5] == 'project') || ($params[5] == 'shared') || ($pos1 !== false) || ($pos3 !== false)) ? 'active' : '';?>">
				<a href="<?php echo BASE_URL; ?>playbooks"><i class="icon-padnote"></i><?php if($_SESSION['UserLoggedIn']['User_Type']=='request'){ echo "Shared Questions"; }else{ echo "Playbooks"; } ?></a>
			</li>
			<?php if($_SESSION['UserLoggedIn']['User_Type'] != 'request') { ?>
				<li class="<?php echo ($params[5] == 'users') ? 'active' : '';?>">
					<a href="<?php echo BASE_URL; ?>users"><i class="icon-user"></i>Users</a>
				</li>
			<?php } ?>
			<?php if($_SESSION['UserLoggedIn']['User_Type']!='company') { ?>
				<li class="<?php echo (($params[5] == 'request') || ($params[5] == 'view') || ($pos2 !== false)) ? 'active' : '';?>">
					<a href="<?php echo BASE_URL; ?>request"><i class="icon-user"></i>Shared Playbooks</a>
				</li>
			<?php } ?>
			<?php if($_SESSION['UserLoggedIn']['User_Type']=='company') { ?>
				<li class="<?php echo ($params[5] == 'trash') ? 'active' : '';?>">
					<a href="<?php echo BASE_URL; ?>trash"><i class="fa fa-trash"></i>Trash</a>
				</li>
			<?php } ?>
	</ul>
</nav>
