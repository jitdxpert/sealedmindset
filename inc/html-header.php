<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8" />
  	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width" />
  	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
  	<title><?php echo $page.' | '.SITE_TITLE; ?></title>

  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" />
  	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />
  	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap-datepicker.min.css" />
  	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/font-awesome.min.css" />
  	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/style.default.css" />
  	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/modal.css" />
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>css/nanoscroller.css" />
  	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/custom.css" />
  	<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/icons.css" />

  	<!--[if lt IE 9]>
      	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->
</head>
<body>