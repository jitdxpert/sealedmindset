<?php
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$params = explode('/', $actual_link);
$string = $params[5];
$find   = '?';
$pos = strpos($string, $find);
?>
<input type="hidden" name="site_url" id="site_url" value="<?php echo BASE_URL; ?>" />
<input type="hidden" name="admin_url" id="admin_url" value="<?php echo ADMIN_URL; ?>" />

<header class="header">
	<nav class="navbar">
	  	<div class="search-box">
            <button class="dismiss"><i class="icon-close"></i></button>
            <form id="searchForm" method="get" role="search">
              	<input type="search" name="q" value="<?php echo (isset($_GET['q'])) ? $_GET['q'] : ''; ?>" id="q" placeholder="Search playbook using title..." class="form-control">
            </form>
        </div>
	  	<div class="container-fluid">
	    	<div class="navbar-holder d-flex align-items-center justify-content-between">
	      		<div class="navbar-header">
	        		<a href="<?php echo BASE_URL; ?>" class="navbar-brand">
	          			<div class="brand-text brand-big hidden-lg-down"><span>Sealed </span><strong>Mindset</strong></div>
                  		<div class="brand-text brand-small"><strong>SM</strong></div></a>
	      			</a>
	      			<a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
	      		</div>
	      		<ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
						<?php if($params[5] == 'playbooks' || $params[5] == 'request' || $pos !== false){ ?>
							<li class="nav-item d-flex align-items-center">
	        			<a id="search" href="#" class="nav-link"><i class="icon-search"></i></a>
	        		</li>
						<?php } ?>
	        		<li class="nav-item d-flex align-items-center dropdown" id="notification-area">
	        			<a class="nav-link" id="notifications" href="<?php echo BASE_URL; ?>notifications/" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	        				<i class="fa fa-bell-o"></i>
	        			</a>
	        		</li>
	        		<li class="nav-item d-flex align-items-center">
	        			<a href="<?php echo BASE_URL; ?>settings/" class="nav-link"><i class="fa fa-gear"></i></a>
	        		</li>
			        <li class="nav-item d-flex align-items-center">
			        	<a href="<?php echo BASE_URL; ?>logout" class="logout">Logout<i class="fa fa-sign-out"></i></a>
			        </li>
	      		</ul>
	    	</div>
	  	</div>
	</nav>
</header>
