	<script src="<?php echo BASE_URL; ?>js/jquery.min.js"></script>
  	<script src="<?php echo BASE_URL; ?>js/tether.min.js"></script>
  	<script src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
  	<script src="<?php echo BASE_URL; ?>js/bootstrap-datepicker.min.js"></script>
  	<script src="<?php echo BASE_URL; ?>js/bootstrap-notify.min.js"></script>
    <script src="<?php echo BASE_URL; ?>js/jquery.nanoscroller.min.js"></script>
  	<script src="<?php echo BASE_URL; ?>js/jquery.validate.js"></script>
  	<script src="<?php echo BASE_URL; ?>js/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo BASE_URL; ?>js/tinymce/js/tinymce/tinymce.min.js"></script>
  	<script src="<?php echo BASE_URL; ?>js/front.js"></script>
</body>
</html>
