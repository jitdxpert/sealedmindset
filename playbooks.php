<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID'])) {
  redirect(BASE_URL);
}

$page = 'Playbooks';
require(dirname(__FILE__) . '/inc/html-header.php');
?>

<div class="page">
  <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
  <div class="page-content d-flex align-items-stretch">
    <?php require(dirname(__FILE__) . '/inc/navbar.php');
    if(isset($_GET["q"])){
      $q = $_GET["q"];
    } else {
      $q= '';
    }
    ?>
    <div class="content-inner">
      <div class="loader hide"></div>
      <header class="page-header">
        <div class="container-fluid">
          <h2 class="no-margin-bottom">
            Playbooks
            <?php if($_SESSION['UserLoggedIn']['User_Type'] == 'department'||$_SESSION['UserLoggedIn']['User_Type'] == 'user'||$_SESSION['UserLoggedIn']['User_Type'] == 'individual') {?>
              <div class="newPlaybook">
                <a data-toggle="modal" data-target="#PlaybookModal"><i class="fa fa-plus"></i></a>
              </div>
            <?php } ?>
          </h2>
        </div>
      </header>
      <?php if ($_SESSION['UserLoggedIn']['User_Type'] == 'individual') { ?>
        <section class="playbooks">
          <div class="container-fluid">
            <?php
            if($_SESSION['UserLoggedIn']['User_Type'] == 'individual') {
              $ProjectSQL = DB::table('projects')
              ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
              ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
              ->where(function($searchquery) use($q){
                $searchquery->where('Project_Name', 'like', '%'.$q.'%');
              })
              ->count();
              if(!empty($ProjectSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                if($q){
                  $targetpage = 'playbooks?q='.$q;
                } else {
                  $targetpage = 'playbooks';
                }
                $total_pages = $ProjectSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination

                $ProjectFetchSQL = DB::table('projects')
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
                ->where(function($searchquery) use($q){
                  $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                })
                ->orderBy('Project_ID', 'DESC')
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($ProjectFetchSQL as $Project) { ?>
                  <div class="parent">
                    <div class="child-row">
                      <div class="row">
                        <div class="col-md-9">
                          <div class="articles card left">
                            <div class="card-close">
                              <?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?>
                              <span class="pull-right">
                                <a data-toggle="modal" data-target="#ExportModal" class="exportRequestBtn" title="Export Playbook" data-project="<?php echo $Project->Project_ID; ?>">
                                  <i class="fa fa-external-link"></i>
                                </a>
                                <a data-toggle="modal" data-target="#PlayRequestModal" class="playRequestBtn" title="Share Playbook" data-project="<?php echo $Project->Project_ID; ?>">
                                  <i class="fa fa-share-alt"></i>
                                </a>
                              </span>
                            </div>
                            <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                              <?php echo GetSharedUser($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="articles card right">
                            <div class="card-body no-padding">
                              <?php echo GetAllTabFull($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                } ?>
                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:60px"> <?=$pagination?></div>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No projects are available.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            } else {
              $RequestSQL = DB::table('requests')
              ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
              ->where('User_ID', $_SESSION['UserLoggedIn']['User_ID'])
              ->count();
              if(!empty($RequestSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                if($q){
                  $targetpage = 'playbooks?q='.$q;
                } else {
                  $targetpage = 'playbooks';
                }
                $total_pages = $RequestSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                $RequestFetchSQL = DB::table('requests')
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where('User_ID', $_SESSION['UserLoggedIn']['User_ID'])
                ->groupBy('Project_ID')
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($RequestFetchSQL as $RequestData) {
                  $ProjectSQL = DB::table('projects')
                  ->where('Project_ID', $RequestData->Project_ID)
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->get();
                  if(!empty($ProjectSQL)) {
                    foreach($ProjectSQL as $Project) { ?>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="articles card">
                            <div class="card-close"><?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?></div>
                            <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                            </div>
                            <div class="item d-flex">
                              <button class="view-playbook" data-url="<?php echo BASE_URL; ?>shared?project=<?php echo safe_b64encode($Project->Project_ID); ?>">View Playbook</button>
                              <!-- <button class="view-playbook" data-url="<?php echo BASE_URL; ?>project/<?php echo safe_b64encode($Project->Project_ID); ?>">View Playbook</button> -->
                            </div>
                          </div>
                        </div>

                      </div>
                      <?php
                    } ?>
                    <!-- ........................PAGINATION.............................. -->
                    <div style="margin-top:60px"> <?=$pagination?></div>
                    <!-- ........................PAGINATION.............................. -->
                  <?php } else { ?>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">No projects are available.</div>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                }
              } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No projects are available.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
            ?>
          </div>
        </section>
      <?php } else { ?>
        <section class="playbooks">
          <div class="container-fluid">
            <?php
            if($_SESSION['UserLoggedIn']['User_Type'] == 'company'||$_SESSION['UserLoggedIn']['User_Type'] == 'department'||$_SESSION['UserLoggedIn']['User_Type'] == 'user') {
              if($_SESSION['UserLoggedIn']['User_Type'] == 'company'){
                $ProjectSQL = DB::table('projects')
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where(function($searchquery) use($q){
                  $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                })
                ->count();
              } elseif($_SESSION['UserLoggedIn']['User_Type'] == 'user') {
                $ProjectSQL = DB::table('projects')
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
                ->where(function($searchquery) use($q){
                  $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                })
                ->count();
              } elseif($_SESSION['UserLoggedIn']['User_Type'] == 'department') {
                $userIds = DB::table('users')
                ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
                ->pluck('User_ID');

                $ProjectSQL = DB::table('projects')
                ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where(function($query) use($userIds){
                  $query->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID']);
                  $query->orWhereIn('Master_ID', $userIds);
                })
                ->where(function($searchquery) use($q){
                  $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                })
                ->count();
              }
              if(!empty($ProjectSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                if($q){
                  $targetpage = 'playbooks?q='.$q;
                } else {
                  $targetpage = 'playbooks';
                }
                $total_pages = $ProjectSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                if($_SESSION['UserLoggedIn']['User_Type'] == 'company'){
                  $ProjectFetchSQL = DB::table('projects')
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->where(function($searchquery) use($q){
                    $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                  })
                  ->orderBy('Project_ID', 'DESC')
                  ->offset($start)
                  ->limit($limit)
                  ->get();
                } elseif($_SESSION['UserLoggedIn']['User_Type'] == 'user') {
                  $ProjectFetchSQL = DB::table('projects')
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
                  ->where(function($searchquery) use($q){
                    $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                  })
                  ->orderBy('Project_ID', 'DESC')
                  ->offset($start)
                  ->limit($limit)
                  ->get();
                } elseif($_SESSION['UserLoggedIn']['User_Type'] == 'department') {
                  // $userIds = DB::table('users')
                  // ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
                  // ->pluck('User_ID');

                  $ProjectFetchSQL = DB::table('projects')
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->where(function($query) use($userIds){
                    $query->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID']);
                    $query->orWhereIn('Master_ID', $userIds);
                  })
                  ->where(function($searchquery) use($q){
                    $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                  })
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy('Project_ID', 'DESC')
                  ->get();
                }

                foreach($ProjectFetchSQL as $Project) { ?>
                  <div class="parent">
                    <div class="child-row">
                      <div class="row">
                        <div class="col-md-9">
                          <div class="articles card left">
                            <div class="card-close">
                              <?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?>
                              <span class="pull-right">
                                <?php if($_SESSION['UserLoggedIn']['User_Type'] != 'user') { ?>
                                  <a data-toggle="modal" data-target="#ExportModal" class="exportRequestBtn" title="Export Playbook" data-project="<?php echo $Project->Project_ID; ?>">
                                    <i class="fa fa-external-link"></i>
                                  </a>
                                <?php } ?>
                                <a data-toggle="modal" data-target="#PlayRequestModal" class="playRequestBtn" title="Share Playbook" data-creator="<?php echo $Project->Master_ID; ?>" data-project="<?php echo $Project->Project_ID; ?>">
                                  <i class="fa fa-share-alt"></i>
                                </a>
                              </span>
                            </div>
                            <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                              <?php echo GetSharedUser($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="articles card right">
                            <div class="card-body no-padding">
                              <?php echo GetAllTabFull($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                } ?>
                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:60px"> <?=$pagination?></div>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No projects are available.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            } elseif($_SESSION['UserLoggedIn']['User_Type'] == 'request') {
              $RequestSQL = DB::table('requests')
              ->join('projects', 'requests.Project_ID', '=', 'projects.Project_ID')
              ->where('requests.Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
              ->where('requests.User_ID', $_SESSION['UserLoggedIn']['User_ID'])
              ->where(function($searchquery) use($q){
                $searchquery->where('projects.Project_Name', 'like', '%'.$q.'%');
              })
              ->count();
              if(!empty($RequestSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                if($q){
                  $targetpage = 'playbooks?q='.$q;
                } else {
                  $targetpage = 'playbooks';
                }
                $total_pages = $RequestSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination

                $RequestFetchSQL = DB::table('requests')
                ->join('projects', 'requests.Project_ID', '=', 'projects.Project_ID')
                ->where('requests.Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where('requests.User_ID', $_SESSION['UserLoggedIn']['User_ID'])
                ->where(function($searchquery) use($q){
                  $searchquery->where('projects.Project_Name', 'like', '%'.$q.'%');
                })
                ->select('requests.Request_ID', 'requests.Project_ID', 'requests.Company_ID', 'requests.Sender_ID', 'requests.User_ID', 'requests.Tab_ID', 'requests.Heading_ID', 'requests.Question_ID', 'requests.Answer_ID', 'requests.Request_On')
                ->groupBy('requests.Project_ID')
                ->offset($start)
                ->limit($limit)
                ->get();

                foreach($RequestFetchSQL as $RequestData) {
                  $ProjectSQL = DB::table('projects')
                  ->where('Project_ID', $RequestData->Project_ID)
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->get();
                  if(!empty($ProjectSQL)) {
                    foreach($ProjectSQL as $Project) { ?>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="articles card">
                            <div class="card-close"><?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?></div>
                            <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                            </div>
                            <div class="item d-flex">
                              <button class="view-playbook" data-url="<?php echo BASE_URL; ?>shared?project=<?php echo safe_b64encode($Project->Project_ID); ?>">View Playbook</button>
                              <!-- <button class="view-playbook" data-url="<?php echo BASE_URL; ?>project/<?php echo safe_b64encode($Project->Project_ID); ?>">View Playbook</button> -->
                            </div>
                          </div>
                        </div>

                      </div>
                      <?php
                    }
                  } else { ?>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">No questions are shared.</div>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                } ?>
                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:20px"> <?=$pagination?></div><br>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No questions are shared.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
            ?>
          </div>
        </section>
      <?php } ?>

      <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
    </div>
  </div>
</div>

<div class="modal right fade" id="PlaybookModal" tabindex="-1" role="dialog" aria-labelledby="PlaybookModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="PlaybookModalLabel">Create New Playbook</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="playbook-form" method="post" action="<?php echo BASE_URL; ?>php/NewPlaybookProcess">
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Project Name<span class="text-danger">*</span></label>
            <input type="text" placeholder="Project Name" name="project" class="form-control" required="" />
          </div>
          <div class="form-group">
            <label class="form-control-label">Choose Department<span class="text-danger">*</span></label>
            <select name="department" class="form-control" required="">
              <option value="">---Choose Department---</option>
              <option value="IT Software">IT Software</option>
              <option value="IT Hardware">IT Hardware</option>
              <option value="Marketing">Marketing</option>
            </select>
          </div>
          <div class="form-group">
            <label class="form-control-label">Choose Deadline<span class="text-danger">*</span></label>
            <input type="text" placeholder="Choose Deadline" name="deadline" class="form-control" required="" data-provide="datepicker" data-date-format="yyyy-mm-dd" />
          </div>
          <div class="form-group">
            <label class="form-control-label">Define Goal in 140 Characters<span class="text-danger">*</span></label>
            <textarea placeholder="Define Goals" name="goals" class="form-control" rows="6" maxlength="140" required=""></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary">CREATE</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal right fade" id="PlayRequestModal" tabindex="-1" role="dialog" aria-labelledby="PlayRequestModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="PlayRequestModalLabel">Send Playbook Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="play-request-form" method="post" action="<?php echo BASE_URL; ?>php/PlaybookRequest" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Members List<span class="text-danger">*</span></label>
            <select name="memberId" id="memberId" class="form-control" required>
            </select>
          </div>
          <div class="form-group">
            <label class="form-control-label">Choose Type<span class="text-danger">*</span></label>
            <select name="shareType" id="shareType" class="form-control" required="">
              <option value="">Select Member First</option>
            </select>
          </div>
          <input type="hidden" id="project" name="project" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary shareButton">SHARE</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal right fade" id="ExportModal" tabindex="-1" role="dialog" aria-labelledby="ExportModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ExportModalLabel">Export Playbook</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php
      $html = '';
      $type = '';
      if($_SESSION['UserLoggedIn']['User_Type']=='company'||$_SESSION['UserLoggedIn']['User_Type']=='department'||$_SESSION['UserLoggedIn']['User_Type']=='individual'){
        $membersSQL = DB::table('users')
        ->where('User_Type', '=', 'user')
        ->orWhere('User_Type', '=', 'individual')
        ->get();
        $html .= '<select name="memberId" id="memberId" class="form-control" required>';
        $html .= '<option value="">Select User</option>';
        foreach($membersSQL as $member) {
          if($member->Master_ID != 0){
            $memberComanySQL = DB::table('companies')
            ->where('Company_ID', $member->Company_ID)
            ->first();
            $userType = "Company: ".$memberComanySQL->Company_Name;
          } else {
            $userType = "User";
          }
          $html .= '<option value="'.$member->User_ID.'"> '.$member->User_Name.' ( '.$userType.' )</option>';
        }
        $html .= '</select>';
      }
      ?>
      <form id="play-export-form" method="post" action="<?php echo BASE_URL; ?>php/ExportPlaybook" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Users List<span class="text-danger">*</span></label>
            <?php echo $html; ?>
          </div>
          <input type="hidden" id="projectId" name="projectId" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary exportButton">EXPORT</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>
