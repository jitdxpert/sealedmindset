<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['username'])) {
	$res['code'] = 6;
	$res['text'] = 'Email address is required.';
	goto RESPONSE;
}

$username = addslashes($_POST['username']);
if(!filter_var($username, FILTER_VALIDATE_EMAIL)) {
	$res['code'] = 5;
	$res['text'] = 'Enter a valid email address.';
	goto RESPONSE;
}

$admin = DB::table('admin')->where('Admin_Email', '=', $username)->first();
if(!$admin) {
	$res['code'] = 3;
	$res['text'] = 'Entered wrong email address.';
	goto RESPONSE;
}

$chars     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
$password  = substr(str_shuffle($chars), 0, 8);
$subject  = 'Sealed Mindset Admin Reset Password';
$body = '
<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#FCFCFD url(' . BASE_URL . 'images/body-bg.png) repeat 0 0;border:1px solid rgba(0, 0, 0, 0.15);font-family:Verdana,sans-serif;">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="75" align="center" style="background:rgba(0, 0, 0, 0.15) repeat;border-bottom: 1px solid rgba(0, 0, 0, 0.15);">
						<img width="200" alt="" src="' . BASE_URL . 'images/email-logo.png" alt="SealedMindset" />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#0e59ac" size="2.5">
							<br /><br /><span>Dear ' . $admin->Admin_Name . ',<br /><br />We have received a request to reset your admin password.<br />Please use below credentials to access your Sealed Mindset admin.</span>
						</font><br /><br /><br /><br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#0e59ac" size="3">
							<span>
								Username: <strong>' . $username . '</strong><br />
								Password: <strong>' . $password . '</strong>
							</span>
						</font><br /><br />
						<font color="#161616" size="2">
							<span>Please keep this information safe and secure.</span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td>
						<hr>
						<br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>In case if you have any questions, please send an Email to : <a style="color:#41a5e1;text-decoration:none;" href="mailto:support@sealedmindset.com">support@sealedmindset.com</a></span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>Regards,<br /><br />The <a href="'.BASE_URL.'">SealedMindset</a> Team.</span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="center">
						<font color="#aaa" size="2">
								<span>P.S: This is a system generated email. Please do not reply.</span><br /><br />
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>';
$send = Send_Mail($subject, $body, $admin->Admin_Name, $username);
if(!$send) {
	$res['code'] = 2;
	$res['text'] = 'Something went wrong, please try again.';
	goto RESPONSE;
}

$updateSQL = DB::table('admin')->where('Admin_Email', '=', $username)->update(['Admin_Password' => sha1($password)]);
if($updateSQL) {
	$res['code'] = 0;
	$res['text'] = 'New password has been sent to your email address.';
	goto RESPONSE;
} else {
	$res['code'] = 1;
	$res['text'] = 'Something went wrong, please try again.';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);