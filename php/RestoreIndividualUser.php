<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['restoreIndividualUserID'])) {
	$res['code'] = 2;
	$res['text'] = 'Something went wrong. Please try again later!';
	goto RESPONSE;
}

$userID  = $_POST['restoreIndividualUserID'];
$userVAL = DB::table('users')->where('User_ID', '=', $userID)->first();

$query = DB::table('users')->where('User_ID', '=', $userID)->update(['Trash' => 0]);
if(!$query) {
	$res['code'] = 1;
	$res['text'] = 'Oops! Unable to remove. Try again later!';
	goto RESPONSE;
} else {
	$res['code'] = 0;
	$res['text'] = 'User Successfully restored!';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
