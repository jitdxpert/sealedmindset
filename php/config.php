<?php
// ** MySQL Settings - You can get this info from your web host ** //
define('DB_NAME', 		'sealedmindset');   					//sealedmindset
define('DB_USER', 		'root');            					//sealedmindset
define('DB_PASSWORD', 	'');                					//sealedmindset
define('DB_HOST', 		'localhost');       					//localhost
define('DB_CHARSET', 	'utf8');
define('DB_COLLATE', 	'utf8_unicode_ci');
define('DB_PREFIX', 	'sm_');

$BASE_URL = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/';

// ** Application BASE URL ** //
define('SITE_TITLE', 	'Sealed Mindset');      			//Website Title
define('BASE_URL', 		$BASE_URL);    						//http://soumitrapaul.com/sealedmindset/
define('ADMIN_URL', 	BASE_URL . 'admin/');   			//http://soumitrapaul.com/sealedmindset/admin/

// Email Constant
define('PHPMAILER_SMTPSECURE',      'ssl');
define('PHPMAILER_HOST',            'soumitrapaul.com');
define('PHPMAILER_PORT',            '465');
define('PHPMAILER_USERNAME',        'info@soumitrapaul.com');
define('PHPMAILER_PASSWORD',        'wiselydevteam101');
define('PHPMAILER_FROM',            'info@soumitrapaul.com');
define('PHPMAILER_FROMNAME',        'Sealed Mindset');
define('PHPMAILER_WORDWRAP',        '50');

// ** Database Class API  ** //
include_once(dirname(__FILE__) . "/db.class.php");
include_once(dirname(__FILE__) . "/orm/boot.php");
require_once dirname(__FILE__) . "/dompdf/autoload.inc.php";

// ** League CSV Reader ** //
require_once dirname(__FILE__) . "/csv/autoload.php";


// ** PHPMailer Class API ** //
include_once(dirname(__FILE__) . "/phpmailer/class.phpmailer.php");
include_once(dirname(__FILE__) . "/phpmailer/class.smtp.php");
$PHPMailer = new PHPMailer;

// ** SwiftMailer Class API ** //
/*include_once(dirname(__FILE__) . "/swiftmailer/vendor/autoload.php";
$transport   = (new Swift_SmtpTransport('smtp.example.org', 25))->setUsername('your username')->setPassword('your password');
$SwiftMailer = new Swift_Mailer($transport);*/

// ** Pagination Class ** //
include(dirname(__FILE__) . "/Pagination.php");

// ** Helper Class File ** //
include(dirname(__FILE__) . "/functions.php");

// ** Session Start ** //
session_start();

// ** INI Set ** //
ini_set('max_execution_time', 300);
ini_set('memory_limit', '100M');
ini_set('always_populate_raw_post_data', -1);

// ** Application Environment ** //
define('DEBUG', true);

if ( DEBUG == true ) {
	ini_set( "display_errors", 1 );
} else {
	ini_set( "display_errors", 0 );
}
