<?php
function safe_b64encode($string) {
	$data = base64_encode($string);
	$data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);

	return $data;
}

function safe_b64decode($string) {
	$data = str_replace(array('-', '_'), array('+', '/'), $string);
	$mod4 = strlen($data) % 4;
	if ( $mod4 ) {
		$data .= substr('====', $mod4);
	}

	return base64_decode($data);
}

function GetAnswerByQuestionIdAndProjectId($QuestionID, $ProjectID) {
	global $db;

	$ansAry = array();
	$HeadingSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "answers` WHERE `Question_ID` = $QuestionID AND `Project_ID` = $ProjectID AND (`Answer_Status` = 'Accept' OR `Answer_Status` = 'Draft') LIMIT 0, 1");
	if ( $HeadingSQL ) {
		if ( mysqli_num_rows($HeadingSQL) > 0 ) {
			$HeadingData = mysqli_fetch_assoc($HeadingSQL);
			$ansAry = $HeadingData;
		}
	}

	return $ansAry;
}

function GetShortCodeReplace($String, $ProjectID) {
	global $db;

	$QuestionStr = array();

	$ScoreArray  = ['1st' => 3, '2nd' => 2, '3rd' => 1];
	$Sum1 		 = ['OB228', 'OB231', 'OB239', 'OB242', 'OB245', 'OB248', 'OB37', 'OB251', 'OB254', 'OB36'];
	$Sum2 		 = ['OB229', 'OB232', 'OB240', 'OB243', 'OB246', 'OB249', 'OB44', 'OB252', 'OB255', 'OB43'];
	$Sum3 		 = ['OB230', 'OB233', 'OB241', 'OB244', 'OB247', 'OB250', 'OB51', 'OB253', 'OB256', 'OB50'];
	$res1 		 = 0;
	$res2 		 = 0;
	$res3 		 = 0;
	$comp1  	 = '';
	$comp2  	 = '';
	$comp3  	 = '';
	$compare 	 = [];


	$QuestionSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Question_Str` LIKE 'OB%'");
	if( $QuestionSQL ) {
		if( mysqli_num_rows($QuestionSQL) > 0 ) {
			while( $QuestionData = mysqli_fetch_assoc($QuestionSQL) ) {
				$QuestionStr[] = $QuestionData['Question_Str'];
			}
		}
	}

	foreach($QuestionStr as $Qtn) {
		$Question 		= GetQuestionByQuestionStr($Qtn);
		$Answer   		= GetAnswerByQuestionIdAndProjectId($Question['QuestionID'], $ProjectID);
		if(empty($Answer)) {
			if( strpos($String, '{'.$Qtn.'}') !== FALSE ) {
				$String = str_replace('{'.$Qtn.'}', '<strong>No Answer Provided</strong>', $String);
				$String = str_replace('{SUM1}', '<strong>No Answer Provided</strong>', $String);
				$String = str_replace('{SUM2}', '<strong>No Answer Provided</strong>', $String);
				$String = str_replace('{SUM3}', '<strong>No Answer Provided</strong>', $String);
			}
		} else {
			foreach($Answer as $key=>$value) {
				if( $key == 'Answer' ) {
					if( strpos($String, '{'.$Qtn.'}') !== FALSE ) {
						if(in_array($Qtn, $Sum1)) {
							$res1   = $res1+$ScoreArray[$value];
							if($res1 == 0) {
								$String = str_replace('{SUM1}', '<strong>No Answer Provided</strong>', $String);
							} else {
								if($Qtn == end($Sum1)) {
									$String = str_replace('{SUM1}', '<strong>'.$res1.'</strong>', $String);
									//$comp1 = $res1;
								}
							}
						}

						if(in_array($Qtn, $Sum2)) {
						    $res2   = $res2+$ScoreArray[$value];
						    if($res2 == 0) {
								$String = str_replace('{SUM2}', '<strong>No Answer Provided</strong>', $String);
							} else {
							    if($Qtn == end($Sum2)) {
							    	$String = str_replace('{SUM2}', '<strong>'.$res2.'</strong>', $String);
							    	//$comp2 = $res2;
							    }
							}
						}

						if(in_array($Qtn, $Sum3)) {
						    $res3   = $res3+$ScoreArray[$value];
						    if($res3 == 0) {
								$String = str_replace('{SUM3}', '<strong>No Answer Provided</strong>', $String);
							} else {
							    if($Qtn == end($Sum3)) {
							    	$String = str_replace('{SUM3}', '<strong>'.$res3.'</strong>', $String);
							    	//$comp3 = $res3;
							    }
							}
						}

						$String = str_replace('{'.$Qtn.'}', '<strong>'.stripslashes($value).'</strong>', $String);
					}
				}
			}
		}
	}

	/*if($comp1 != '' && $comp2 != '' && $comp3 != '') {
		if( (strpos($String, '{SUM1}') !== FALSE) && (strpos($String, '{SUM3}') !== FALSE) && (strpos($String, '{SUM2}') !== FALSE) ) {
			$compare = ['SUM1' => $comp1, 'SUM2' => $comp2, 'SUM3' => $comp3];
			$ordered_values = $compare;
			rsort($ordered_values);
			foreach ($compare as $k => $v) {
			    foreach ($ordered_values as $ordered_key => $ordered_value) {
			    	$old_key = $k;
			        if ($v === $ordered_value) {
			            $k = $ordered_key;
			            break;
			        }
			    }
			    $kv = ((int) $k + 1);
			    $String = str_replace('{'.$old_key.'}', '<strong>'.$kv.'</strong>', $String);
			}
		}
	}*/

	return $String;
}

function GetHeadingDetails($headingId) {
	global $db;

	$HeadingSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "headings` WHERE `Heading_ID` = $headingId LIMIT 0, 1");
	if ( $HeadingSQL ) {
		if ( mysqli_num_rows($HeadingSQL) > 0 ) {
			$HeadingData = mysqli_fetch_assoc($HeadingSQL);
			return $HeadingData;
		}
	}
}

function GetQuestionsByHeadingId($headingId) {
	global $db;

	$qtnAry = array();
	$HeadingSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Heading_ID` = $headingId");
	if ( $HeadingSQL ) {
		if ( mysqli_num_rows($HeadingSQL) > 0 ) {
			$i = 0;
			while ( $HeadingData = mysqli_fetch_assoc($HeadingSQL) ) {
				$qtnAry[$i]['QuestionID']       	= $HeadingData['Question_ID'];
				$qtnAry[$i]['QuestionName']     	= $HeadingData['Question_Name'];
				$qtnAry[$i]['QuestionHeading']  	= $HeadingData['Question_Heading'];
				$qtnAry[$i]['QuestionType']     	= $HeadingData['Question_Type'];
				$qtnAry[$i]['QuestionOption']   	= $HeadingData['Question_option'];
				$qtnAry[$i]['QuestionOptType']  	= $HeadingData['Question_option_type'];
				$qtnAry[$i]['QuestionVideo']    	= $HeadingData['Question_Video_URL'];
				$qtnAry[$i]['QuestionAllias']   	= $HeadingData['Question_Allias'];
				$qtnAry[$i]['QuestionStr']      	= $HeadingData['Question_Str'];
				$qtnAry[$i]['QuestionRefText']  	= $HeadingData['Question_Ref_Text'];
				$qtnAry[$i]['QuestionPlaceholder']  = $HeadingData['Question_Placeholder'];
				$i++;
			}
		}
	}

	return $qtnAry;
}

function GetRequestQuestionsByHeadingId($HeadingID, $QuestionID) {
	global $db;

	$qtnAry = array();
	$HeadingSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Heading_ID` = $HeadingID AND `Question_ID` = $QuestionID");
	if ( $HeadingSQL ) {
		if ( mysqli_num_rows($HeadingSQL) > 0 ) {
			$i = 0;
			while ( $HeadingData = mysqli_fetch_assoc($HeadingSQL) ) {
				$qtnAry[$i]['QuestionID']       	= $HeadingData['Question_ID'];
				$qtnAry[$i]['QuestionName']     	= $HeadingData['Question_Name'];
				$qtnAry[$i]['QuestionHeading']  	= $HeadingData['Question_Heading'];
				$qtnAry[$i]['QuestionType']     	= $HeadingData['Question_Type'];
				$qtnAry[$i]['QuestionOption']   	= $HeadingData['Question_option'];
				$qtnAry[$i]['QuestionOptType']  	= $HeadingData['Question_option_type'];
				$qtnAry[$i]['QuestionVideo']    	= $HeadingData['Question_Video_URL'];
				$qtnAry[$i]['QuestionAllias']   	= $HeadingData['Question_Allias'];
				$qtnAry[$i]['QuestionStr']      	= $HeadingData['Question_Str'];
				$qtnAry[$i]['QuestionRefText']  	= $HeadingData['Question_Ref_Text'];
				$qtnAry[$i]['QuestionPlaceholder']  = $HeadingData['Question_Placeholder'];
				$i++;
			}
		}
	}

	return $qtnAry;
}

function GetSharedUserByQuestionIdAndProjectId($QuestionID, $ProjectID) {
	global $db;

	$User = '';
	$CheckQtnShare = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Question_ID` = $QuestionID AND `Project_ID` = $ProjectID LIMIT 0, 1");
	if ( $CheckQtnShare ) {
		if ( mysqli_num_rows($CheckQtnShare) > 0 ) {
			$HeadingData = mysqli_fetch_assoc($CheckQtnShare);
			$UserID = $HeadingData['User_ID'];
			$User   = GetUserById($UserID);
		}
	}

	return $User;
}

function CheckQuestionShared($QuestionID, $ProjectID) {
	global $db;

	$data = '';
	$CheckQtnShare = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Question_ID` = $QuestionID AND `Project_ID` = $ProjectID LIMIT 0, 1");
	if ( $CheckQtnShare ) {
		if ( mysqli_num_rows($CheckQtnShare) > 0 ) {
			$data = 'disabled';
		}
	}

	return $data;
}

function GetQuestionsByTabIDAndHeadingID($TabID, $HeadingID, $ProjectID) {
	global $db;

	$html = '';

	$viewTypeSQL = DB::table('share')
	->where('Project_ID', $ProjectID)
	->where('User_ID', $_SESSION['UserLoggedIn']['User_ID'])
	->first();
	if(!empty($viewTypeSQL)){
	  $viewType = $viewTypeSQL->Shared_Type;
	} else {
	  $viewType = '';
	}

	$distinctHeaderAry = array();
	$HeadingRef = array();
	$largeEditor = [159, 161, 163, 165, 167];
	$QuestionSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Tab_ID` = $TabID AND `Heading_ID` = $HeadingID");
	if ( $QuestionSQL ) {
		if ( mysqli_num_rows($QuestionSQL) > 0 ) {
			$i = 0;
			$m = 0;
			while ( $QuestionData = mysqli_fetch_assoc($QuestionSQL) ) {
				if ( !in_array($QuestionData['Heading_ID'], $distinctHeaderAry) ) {
					$distinctHeaderAry[] = $QuestionData['Heading_ID'];
				}
			}
			$html .= '<div class="row first-row">';
				foreach ( $distinctHeaderAry as $headerID ) {
					$headingData = GetHeadingDetails($headerID);
					$qtnDetails  = GetQuestionsByHeadingId($headerID);
					foreach ( $qtnDetails as $qtn ) {
						$intro  = ($qtn['QuestionAllias'] == 'Overview' && $qtn['QuestionVideo'] != '') ? 'Introduction' : 'Reference';
						$refval = ($qtn['QuestionRefText'] != '') ? $qtn['QuestionRefText'] : '';
						$refval = GetShortCodeReplace($refval, $ProjectID);
					}
					$head = explode(",", $headingData['Question_Ref_ID']);
					$html .= '<div class="col-md-4">';
						$html .= '<div class="card">';
							$html .= '<div class="card-header">';
								$html .= '<h2 class="h3">'.$intro.'</h2>';
							$html .= '</div>';
							$html .= '<div class="card-body">';
								$html .= '<div class="nano">';
									$html .= '<div class="nano-content">';
										$html .= '<div class="answerWrapper">';
											if( $refval != '' ) {
												$html .= '<div class="answerArea">';
													$html .= $refval;
												$html .= '</div>';
											}
											for($r=0;$r<count($head);$r++) {
												if( $intro != 'Introduction' ) {
													if( !empty($head[$r]) ) {
														$Que = GetQuestionByQuestionID($head[$r]);
														$Ans = GetAnswerByQuestionIdAndProjectId($head[$r], $ProjectID);
														if( !empty($Ans) ) {
															$html .= '<div class="answerArea">';
																if( $Que['QuestionHeading'] ) {
																	$html .= stripslashes($Que['QuestionHeading']);
																} else {
																	$html .= stripslashes($Que['QuestionName']);
																}
																$html .= '<hr/>';
																$html .= $Ans['Answer'];
															$html .= '</div>';
														} else {
															if($refval == '') {
																$html .= '<div class="answerArea">';
																	$html .= 'No data pulled forward';
																$html .= '</div>';
															}
															break;
														}
													}
												} else {
													$html .= '<div class="answerArea">';
														$html .= 'No data pulled forward';
													$html .= '</div>';
												}
											}
										$html .= '</div>';
									$html .= '</div>';
								$html .= '</div>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
				}

				$html .= '<div class="col-md-8">';
				foreach ( $distinctHeaderAry as $headerID ) {
					$html .= '<div class="card">';
						$headingData = GetHeadingDetails($headerID);
						$qtnDetails  = GetQuestionsByHeadingId($headerID);
						/*$html .= '<div class="card-header">';
							$html .= '<h2 class="h3">';
								$html .= $headingData['Heading_Name'];
							$html .= '</h2>';
						$html .= '</div>';*/

						$n = 0;
						$html .= '<div class="card-body">';
							$html .= '<div class="nano">';
								$html .= '<div class="nano-content">';
									foreach ( $qtnDetails as $qtn ) {
										$Answer = GetAllAnswerByProjectIdAndCompanyId($ProjectID, $_SESSION['UserLoggedIn']['Company_ID'], $TabID, $headerID, $qtn['QuestionID']);
										$html .= '<div class="row question-row '.$qtn['QuestionOptType'].'">';
											$html .= '<div class="col-lg-12">';
												$html .= '<div class="form-group">';
													$CheckQtnShare = mysqli_query($db,"SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Project_ID` = " . $ProjectID . " AND `Question_ID` = " . $qtn['QuestionID']);
													$disabled = CheckQuestionShared($qtn['QuestionID'], $ProjectID);
													$video    = ($qtn['QuestionAllias'] == 'Overview' && $qtn['QuestionVideo'] != '' ) ? $qtn['QuestionVideo'] : '';
													$placeholder = ' placeholder="'.$qtn['QuestionPlaceholder'].'"';
													$sharer   = GetSharedUserByQuestionIdAndProjectId($qtn['QuestionID'], $ProjectID);
													if ( !empty($sharer) ) {
														$sharerName = $sharer['User_Name'];
													}

													if ( $CheckQtnShare ) {
														if ( mysqli_num_rows( $CheckQtnShare ) > 0 ) {
															if ( $disabled != "" ) {
																$html .= '<h4 class="h5">' . stripslashes($qtn['QuestionHeading']) . stripslashes(GetShortCodeReplace($qtn['QuestionName'], $ProjectID)) . '</h4>';
															} else {
																$html .= '<h4 class="h5">' . stripslashes($qtn['QuestionHeading']) . stripslashes(GetShortCodeReplace($qtn['QuestionName'], $ProjectID));
																if( $qtn['QuestionAllias'] != 'Overview' ) {
																	if($_SESSION['UserLoggedIn']['User_Type']!='request'){
																		if($viewType==''){
																			$html .= '<a data-toggle="modal" data-target="#RequestModal" onClick="setQuestionID(' . $qtn['QuestionID'] . ')" class="requestBtn" title="Share"><i class="fa fa-share-alt"></i></a>';
																		}
																	}
																}
																$html .= '</h4>';
															}
															if( $video != "" ) {
																$html .= '<div class="embed-responsive embed-responsive-16by9">';
																	$html .= '<img src="' . BASE_URL . 'img/video-coming-soon.jpg" class="embed-responsive-item" />';
																	/*if( strpos($video, 'youtube') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } elseif( strpos($video, 'vimeo') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } else {
																		$html .= '<video controls webkit-playsinline class="embed-responsive-item" poster="'.BASE_URL.'img/poster.jpg"  class="tab-video">';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.mp4" type="video/mp4" />';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.webm" type="video/webm" />';
									                                    $html .= '</video>';
									                                  }*/
									                              	$html .= '</div>';
															}
															if ( $qtn['QuestionType'] == 'text' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																$html .= '<input type="text" '.$placeholder.' class="form-control" ' . $disabled . ' name="answer[' . $qtn['QuestionID'] . ']" value="' . $answer . '" />';
																$html .= ($disabled != '') ? '<p class="help-block pull-right share-que">This Question Shared with <strong class="text-info"><i>' . $sharerName . '</i></strong></p>' : '';
															} elseif ( $qtn['QuestionType'] == 'desc' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																if($disabled != '') {
																	$html .= '<textarea '.$placeholder.' class="form-control nonEditableMCE" rows="7" '.$disabled.' name="answer['.$qtn['QuestionID'].']">'.$answer.'</textarea>';
																} else {
																	$editorClass = (in_array($qtn['QuestionID'], $largeEditor)) ? 'editableLargeMCE' : 'editableMCE';
																	$html .= '<textarea class="form-control '.$editorClass.'" rows="7" name="answer['.$qtn['QuestionID'].']">'.$answer.'</textarea>';
																}
																$html .= ($disabled != '') ? '<p class="help-block pull-right share-que">This Question Shared with <strong class="text-info"><i>' . $sharerName . '</i></strong></p>' : '';
															} elseif ( $qtn['QuestionType'] == 'radio' ) {
																$optionArray = explode('|', $qtn['QuestionOption']);
																$html .= '<div class="row">';
																	$html .= '<div class="btn-group btn-group-justified '.$qtn['QuestionOptType'].'" data-toggle="buttons">';
																	foreach ( $optionArray as $key => $option ) {
																		if($key == 0 && !$Answer['Answer']){
																			$activeClass = 'active';
																			$checkedRadio = 'checked';
																		}elseif($option == $Answer['Answer']){
																			$activeClass = 'active';
																			$checkedRadio = 'checked';
																		}else{
																			$activeClass = '';
																			$checkedRadio = '';
																		}
																		$html .= '<label class="btn btn-lg btn-primary '.$disabled.' '.$activeClass.'">
																			<input '.($Answer['Answer_Status'] == "Accept" ? "disabled" : "").' name="answer['.$qtn['QuestionID'].']" '.$disabled.' '.$checkedRadio.' value="'.$option.'" type="radio">' . $option . '
																		</label>';
																	}
																	$html .= '</div>';
																$html .= '</div>';
																$html .= ($disabled != '') ? '<p class="help-block pull-right share-que">This Question Shared with <strong class="text-info"><i>' . $sharerName . '</i></strong></p>' : '';
															}
														} else {
															$html .= '<h4 class="h5">' . stripslashes($qtn['QuestionHeading']) . stripslashes(GetShortCodeReplace($qtn['QuestionName'], $ProjectID));
															if( $qtn['QuestionAllias'] != 'Overview' ) {
																if($_SESSION['UserLoggedIn']['User_Type']!='request'){
																	if($viewType==''){
																		$html .= '<a data-toggle="modal" data-target="#RequestModal" onClick="setQuestionID(' . $qtn['QuestionID'] . ')" class="requestBtn" title="Share"><i class="fa fa-share-alt"></i></a>';
																	}
																}
															}
															$html .= '</h4>';
															if( $video != "" ) {
																$html .= '<div class="embed-responsive embed-responsive-16by9">';
																	$html .= '<img src="' . BASE_URL . 'img/video-coming-soon.jpg" class="embed-responsive-item" />';
																	/*if( strpos($video, 'youtube') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } elseif( strpos($video, 'vimeo') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } else {
																		$html .= '<video controls webkit-playsinline class="embed-responsive-item" poster="'.BASE_URL.'img/poster.jpg"  class="tab-video">';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.mp4" type="video/mp4" />';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.webm" type="video/webm" />';
									                                    $html .= '</video>';
									                                }*/
									                            	 $html .= '</div>';
															}
															if ( $qtn['QuestionType'] == 'text' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																$html .= '<input type="text" '.$placeholder.' class="form-control" name="answer[' . $qtn['QuestionID'] . ']" value="' . $answer . '" />';
															} elseif ( $qtn['QuestionType'] == 'desc' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																$editorClass = (in_array($qtn['QuestionID'], $largeEditor)) ? 'editableLargeMCE' : 'editableMCE';
																$html .= '<textarea '.$placeholder.' class="form-control '.$editorClass.'" rows="7" name="answer[' . $qtn['QuestionID'] . ']">' . $answer . '</textarea>';
															} elseif ( $qtn['QuestionType'] == 'radio' ) {
																$html .= '<div class="col-md-12">';
																	$optionArray = explode('|',$qtn['QuestionOption']);
																	$html .= '<div class="row">';
																		$html .= '<div class="btn-group btn-group-justified '.$qtn['QuestionOptType'].'" data-toggle="buttons">';
																		foreach ( $optionArray as $key => $option ) {
																			if($key == 0 && !$Answer['Answer']){
																				$activeClass = 'active';
																				$checkedRadio = 'checked';
																			}elseif($option == $Answer['Answer']){
																				$activeClass = 'active';
																				$checkedRadio = 'checked';
																			}else{
																				$activeClass = '';
																				$checkedRadio = '';
																			}
																			$html .= '<label class="btn btn-lg btn-primary '.$disabled.' '.$activeClass.'">
																				<input '.($Answer['Answer_Status'] == "Accept" ? "disabled" : "").' name="answer['.$qtn['QuestionID'].']" '.$checkedRadio.' value="'.$option.'" type="radio">' . $option . '
																			</label>';
																		}
																		$html .= '</div>';
																	$html .= '</div>';
																$html .= '</div>';
															}
														}
													}
													$html .= '<input type="hidden" name="Heading_ID[' . $qtn['QuestionID'] . ']" value="'.$headerID.'">';
												$html .='</div>';
											$html .='</div>';
											if ($Answer['Answer_Status'] == 'Wait') {
												$html .='<div class="col-lg-12 decision-box">';
													$html .= '<a data-toggle="modal" data-target="#AnswerAcceptModal" onClick="setAcceptAnswerID(' . $Answer['Answer_ID'] . ')" class="acceptAnswerBtn btn btn-success btn-sm""><i class="fa fa-check"></i> Accept</a>';
													$html .= '&nbsp;&nbsp;';
													$html .= '<a data-toggle="modal" data-target="#AnswerRejectModal" onClick="setAnswerID(' . $Answer['Answer_ID'] . ')" class="rejectAnswerBtn btn btn-danger btn-sm""><i class="fa fa-times"></i> Reject</a>';
												$html .='</div>';
											} else if ($Answer['Answer_Status'] == 'Reject') {
												$html .= '<p class="help-block text-danger rej-answ">You have Rejected the answer</p>';
											} else if ($Answer['Answer_Status'] == 'Accept') {
												$html .= '<p class="help-block text-success acc-answ">You have Accepted the answer</p>';
											} else {

											}
										$html .= '</div>';
										$n++;
									}
								$html .= '</div>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';

				$m++;
			}
			$html .= '</div>';

			$tabArray = array();
			$tabContent = array();
			if($_SESSION['UserLoggedIn']['Profile_Type'] == 'demo') {
				$TabSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "tabs` WHERE `Tab_Type` = 'demo'");
			} else {
				$TabSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "tabs`");
			}
			if ( $TabSQL ) {
				if ( mysqli_num_rows($TabSQL) > 0 ) {
					while ( $checkTab = mysqli_fetch_assoc($TabSQL) ) {
						$tabArray[] 	= $checkTab['Tab_ID'];
						$tabContent[] 	= $checkTab['Tab_Content'];
						$tabName[] 		= $checkTab['Tab_Name'];
					}
					$key = array_search($TabID, $tabArray);
					if ( $key == 0 ) {
						$html .= '<div class="row buttonRow">';
							$html .= '<div class="col-lg-12">';
								$html .= '<button type="submit" class="btn btn-primary pull-right">SAVE & NEXT</button>';
							$html .= '</div>';
						$html .= '</div>';
					} elseif ( $key == count($tabArray) - 1 ) {
						$html .= '<div class="row buttonRow">';
							$html .= '<div class="col-lg-12">';
								$html .= '<button type="button" onClick="prevTab()" class="btn btn-default">BACK</button>';
								$html .= '<button type="submit" class="btn btn-primary pull-right">SAVE</button>';
								if($tabContent[$key] == 'Export'){
									$html .= '&nbsp;&nbsp;';
									$html .= '<a data-toggle="tooltip" data-title="Export DOC File" target="_blank" href="'.BASE_URL.'php/download-doc/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-warning exporttBtnDoc pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-word-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' DOC</a>';
									$html .= '&nbsp;&nbsp;';
									$html .= '<a data-toggle="tooltip" data-title="Export PDF File" target="_blank" href="'.BASE_URL.'php/download-pdf/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-info exporttBtn pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' PDF</a>';
								}
							$html .= '</div>';
						$html .= '</div>';
					} else {
						$html .= '<div class="row buttonRow">';
							$html .= '<div class="col-lg-12">';
								$html .= '<button type="button" onClick="prevTab()" class="btn btn-default">BACK</button>';
								$html .= '<button type="submit" class="btn btn-primary pull-right">SAVE & NEXT</button>';
								if($tabContent[$key] == 'Export'){
									$html .= '&nbsp;&nbsp;';
									$html .= '<a data-toggle="tooltip" data-title="Export DOC File" target="_blank" href="'.BASE_URL.'php/download-doc/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-warning exporttBtnDoc pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-word-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' DOC</a>';
									$html .= '&nbsp;&nbsp;';
									$html .= '<a data-toggle="tooltip" data-title="Export PDF File" target="_blank" href="'.BASE_URL.'php/download-pdf/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-info exporttBtn pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' PDF</a>';
								}
							$html .= '</div>';
						$html .= '</div>';
					}
				}
			}
		}
	}

	return $html;
}

function GetAnswerStatus($AnswerID) {
	global $db;

	$AnsStatus = '';
	$CheckAnswerSql = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "answers` WHERE `Answer_ID` = $AnswerID ORDER BY `Answer_On` DESC LIMIT 0, 1");
	if ( $CheckAnswerSql ) {
		$CheckAnswer = mysqli_fetch_assoc($CheckAnswerSql);
		if ( $CheckAnswer['Answer_Status'] == 'Accept' ) {
			$AnsStatus = 1;
		} elseif($CheckAnswer['Answer_Status'] == 'Reject') {
			$AnsStatus = 2;
		} elseif($CheckAnswer['Answer_Status'] == 'Wait') {
			$AnsStatus = 0;
		} else {
			$AnsStatus = 4;
		}
	}

	return $AnsStatus;
}

function GetQuestionsByRequestTabID($TabID, $ProjectID) {
	global $db;

	$html = '';
	$largeEditor = [159, 161, 163, 165, 167];
	$RequestSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Company_ID` = " . $_SESSION['UserLoggedIn']['Company_ID'] . " AND `User_ID` = " . $_SESSION['UserLoggedIn']['User_ID'] . " AND `Tab_ID` = $TabID AND `Project_ID` = $ProjectID");
	if ( $RequestSQL ) {
		if ( mysqli_num_rows($RequestSQL) > 0 ) {
			while ( $Request = mysqli_fetch_assoc($RequestSQL) ) {
				$HeadingID  = $Request['Heading_ID'];
				$QuestionID = $Request['Question_ID'];
				$headingData = GetHeadingDetails($HeadingID);
				$html .= '<div class="questionCard">';
				$html .= '<h2 class="h3">' . $headingData['Heading_Name'] . '</h2>';
				$qtnDetails = GetRequestQuestionsByHeadingId($HeadingID, $QuestionID);
				$n = 0;
				foreach ( $qtnDetails as $qtn ) {
					$Answer = GetAllAnswerByProjectIdAndCompanyId($ProjectID, $_SESSION['UserLoggedIn']['Company_ID'], $TabID, $HeadingID, $qtn['QuestionID']);

					$html .= '<div class="row">';
						$html .= '<div class="col-lg-12">';
							$html .= '<div class="form-group">';
								$html .= '<h4 class="h5">' . stripslashes($qtn['QuestionHeading']) . stripslashes(GetShortCodeReplace($qtn['QuestionName'], $ProjectID)) . '</h4>';
								if ( $qtn['QuestionType'] == 'text' ) {
									$answer = str_replace("<br>", "", $Answer['Answer']);
									$answer = str_replace("<hr>", "", $answer);
									//$answer = $Answer['Answer'];
									$html .= '<input type="text" class="form-control" value="' . $answer . '" name="answer[' . $qtn['QuestionID'] . ']" '.($Answer['Answer_Status'] == "Accept" ? "readonly" : "").' />';
								} elseif ( $qtn['QuestionType'] == 'desc' ) {
									$answer = str_replace("<br>", "", $Answer['Answer']);
									$answer = str_replace("<hr>", "", $answer);
									//$answer = $Answer['Answer'];
									if($Answer['Answer_Status'] == "Accept") {
										$html .= '<textarea class="form-control nonEditableMCE" rows="7" name="answer['.$qtn['QuestionID'].']" readonly>'.$answer.'</textarea>';
									} else {
										$editorClass = (in_array($qtn['QuestionID'], $largeEditor)) ? 'editableLargeMCE' : 'editableMCE';
										$html .= '<textarea class="form-control '.$editorClass.'" rows="7" name="answer['.$qtn['QuestionID'].']">'.$answer.'</textarea>';
									}
								} else {
									$optionArray = explode('|',$qtn['QuestionOption']);
									$html .= '<div class="row">';
										$html .= '<div class="btn-group btn-group-justified '.$qtn['QuestionOptType'].'" data-toggle="buttons">';
										foreach ( $optionArray as $key => $option ) {
											if($key == 0 && !$Answer['Answer']){
												$activeClass = 'active';
												$checkedRadio = 'checked';
											}elseif($option == $Answer['Answer']){
												$activeClass = 'active';
												$checkedRadio = 'checked';
											}else{
												$activeClass = '';
												$checkedRadio = '';
											}
											$html .= '<label class="btn btn-primary '.($Answer['Answer_Status'] == "Accept" ? "disabled" : "").' '.$activeClass.'">
												<input '.($Answer['Answer_Status'] == "Accept" ? "disabled" : "").' name="answer['.$qtn['QuestionID'].']" '.$checkedRadio.' value="'.$option.'" type="radio">'.$option.'
												</label>';
										}
										$html .= '</div>';
									$html .= '</div>';
								}
								if( $Answer['Answer_Status'] == 'Reject') {
									$html .= '<p class="help-block text-danger rej-answ-sharer">Your answer has been rejected</p>';
								} elseif ( $Answer['Answer_Status'] == 'Accept') {
									$html .= '<p class="help-block text-success acc-answ-sharer">Your answer has been accepted</p>';
								}
								$html .= '<input type="hidden" name="Heading_ID[' . $qtn['QuestionID'] . ']" value="'.$HeadingID.'">';
							$html .='</div>';
						$html .='</div>';
					$html .='</div>';
					$n++;
				}
				$html .= '</div>';
			}

			$tabArray = array();
			$TabRequestSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Company_ID` = " . $_SESSION['UserLoggedIn']['Company_ID'] . " AND `User_ID` = " . $_SESSION['UserLoggedIn']['User_ID'] . " AND `Project_ID` = " . $ProjectID . " GROUP BY `Tab_ID`");
			if ( $TabRequestSQL ) {
				if ( mysqli_num_rows($TabRequestSQL) > 0 ) {
					while ( $checkTab = mysqli_fetch_assoc($TabRequestSQL) ) {
						$tabArray[] = $checkTab['Tab_ID'];
					}
					$key = array_search($TabID, $tabArray);
					if ( count($tabArray) == 1 ) {
						$html .= '<div class="row">';
						$html .= '<div class="col-lg-12">';
						$html .= '<button type="submit" class="btn btn-primary pull-right">SAVE</button>';
						$html .= '</div>';
						$html .= '</div>';
					} else {
						if ( $key == 0 ) {
							$html .= '<div class="row">';
								$html .= '<div class="col-lg-12">';
									$html .= '<button type="submit" class="btn btn-primary pull-right">SAVE & NEXT</button>';
								$html .= '</div>';
							$html .= '</div>';
						} elseif ( $key == count($tabArray) - 1 ) {
							$html .= '<div class="row">';
								$html .= '<div class="col-lg-12">';
									$html .= '<button type="button" onClick="prevTab()" class="btn btn-default">BACK</button>';
									$html .= '<button type="submit" class="btn btn-primary pull-right">SAVE</button>';
								$html .= '</div>';
							$html .= '</div>';
						} else {
							$html .= '<div class="row">';
								$html .= '<div class="col-lg-12">';
									$html .= '<button type="button" onClick="prevTab()" class="btn btn-default">BACK</button>';
									$html .= '<button type="submit" class="btn btn-primary pull-right">SAVE & NEXT</button>';
								$html .= '</div>';
							$html .= '</div>';
						}
					}
				}
			}
		}
	}

	return $html;
}

function GetAllAnswerByProjectIdAndCompanyId($ProjectID, $CompanyID, $TabID, $HeadingID, $QuestionID) {
	global $db;

	$userSql = '';
	if ( $_SESSION['UserLoggedIn']['User_Type'] == 'request' ) {
		$userSql = " AND `User_ID` = " . $_SESSION['UserLoggedIn']['User_ID'];
	}
	$AnswerSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "answers` WHERE `Project_ID` = $ProjectID AND `Company_ID` = $CompanyID AND `Tab_ID` = $TabID AND `Heading_ID` = $HeadingID $userSql AND `Question_ID` = $QuestionID ORDER BY `Answer_On` DESC LIMIT 0,1");
	if ( $AnswerSQL ) {
		$Answers = mysqli_fetch_assoc($AnswerSQL);
		return $Answers;
	}
}

function SendNotification($notificationAry) {
   global $db;

   $count = 0;
   $fields = '';
   foreach ( $notificationAry as $col => $val ) {
      if ($count++ != 0) $fields .= ', ';
      $col = addslashes($col);
      $val = addslashes($val);
      $fields .= "`$col` = '$val'";
   }
   $query = mysqli_query($db,"INSERT INTO `" . DB_PREFIX . "notifications` SET $fields;");
}

function GetUserById($UserID) {
	global $db;

	$UserSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "users` WHERE `User_ID` = $UserID LIMIT 0, 1");
	if ( $UserSQL ) {
		$UserRow = mysqli_num_rows($UserSQL);
	} else {
		$UserRow = 0;
	}
	if ( $UserRow == 1 ) {
		$User = mysqli_fetch_assoc($UserSQL);
	} else {
		$User = array();
	}

	return $User;
}

function GetUserByEmail($Email) {
	global $db;

	$UserSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "users` WHERE `User_Email` = '$Email' LIMIT 0, 1");
	if ( $UserSQL ) {
		$UserRow = mysqli_num_rows($UserSQL);
	} else {
		$UserRow = 0;
	}
	if ( $UserRow == 1 ) {
		$User = mysqli_fetch_assoc($UserSQL);
	} else {
		$User = array();
	}

	return $User;
}

function GetProjectById($ProjectID) {
	global $db;

	$ProjectSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "projects` WHERE `Project_ID` = $ProjectID LIMIT 0, 1");
	if ( $ProjectSQL ) {
		$ProjectRow = mysqli_num_rows($ProjectSQL);
	} else {
		$ProjectRow = 0;
	}
	if ( $ProjectRow == 1 ) {
		$Project = mysqli_fetch_assoc($ProjectSQL);
	} else {
		$Project = array();
	}

	return $Project;
}

function GetUserDetailsFromCompanyID($companyID) {
	global $db;

	$companyUserSql = mysqli_query($db,"SELECT * FROM `" . DB_PREFIX . "users` WHERE `Company_ID` = $companyID AND `User_Type` = 'company'");
	if ( $companyUserSql ) {
		if ( mysqli_num_rows($companyUserSql) > 0 ) {
			$companyUser = mysqli_fetch_assoc($companyUserSql);
		} else {
			$companyUser = array();
		}
	}

	return $companyUser;
}

function GetAllTabFull($ProjectID) {
	global $db;

	$i = 1;
	$html = '';
	$questionIDAry = array();
	$TabSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "tabs`");

	$queCountSQL = mysqli_query($db, "SELECT COUNT(`Question_ID`) AS 'Question_Count' FROM `".DB_PREFIX."questions` WHERE `Question_Allias` = 'Question'");
	$queFetch = mysqli_fetch_assoc($queCountSQL);
	$queCount = $queFetch['Question_Count'];

	$ansCountSQL = mysqli_query($db, "SELECT COUNT(`Answer_ID`) AS 'Answer_Count' FROM `".DB_PREFIX."answers` WHERE `Project_ID` = ".$ProjectID);
	$ansFetch = mysqli_fetch_assoc($ansCountSQL);
	$ansCount = $ansFetch['Answer_Count'];

	if ( $TabSQL ) {
		if ( mysqli_num_rows($TabSQL) > 0 ) {
			$Tabcount = mysqli_num_rows($TabSQL);
			$html .= '<div class="item d-flex">';
			while ( $TabData = mysqli_fetch_assoc($TabSQL) ) {
				$questionCountSql = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Tab_ID` = " . $TabData['Tab_ID']);
				if ( $questionCountSql ) {
					if ( mysqli_num_rows($questionCountSql) > 0 ) {
						$questionCount = mysqli_num_rows($questionCountSql);
						while ( $question = mysqli_fetch_assoc($questionCountSql) ) {
							$questionIDAry[] = $question['Question_ID'];
						}
						$allqtn = implode(',', $questionIDAry);
						$answerSQL = mysqli_query($db,"SELECT * FROM `" . DB_PREFIX . "answers` WHERE `Project_ID` = $ProjectID AND `Company_ID` = " . $_SESSION['UserLoggedIn']['Company_ID'] . " AND `Tab_ID` = " . $TabData['Tab_ID'] . " AND `Question_ID` IN (" . $allqtn . ") AND `Answer` != ''");
						if ( $answerSQL ) {
							if ( mysqli_num_rows($answerSQL) > 0 ) {
								$answerCount = mysqli_num_rows($answerSQL);
								if ( $answerCount == count($questionIDAry) ) {
									$html .= '<div class="image"><img src="img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle"></div>';
									if ( $i % 3 == 0 ) {
										if ( $i != $Tabcount ) {
											$html .= '</div>';
											$html .= '<div class="item d-flex">';
										}
									}
									$i = $i+1;
								}
							}
						}
						$questionIDAry = array();
					}
				}
			}
			$html .= '</div>';
			if ( $i == 1 ) {
				$html .= '<div class="item d-flex">';
					$html .= 'No Answers submitted.';
				$html .= '</div>';
			}
			$html .= '<div class="item d-flex pull-left">';

			$viewTypeSQL = DB::table('share')
			->where('Project_ID', $ProjectID)
			->where('User_ID', $_SESSION['UserLoggedIn']['User_ID'])
			->first();
			if(!empty($viewTypeSQL)){
				$viewType = $viewTypeSQL->Shared_Type;
			} else {
				$viewType = '';
			}

			if($viewType=='view'){
				$html .= '<button class="view-playbook" data-url="' . BASE_URL . 'view/?view=' . safe_b64encode($ProjectID) . '">Watch Playbook</button></div>';
			} else {
				$html .= '<button class="view-playbook" data-url="' . BASE_URL . 'project/' . safe_b64encode($ProjectID) . '">View Playbook</button></div>';
			}

			if( $ansCount == $queCount ) {
				$html .= '<div class="item d-flex pull-right">';
					$html .= '<a href="#" class="ExportPDF" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'">Export PDF</a>';
				$html .= '</div>';
			}
		}
	}

	return $html;
}

function GetSharedUser($ProjectID) {
	global $db;

	$html = '';
	$i = 1;
	$RequestUserSql = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Project_ID` = $ProjectID AND `Sender_ID` = " . $_SESSION['UserLoggedIn']['User_ID'] . " GROUP BY `User_ID`");
	if ( $RequestUserSql ) {
		if ( mysqli_num_rows($RequestUserSql) > 0 ) {
			$countRequestUser = mysqli_num_rows($RequestUserSql);
			$html .= '<div class="item d-flex">';
			while ( $RequestUser = mysqli_fetch_assoc($RequestUserSql) ) {
				$User = GetUserById($RequestUser['User_ID']);
				$html .= '<div class="image"><img src="' . BASE_URL . 'img/avatar-2.jpg" data-toggle="tooltip" title="' . $User['User_Name'] . '" class="img-fluid rounded-circle"></div>';
				if ( $i % 13 == 0 ) {
					if ( $i != $Tabcount ) {
						$html .= '</div>';
						$html .= '<div class="item d-flex">';
					}
				}
				$i = $i + 1;
			}
			$html .= '</div>';
		} else {
			$html .= '<div class="item d-flex">';
				$html .= 'You Don\'t have any member in this project';
			$html .= '</div>';
		}
	}

	return $html;
}

function GetShared($ProjectID) {
	global $db;

	$html = '';
	$i = 1;
	$RequestUserSql = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Project_ID` = $ProjectID GROUP BY `User_ID`");
	if ( $RequestUserSql ) {
		if ( mysqli_num_rows($RequestUserSql) > 0 ) {
			$countRequestUser = mysqli_num_rows($RequestUserSql);
			$html .= '<div class="item d-flex">';
			while ( $RequestUser = mysqli_fetch_assoc($RequestUserSql) ) {
				if($RequestUser['User_ID']){
					$User = GetUserById($RequestUser['User_ID']);
					$html .= '<div class="image"><img src="' . BASE_URL . 'img/avatar-2.jpg" data-toggle="tooltip" title="' . $User['User_Name'] . '" class="img-fluid rounded-circle"></div>';
					if ( $i % 13 == 0 ) {
						if ( $i != $Tabcount ) {
							$html .= '</div>';
							$html .= '<div class="item d-flex">';
						}
					}
					$i = $i + 1;
				}
			}
			$html .= '</div>';
		} else {
			$html .= '<div class="item d-flex">';
				$html .= 'No member shared in this project';
			$html .= '</div>';
		}
	}

	return $html;
}

function GetQuestionByQuestionID($QuestionID) {
	global $db;

	$qtnAry = array();
	$HeadingSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Question_ID` = $QuestionID LIMIT 0, 1");
	if ( $HeadingSQL ) {
		if ( mysqli_num_rows($HeadingSQL) > 0 ) {
			$HeadingData = mysqli_fetch_assoc($HeadingSQL);
			$qtnAry['QuestionID']       	= $HeadingData['Question_ID'];
			$qtnAry['QuestionName']     	= $HeadingData['Question_Name'];
			$qtnAry['QuestionHeading']  	= $HeadingData['Question_Heading'];
			$qtnAry['QuestionType']     	= $HeadingData['Question_Type'];
			$qtnAry['QuestionOption']   	= $HeadingData['Question_option'];
			$qtnAry['QuestionOptType']  	= $HeadingData['Question_option_type'];
			$qtnAry['QuestionVideo']    	= $HeadingData['Question_Video_URL'];
			$qtnAry['QuestionAllias']   	= $HeadingData['Question_Allias'];
			$qtnAry['QuestionStr']      	= $HeadingData['Question_Str'];
			$qtnAry['QuestionRefText']  	= $HeadingData['Question_Ref_Text'];
			$qtnAry['QuestionPlaceholder']  = $HeadingData['Question_Placeholder'];
		}
	}

	return $qtnAry;
}

function GetQuestionByQuestionStr($QuestionStr) {
	global $db;

	$qtnAry = array();
	$HeadingSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Question_Str` = '$QuestionStr' LIMIT 0, 1");
	if ( $HeadingSQL ) {
		if ( mysqli_num_rows($HeadingSQL) > 0 ) {
			$HeadingData = mysqli_fetch_assoc($HeadingSQL);
			$qtnAry['QuestionID']      = $HeadingData['Question_ID'];
		}
	}

	return $qtnAry;
}

function GetHumanTime($time) {
    $time = time() - $time;
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    foreach ( $tokens as $unit => $text ) {
        if ( $time < $unit ) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
    }
}

function GetViewQuestionsByTabIDAndHeadingID($TabID, $HeadingID, $ProjectID) {
	global $db;

	$html = '';
	$distinctHeaderAry = array();
	$HeadingRef = array();
	$largeEditor = [159, 161, 163, 165, 167];
	$QuestionSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "questions` WHERE `Tab_ID` = $TabID AND `Heading_ID` = $HeadingID");
	if ( $QuestionSQL ) {
		if ( mysqli_num_rows($QuestionSQL) > 0 ) {
			$i = 0;
			$m = 0;
			while ( $QuestionData = mysqli_fetch_assoc($QuestionSQL) ) {
				if ( !in_array($QuestionData['Heading_ID'], $distinctHeaderAry) ) {
					$distinctHeaderAry[] = $QuestionData['Heading_ID'];
				}
			}
			$html .= '<div class="row first-row">';
				foreach ( $distinctHeaderAry as $headerID ) {
					$headingData = GetHeadingDetails($headerID);
					$qtnDetails  = GetQuestionsByHeadingId($headerID);
					foreach ( $qtnDetails as $qtn ) {
						$intro  = ($qtn['QuestionAllias'] == 'Overview' && $qtn['QuestionVideo'] != '') ? 'Introduction' : 'Reference';
						$refval = ($qtn['QuestionRefText'] != '') ? $qtn['QuestionRefText'] : '';
						$refval = GetShortCodeReplace($refval, $ProjectID);
					}
					$head = explode(",", $headingData['Question_Ref_ID']);
					$html .= '<div class="col-md-4">';
						$html .= '<div class="card">';
							$html .= '<div class="card-header">';
								$html .= '<h2 class="h3">'.$intro.'</h2>';
							$html .= '</div>';
							$html .= '<div class="card-body">';
								$html .= '<div class="nano">';
									$html .= '<div class="nano-content">';
										$html .= '<div class="answerWrapper">';
											if( $refval != '' ) {
												$html .= '<div class="answerArea">';
													$html .= $refval;
												$html .= '</div>';
											}
											for($r=0;$r<count($head);$r++) {
												if( $intro != 'Introduction' ) {
													if( !empty($head[$r]) ) {
														$Que = GetQuestionByQuestionID($head[$r]);
														$Ans = GetAnswerByQuestionIdAndProjectId($head[$r], $ProjectID);
														if( !empty($Ans) ) {
															$html .= '<div class="answerArea">';
																if( $Que['QuestionHeading'] ) {
																	$html .= stripslashes($Que['QuestionHeading']);
																} else {
																	$html .= stripslashes($Que['QuestionName']);
																}
																$html .= '<hr/>';
																$html .= $Ans['Answer'];
															$html .= '</div>';
														} else {
															if($refval == '') {
																$html .= '<div class="answerArea">';
																	$html .= 'No data pulled forward';
																$html .= '</div>';
															}
															break;
														}
													}
												} else {
													$html .= '<div class="answerArea">';
														$html .= 'No data pulled forward';
													$html .= '</div>';
												}
											}
										$html .= '</div>';
									$html .= '</div>';
								$html .= '</div>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
				}

				$html .= '<div class="col-md-8">';
				foreach ( $distinctHeaderAry as $headerID ) {
					$html .= '<div class="card">';
						$headingData = GetHeadingDetails($headerID);
						$qtnDetails  = GetQuestionsByHeadingId($headerID);
						/*$html .= '<div class="card-header">';
							$html .= '<h2 class="h3">';
								$html .= $headingData['Heading_Name'];
							$html .= '</h2>';
						$html .= '</div>';*/

						$n = 0;
						$html .= '<div class="card-body">';
							$html .= '<div class="nano">';
								$html .= '<div class="nano-content">';
									foreach ( $qtnDetails as $qtn ) {
										$Answer = GetAllAnswerByProjectIdAndCompanyId($ProjectID, $_SESSION['UserLoggedIn']['Company_ID'], $TabID, $headerID, $qtn['QuestionID']);
										$html .= '<div class="row question-row '.$qtn['QuestionOptType'].'">';
											$html .= '<div class="col-lg-12">';
												$html .= '<div class="form-group">';
													$CheckQtnShare = mysqli_query($db,"SELECT * FROM `" . DB_PREFIX . "requests` WHERE `Project_ID` = " . $ProjectID . " AND `Question_ID` = " . $qtn['QuestionID']);
													$disabled = CheckQuestionShared($qtn['QuestionID'], $ProjectID);
													$video    = ($qtn['QuestionAllias'] == 'Overview' && $qtn['QuestionVideo'] != '' ) ? $qtn['QuestionVideo'] : '';
													$placeholder = ' placeholder="'.$qtn['QuestionPlaceholder'].'"';
													$sharer   = GetSharedUserByQuestionIdAndProjectId($qtn['QuestionID'], $ProjectID);
													if ( !empty($sharer) ) {
														$sharerName = $sharer['User_Name'];
													}

													if ( $CheckQtnShare ) {
														if ( mysqli_num_rows( $CheckQtnShare ) > 0 ) {
															if ( $disabled != "" ) {
																$html .= '<h4 class="h5">' . stripslashes($qtn['QuestionHeading']) . stripslashes(GetShortCodeReplace($qtn['QuestionName'], $ProjectID)) . '</h4>';
															} else {
																$html .= '<h4 class="h5">' . stripslashes($qtn['QuestionHeading']) . stripslashes(GetShortCodeReplace($qtn['QuestionName'], $ProjectID));
																if( $qtn['QuestionAllias'] != 'Overview' ) {
																	if($_SESSION['UserLoggedIn']['User_Type']!='request'){
																		// $html .= '<a data-toggle="modal" data-target="#RequestModal" onClick="setQuestionID(' . $qtn['QuestionID'] . ')" class="requestBtn" title="Share"><i class="fa fa-share-alt"></i></a>';
																	}
																}
																$html .= '</h4>';
															}
															if( $video != "" ) {
																$html .= '<div class="embed-responsive embed-responsive-16by9">';
																	$html .= '<img src="' . BASE_URL . 'img/video-coming-soon.jpg" class="embed-responsive-item" />';
																	/*if( strpos($video, 'youtube') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } elseif( strpos($video, 'vimeo') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } else {
																		$html .= '<video controls webkit-playsinline class="embed-responsive-item" poster="'.BASE_URL.'img/poster.jpg"  class="tab-video">';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.mp4" type="video/mp4" />';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.webm" type="video/webm" />';
									                                    $html .= '</video>';
									                                  }*/
									                              	$html .= '</div>';
															}
															if ( $qtn['QuestionType'] == 'text' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																$html .= '<input type="text" '.$placeholder.' class="form-control" disabled name="answer[' . $qtn['QuestionID'] . ']" value="' . $answer . '" />';
																$html .= ($disabled != '') ? '<p class="help-block pull-right share-que">This Question Shared with <strong class="text-info"><i>' . $sharerName . '</i></strong></p>' : '';
															} elseif ( $qtn['QuestionType'] == 'desc' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																if($disabled != '') {
																	$html .= '<textarea '.$placeholder.' class="form-control nonEditableMCE" rows="7" disabled name="answer['.$qtn['QuestionID'].']">'.$answer.'</textarea>';
																} else {
																	$editorClass = (in_array($qtn['QuestionID'], $largeEditor)) ? 'nonEditableMCE' : 'nonEditableMCE';
																	$html .= '<textarea class="form-control '.$editorClass.'" rows="7" disabled name="answer['.$qtn['QuestionID'].']">'.$answer.'</textarea>';
																}
																$html .= ($disabled != '') ? '<p class="help-block pull-right share-que">This Question Shared with <strong class="text-info"><i>' . $sharerName . '</i></strong></p>' : '';
															} elseif ( $qtn['QuestionType'] == 'radio' ) {
																$optionArray = explode('|', $qtn['QuestionOption']);
																$html .= '<div class="row">';
																	$html .= '<div class="btn-group btn-group-justified '.$qtn['QuestionOptType'].'" data-toggle="buttons">';
																	foreach ( $optionArray as $key => $option ) {
																		if($key == 0 && !$Answer['Answer']){
																			$activeClass = 'active';
																			$checkedRadio = 'checked';
																		}elseif($option == $Answer['Answer']){
																			$activeClass = 'active';
																			$checkedRadio = 'checked';
																		}else{
																			$activeClass = '';
																			$checkedRadio = '';
																		}
																		$html .= '<label class="btn btn-lg btn-primary '.$disabled.' '.$activeClass.'">
																			<input '.($Answer['Answer_Status'] == "Accept" ? "disabled" : "").' name="answer['.$qtn['QuestionID'].']" '.$disabled.' '.$checkedRadio.' value="'.$option.'" type="radio">' . $option . '
																		</label>';
																	}
																	$html .= '</div>';
																$html .= '</div>';
																$html .= ($disabled != '') ? '<p class="help-block pull-right share-que">This Question Shared with <strong class="text-info"><i>' . $sharerName . '</i></strong></p>' : '';
															}
														} else {
															$html .= '<h4 class="h5">' . stripslashes($qtn['QuestionHeading']) . stripslashes(GetShortCodeReplace($qtn['QuestionName'], $ProjectID));
															if( $qtn['QuestionAllias'] != 'Overview' ) {
																// if($_SESSION['UserLoggedIn']['User_Type']!='request'){
																// 	$html .= '<a data-toggle="modal" data-target="#RequestModal" onClick="setQuestionID(' . $qtn['QuestionID'] . ')" class="requestBtn" title="Share"><i class="fa fa-share-alt"></i></a>';
																// }
															}
															$html .= '</h4>';
															if( $video != "" ) {
																$html .= '<div class="embed-responsive embed-responsive-16by9">';
																	$html .= '<img src="' . BASE_URL . 'img/video-coming-soon.jpg" class="embed-responsive-item" />';
																	/*if( strpos($video, 'youtube') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } elseif( strpos($video, 'vimeo') > 0 ) {
																        $html .= '<iframe src="'.$video.'" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>';
																    } else {
																		$html .= '<video controls webkit-playsinline class="embed-responsive-item" poster="'.BASE_URL.'img/poster.jpg"  class="tab-video">';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.mp4" type="video/mp4" />';
									                                      $html .= '<source src="'.BASE_URL.'video/'.$video.'.webm" type="video/webm" />';
									                                    $html .= '</video>';
									                                }*/
									                            	 $html .= '</div>';
															}
															if ( $qtn['QuestionType'] == 'text' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																$html .= '<input type="text" '.$placeholder.' class="form-control" name="answer[' . $qtn['QuestionID'] . ']" value="' . $answer . '" disabled />';
															} elseif ( $qtn['QuestionType'] == 'desc' ) {
																if(isset($Answer['Answer'])) {
																	$answer = str_replace("<br>", "", $Answer['Answer']);
																	$answer = str_replace("<hr>", "", $answer);
																	//$answer = $Answer['Answer'];
																} else {
																	$answer = ($qtn['QuestionPlaceholder']!='') ? $qtn['QuestionPlaceholder'] : 'No Answer Provided';
																}
																$editorClass = (in_array($qtn['QuestionID'], $largeEditor)) ? 'nonEditableMCE' : 'nonEditableMCE';
																$html .= '<textarea '.$placeholder.' class="form-control '.$editorClass.'" rows="7" name="answer[' . $qtn['QuestionID'] . ']" disabled>' . $answer . '</textarea>';
															} elseif ( $qtn['QuestionType'] == 'radio' ) {
																$html .= '<div class="col-md-12">';
																	$optionArray = explode('|',$qtn['QuestionOption']);
																	$html .= '<div class="row">';
																		$html .= '<div class="btn-group btn-group-justified '.$qtn['QuestionOptType'].'" data-toggle="buttons">';
																		foreach ( $optionArray as $key => $option ) {
																			if($key == 0 && !$Answer['Answer']){
																				$activeClass = 'active';
																				$checkedRadio = 'checked';
																			}elseif($option == $Answer['Answer']){
																				$activeClass = 'active';
																				$checkedRadio = 'checked';
																			}else{
																				$activeClass = '';
																				$checkedRadio = '';
																			}
																			$html .= '<label class="btn btn-lg btn-primary disabled">
																				<input '.($Answer['Answer_Status'] == "Accept" ? "disabled" : "disabled").' name="answer['.$qtn['QuestionID'].']" '.$checkedRadio.' value="'.$option.'" type="radio">' . $option . '
																			</label>';
																		}
																		$html .= '</div>';
																	$html .= '</div>';
																$html .= '</div>';
															}
														}
													}
													$html .= '<input type="hidden" name="Heading_ID[' . $qtn['QuestionID'] . ']" value="'.$headerID.'">';
												$html .='</div>';
											$html .='</div>';

										$html .= '</div>';
										$n++;
									}
								$html .= '</div>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';

				$m++;
			}
			$html .= '</div>';

			$tabArray = array();
			$tabContent = array();
			if($_SESSION['UserLoggedIn']['Profile_Type'] == 'demo') {
				$TabSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "tabs` WHERE `Tab_Type` = 'demo'");
			} else {
				$TabSQL = mysqli_query($db, "SELECT * FROM `" . DB_PREFIX . "tabs`");
			}
			if ( $TabSQL ) {
				if ( mysqli_num_rows($TabSQL) > 0 ) {
					while ( $checkTab = mysqli_fetch_assoc($TabSQL) ) {
						$tabArray[] 	= $checkTab['Tab_ID'];
						$tabContent[] 	= $checkTab['Tab_Content'];
						$tabName[] 		= $checkTab['Tab_Name'];
					}
					$key = array_search($TabID, $tabArray);
					if ( $key == 0 ) {
						$html .= '<div class="row buttonRow">';
							$html .= '<div class="col-lg-12">';
								// $html .= '<button type="submit" class="btn btn-primary pull-right">SAVE & NEXT</button>';
							$html .= '</div>';
						$html .= '</div>';
					} elseif ( $key == count($tabArray) - 1 ) {
						$html .= '<div class="row buttonRow">';
							$html .= '<div class="col-lg-12">';
								// $html .= '<button type="button" onClick="prevTab()" class="btn btn-default">BACK</button>';
								// $html .= '<button type="submit" class="btn btn-primary pull-right">SAVE</button>';
								// if($tabContent[$key] == 'Export'){
								// 	$html .= '&nbsp;&nbsp;';
								// 	$html .= '<a data-toggle="tooltip" data-title="Export DOC File" target="_blank" href="'.BASE_URL.'php/download-doc/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-warning exporttBtnDoc pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-word-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' DOC</a>';
								// 	$html .= '&nbsp;&nbsp;';
								// 	$html .= '<a data-toggle="tooltip" data-title="Export PDF File" target="_blank" href="'.BASE_URL.'php/download-pdf/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-info exporttBtn pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' PDF</a>';
								// }
							$html .= '</div>';
						$html .= '</div>';
					} else {
						$html .= '<div class="row buttonRow">';
							$html .= '<div class="col-lg-12">';
								// $html .= '<button type="button" onClick="prevTab()" class="btn btn-default">BACK</button>';
								// $html .= '<button type="submit" class="btn btn-primary pull-right">SAVE & NEXT</button>';
								// if($tabContent[$key] == 'Export'){
								// 	$html .= '&nbsp;&nbsp;';
								// 	$html .= '<a data-toggle="tooltip" data-title="Export DOC File" target="_blank" href="'.BASE_URL.'php/download-doc/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-warning exporttBtnDoc pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-word-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' DOC</a>';
								// 	$html .= '&nbsp;&nbsp;';
								// 	$html .= '<a data-toggle="tooltip" data-title="Export PDF File" target="_blank" href="'.BASE_URL.'php/download-pdf/?p='.$ProjectID.'&t='.$TabID.'" class="btn btn-info exporttBtn pull-right" data-tab="'.$TabID.'" data-company="'.$_SESSION['UserLoggedIn']['Company_ID'].'" data-project="'.$ProjectID.'"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;EXPORT '.$tabName[$key].' PDF</a>';
								// }
							$html .= '</div>';
						$html .= '</div>';
					}
				}
			}
		}
	}

	return $html;
}

function Send_Mail($subject, $body, $recipient_name, $recipient_mail, $cc = FALSE, $bcc = FALSE, $attachment = FALSE, $debug = FALSE) {
    global $PHPMailer;

    $PHPMailer->isSMTP();                                                           // Set mailer to use SMTP
    $PHPMailer->Host        = PHPMAILER_HOST;                                       // Specify main and backup server
    $PHPMailer->Port        = PHPMAILER_PORT;
    $PHPMailer->SMTPAuth    = TRUE;                                                 // Enable SMTP authentication
    $PHPMailer->Username    = PHPMAILER_USERNAME;                                   // SMTP username
    $PHPMailer->Password    = PHPMAILER_PASSWORD;                                   // SMTP password
    $PHPMailer->SMTPSecure  = PHPMAILER_SMTPSECURE;                                 // Enable encryption, 'ssl' also accepted
    $PHPMailer->From        = PHPMAILER_FROM;
    $PHPMailer->FromName    = PHPMAILER_FROMNAME;
    $PHPMailer->smtpConnect([
        'ssl' => [
            'verify_peer'       => FALSE,
            'verify_peer_name'  => FALSE,
            'allow_self_signed' => TRUE
        ]
    ]);
    if ( is_array($recipient_name) && is_array($recipient_mail) ) {
        for ( $i = 0; $i < count($recipient_mail); $i++ ) {
            $PHPMailer->addAddress($recipient_mail[$i], $recipient_name[$i]);       // Add multiple recipient
        }
    } else {
        $PHPMailer->addAddress($recipient_mail, $recipient_name);                   // Add a recipient
    }
    $PHPMailer->addReplyTo(PHPMAILER_FROM, PHPMAILER_FROMNAME);                     // Reply to email and name
    if ( $cc ) {                                                                // Add CC recipient
        if ( is_array($cc) ) {
            for ( $i = 0; $i < count($cc); $i++ ) {
                $PHPMailer->addCC($cc[$i]);                                         // Add multiple CC recipient
            }
        } else {
            $PHPMailer->addCC($cc);                                                 // Add single CC recipient
        }
    }
    if ( $bcc ) {                                                               // Add BCC recipient
        if ( is_array($bcc) ) {
            for ( $i = 0; $i < count($bcc); $i++ ) {
                $PHPMailer->addBCC($bcc[$i]);                                       // Add multiple BCC recipient
            }
        } else {
            $PHPMailer->addBCC($bcc);                                               // Add single BCC recipient
        }
    }
    $PHPMailer->WordWrap    = PHPMAILER_WORDWRAP;                                   // Set word wrap to 50 characters
    if ( $attachment ) {
        for ( $i = 0; $i < count($attachment); $i++ ) {
            $PHPMailer->addAttachment($attachment[$i]['src'], $attachment[$i]['name']);
        }
    }
    $PHPMailer->isHTML(TRUE);                                                       // Set email format to HTML

    if ( $debug ) {
        $PHPMailer->SMTPDebug   = 2;                                                // Enable debugging mode
    }

    $PHPMailer->Subject         = $subject;
    $PHPMailer->Body            = $body;
    if ( !$PHPMailer->send() ) {
        return 'Message could not be sent.';
        return 'Mailer Error: ' . $PHPMailer->ErrorInfo;
        exit;
    }
    return 'Message has been sent';
}

function redirect($url) {
	return header('location:'.$url);
	exit;
}
