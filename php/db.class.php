<?php 

class DB_OLD {
	
	protected $db_name = DB_NAME;
	protected $db_user = DB_USER;
	protected $db_pass = DB_PASSWORD;
	protected $db_host = DB_HOST;
	
	public function connect() {
	
		$con = mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
		if ( mysqli_connect_errno() ) {
			printf("Connection failed: %s\ ", mysqli_connect_error());
			exit();
		}
		return $con;
	}

}

$con = new DB_OLD();
$db = $con->connect();