<?php
include(dirname(__FILE__) . '/config.php');
$creator = $_GET['creator'];
$html = '';
if($_SESSION['UserLoggedIn']['User_Type']=='company'){
  $membersSQL = DB::table('users')
  ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
  ->where('User_ID', '!=', $creator)
  ->get();

  $html .= '<option value="">Select Member</option>';
  foreach($membersSQL as $member) {
    if($member->User_Type == 'department') {
      $type = 'Department';
    } elseif($member->User_Type == 'user') {
      $type = 'User';
    } elseif($member->User_Type == 'request') {
      $type = 'Contributor';
    }
    $html .= '<option value="'.$member->User_ID.'">'.$member->User_Name.' ('.$type.')</option>';
  }

} elseif($_SESSION['UserLoggedIn']['User_Type']=='department'){
  $membersSQL = DB::table('users')
  ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
  ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
  ->where('User_ID', '!=', $creator)
  ->get();

  $html .= '<option value="">Select Member</option>';
  foreach($membersSQL as $member) {
    if($member->User_Type == 'user') {
      $type = 'User';
    } elseif($member->User_Type == 'department') {
      $type = 'Department';
    } elseif($member->User_Type == 'request') {
      $type = 'Contributor';
    }
    $html .= '<option value="'.$member->User_ID.'">'.$member->User_Name.' ('.$type.')</option>';
  }

} elseif($_SESSION['UserLoggedIn']['User_Type']=='user'){
  $membersSQL = DB::table('users')
  ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
  ->where(function($query){
    $query->where('User_Type', 'user');
    $query->Where('Master_ID', $_SESSION['UserLoggedIn']['Master_ID']);
  })
  ->orWhere(function($query){
    $query->where('User_Type', 'request');
    $query->Where('Master_ID', $_SESSION['UserLoggedIn']['User_ID']);
  })
  ->where('User_ID', '!=', $creator)
  ->get();

  $html .= '<option value="">Select Member</option>';
  foreach($membersSQL as $member) {
    if($member->User_Type == 'user') {
      $type = 'User';
    } elseif($member->User_Type == 'department') {
      $type = 'Department';
    } elseif($member->User_Type == 'request') {
      $type = 'Contributor';
    }
    $html .= '<option value="'.$member->User_ID.'">'.$member->User_Name.' ('.$type.')</option>';
  }

} elseif($_SESSION['UserLoggedIn']['User_Type']=='individual'){
  $membersSQL = DB::table('users')
  ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
  ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
  ->where('User_ID', '!=', $creator)
  ->get();

  $html .= '<option value="">Select Member</option>';
  foreach($membersSQL as $member) {
    if($member->User_Type == 'request') {
      $type = 'Contributor';
    }
    $html .= '<option value="'.$member->User_ID.'">'.$member->User_Name.' ('.$type.')</option>';
  }
  
}
echo $html;
