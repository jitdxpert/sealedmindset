<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
$notificationAry = [];
if(empty($_POST['memberId'])) {
	$res['code'] = 5;
	$res['text'] = 'All fields are required.';
	goto RESPONSE;
}

$memberId   = addslashes($_POST['memberId']);
$user			  = GetUserById($memberId);
$name       = $user['User_Name'];
$email      = $user['User_Email'];
$projectId  = addslashes($_POST['projectId']);
$project 	  = GetProjectById($projectId);
$companyId  = addslashes($_SESSION['UserLoggedIn']['Company_ID']);

$User = DB::table('users')->where('User_Email', $email)->first();
if(!empty($User)) {
	$checkProjectSQL = DB::table('projects')
	->where('Project_Name', $project['Project_Name'])
	->where('Master_ID', $memberId)
	->first();
	if(!empty($checkProjectSQL)) {
		$res['code'] = 6;
		$res['text'] = 'This Playbook already exported to this user.';
		goto RESPONSE;
	} else {
		$subject  = 'A Playbook Imported';
		$body = '<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#FCFCFD url(' . BASE_URL . 'images/body-bg.png) repeat 0 0;border:1px solid rgba(0, 0, 0, 0.15);font-family:Verdana,sans-serif;">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="75" align="center" style="background:rgba(0, 0, 0, 0.15) repeat;border-bottom: 1px solid rgba(0, 0, 0, 0.15);">
								<img width="200" alt="" src="' . BASE_URL . 'images/email-logo.png" alt="SealedMindset" />
							</td>
						</tr>
						<tr>
							<td align="left" valign="top" style="padding-left:20px;">
								<font color="#0e59ac" size="2.5">
									<br /><br /><span>Dear ' . $User->User_Name . ',<br /><br />A new playbook has been imported to your Sealed Mindset account.<br />Please login to your account to check the playbook.</span>
								</font><br /><br /><br /><br />
							</td>
						</tr>
						<tr>
							<td>
								<hr>
								<br />
							</td>
						</tr>
						<tr>
							<td align="left" valign="top" style="padding-left:20px;">
								<font color="#000" size="2">
									<span>In case if you have any questions, please send an Email to : <a style="color:#41a5e1;text-decoration:none;" href="mailto:support@sealedmindset.com">support@sealedmindset.com</a></span>
								</font><br /><br />
							</td>
						</tr>
						<tr>
							<td align="left" valign="top" style="padding-left:20px;">
								<font color="#000" size="2">
									<span>Regards,<br /><br />The <a href="'.BASE_URL.'">SealedMindset</a> Team.</span>
								</font><br /><br />
							</td>
						</tr>
						<tr>
							<td align="center">
								<font color="#aaa" size="2">
										<span>P.S: This is a system generated email. Please do not reply.</span><br /><br />
								</font>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>';
		$send = Send_Mail($subject, $body, $User->User_Name, $User->User_Email);
		if(!$send) {
			$res['code'] = 2;
			$res['text'] = 'Unable to Export, please try again.';
			goto RESPONSE;
		}

		$newProjectId = DB::table('projects')->insertGetId([
				'Company_ID' 		 		 => $User->Company_ID,
				'Master_ID'					 => $User->User_ID,
				'Project_Name' 		 	 => $project['Project_Name'],
				'Project_Department' => $project['Project_Department'],
				'Project_Deadline'   => $project['Project_Deadline'],
				'Project_Goals' 	 	 => $project['Project_Goals'],
				'Project_CreatedOn'  => date('Y-m-d H:i:s')
		]);
		if(!$newProjectId) {
			$res['code'] = 1;
			$res['text'] = 'Something went wrong, please try again.';
			goto RESPONSE;
		}

		$Sender  = GetUserById($_SESSION['UserLoggedIn']['User_ID']);
		SendNotification([
			'Project_ID'        => $newProjectId,
			'Company_ID'        => $companyId,
			'Sender_ID'         => $_SESSION['UserLoggedIn']['User_ID'],
			'User_ID'           => $User->User_ID,
			'Tab_ID'            => 0,
			'Heading_ID'        => 0,
			'Question_ID'       => 0,
			'Noti_Message_Type' => 'playbook export',
			'Noti_Message'      => '<b>'.$Sender['User_Name'].'</b> exported a playbook to you.',
			'Noti_PostedOn'     => date('Y-m-d H:i:s')
		]);
		$eventName = $Sender['User_Name'].' exported a playbook to '.$User->User_Name;
		DB::table('historys')->insert([
			'Project_ID'	=> $projectId,
			'Company_ID'	=> $companyId,
			'Tab_ID'			=> 0,
			'Heading_ID'	=> 0,
			'Question_ID'	=> 0,
			'Sender_ID'		=> $_SESSION['UserLoggedIn']['User_ID'],
			'Receiver_ID'	=> $User->User_ID,
			'Event_Name'	=> $eventName,
			'Event_On'		=> date('Y-m-d H:i:s')
		]);
		$res['code'] = 0;
		$res['text'] = 'Playbook exported successfully.';
		goto RESPONSE;
	}
}

RESPONSE:
echo json_encode($res);
