<?php
include(dirname(__FILE__) . '/config.php');

if(!$_GET['file']) {
	redirect(BASE_URL);
}
$fileName = $_GET['file'];
$pathName = dirname(dirname(__FILE__)).'/doc/';

$newString = file_get_contents($pathName.$fileName.'.txt');

header("Content-Type: application/vnd.msword");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=".$fileName.".doc");

echo $newString;