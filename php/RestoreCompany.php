<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['restoreCompanyID'])) {
	$res['code'] = 2;
	$res['text'] = 'Something went wrong. Please try again later!';
	goto RESPONSE;
}

$compID  = $_POST['restoreCompanyID'];
$userVAL = DB::table('users')->where('Company_ID', '=', $compID)->first();
$userID  = $userVAL->User_ID;

$query = DB::table('companies')->where('Company_ID', '=', $compID)->update(['Trash' => 0]);
if(!$query) {
	$res['code'] = 1;
	$res['text'] = 'Oops! Unable to remove. Try again later!';
	goto RESPONSE;
} else {
	DB::table('notifications')->where('Company_ID', '=', $compID)->update(['Trash' => 0]);
	DB::table('projects')->where('Company_ID', '=', $compID)->update(['Trash' => 0]);
	DB::table('historys')->where('Company_ID', '=', $compID)->update(['Trash' => 0]);
	DB::table('answers')->where('Company_ID', '=', $compID)->update(['Trash' => 0]);
	DB::table('users')->where('User_ID', '=', $userID)->update(['Trash' => 0]);
	
	$res['code'] = 0;
	$res['text'] = 'Company data Successfully restored!';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
