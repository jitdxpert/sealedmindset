<?php
include(dirname(__FILE__) . '/config.php');

$res  = [];
$html = '';
if(!isset($_POST['company']) && !isset($_POST['project']) && !isset($_POST['tab'])) {
	$res['code'] = 1;
	$res['text'] = 'Sorry! Unable to export the Document.';
	goto RESPONSE;
}

$company = $_POST['company'];
$project = $_POST['project'];
$tab 	 = $_POST['tab'];

$pdfContent = DB::table('pdf_content')->where('PDF_Tab_ID', '=', $tab)->first();
if(empty($pdfContent)) {
	$res['code'] = 1;
	$res['text'] = 'Sorry! Unable to find content for the Document.';
	goto RESPONSE;
} else {
	$string = $pdfContent->PDF_Content_Doc;

	$newString = GetShortCodeReplace($string, $project);
	$pathName = dirname(dirname(__FILE__)).'/doc/';
	$fileName = 'SealedMindset_'.strtotime(date('Y-m-d H:i:s')).'_'.safe_b64encode($project);
	
	file_put_contents($pathName.$fileName.'.txt', $newString);
	$res['code'] = 0;
	$res['doc']  = BASE_URL.'php/DownloadDoc/?file='.$fileName;
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);