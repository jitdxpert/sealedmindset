<?php
include(dirname(__FILE__) . '/config.php');

$res  = [];
$html = '';
if(!isset($_POST['company']) && !isset($_POST['project']) && !isset($_POST['tab'])) {
	$res['code'] = 1;
	$res['text'] = 'Sorry! Unable to export the PDF.';
	goto RESPONSE;
}

$company = $_POST['company'];
$project = $_POST['project'];
$tab 	 = $_POST['tab'];

$pdfContent = DB::table('pdf_content')->where('PDF_Tab_ID', '=', $tab)->first();
if(empty($pdfContent)) {
	$res['code'] = 1;
	$res['text'] = 'Sorry! Unable to find content for the PDF.';
	goto RESPONSE;
} else {
	$string = $pdfContent->PDF_Content;

	$newString = GetShortCodeReplace($string, $project);
	$newString = mb_convert_encoding($newString, 'HTML-ENTITIES', 'UTF-8');

	$pathName = dirname(dirname(__FILE__)).'/pdf/';
	$fileName = 'SealedMindset_'.strtotime(date('Y-m-d H:i:s')).'_'.safe_b64encode($project).'.pdf';

	$dompdf = new Dompdf\Dompdf();
	$dompdf->loadHtml($newString);
	$dompdf->setPaper('A4');
	$dompdf->render();

	file_put_contents($pathName.$fileName, $dompdf->output());
	$res['code'] = 0;
	$res['pdf']  = BASE_URL.'pdf/'.$fileName;
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);