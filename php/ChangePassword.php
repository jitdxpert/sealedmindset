<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['old_password']) && empty($_POST['new_password']) && empty($_POST['con_password']) ) {
	$res['code'] = 1;
	$res['text'] = 'Server busy try again later.';
	goto RESPONSE;
}

$old_password = addslashes($_POST['old_password']);
$new_password = addslashes($_POST['new_password']);
$con_password = addslashes($_POST['con_password']);
$user_id 	  = addslashes($_POST['user_id']);
$User  		  = GetUserById($user_id);

$checkOldPassSQL = DB::table('users')->where('User_Password', '=', sha1($old_password))->where('User_ID', '=', $user_id)->first();
if($con_password != $new_password) {
	$res['code'] = 1;
	$res['text'] = 'Confirm Password doesn\'t match.';
	goto RESPONSE;
}

if(empty($checkOldPassSQL)) {
	$res['code'] = 1;
	$res['text'] = 'Your Old Password doesn\'t match.';
	goto RESPONSE;
}

$passwordSQL = DB::table('users')->where('User_ID', '=', $user_id)->update(['User_Password' => sha1($new_password)]);
if(!$passwordSQL) {
	$res['code'] = 1;
	$res['text'] = 'Password couldn\'t updated.';
	goto RESPONSE;
}

$subject  = 'Your SealedMindset password has been changed.';
$body = '<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#FCFCFD url(' . BASE_URL . 'images/body-bg.png) repeat 0 0;border:1px solid rgba(0, 0, 0, 0.15);font-family:Verdana,sans-serif;">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="75" align="center" style="background:rgba(0, 0, 0, 0.15) repeat;border-bottom: 1px solid rgba(0, 0, 0, 0.15);">
						<img width="200" alt="" src="' . BASE_URL . 'images/email-logo.png" alt="SealedMindset" />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#0e59ac" size="2.5">
							<br /><br /><span>Dear ' . $User['User_Name'] . ',<br /><br />You have successfully changed your password and now you can login with your new password.</span>
						</font><br /><br /><br /><br />
					</td>
				</tr>
				<tr>
					<td>
						<hr>
						<br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>In case if you have any questions, please send an Email to : <a style="color:#41a5e1;text-decoration:none;" href="mailto:support@sealedmindset.com">support@sealedmindset.com</a></span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>Regards,<br /><br />The <a href="'.BASE_URL.'">SealedMindset</a> Team.</span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="center">
						<font color="#aaa" size="2">
								<span>P.S: This is a system generated email. Please do not reply.</span><br /><br />
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>';
$Send = Send_Mail($subject, $body, $User['User_Name'], $User['User_Email']);
if($Send) {
	$res['code'] = 0;
	$res['text'] = 'Password successfully updated.';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);