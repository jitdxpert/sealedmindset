<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['deleteCompanyID'])) {
	$res['code'] = 2;
	$res['text'] = 'Something went wrong. Please try again later!';
	goto RESPONSE;
}

$compID  = $_POST['deleteCompanyID'];
$userVAL = DB::table('users')->where('Company_ID', '=', $compID)->first();
$userID  = $userVAL->User_ID;

$query = DB::table('companies')->where('Company_ID', '=', $compID)->delete();
if(!$query) {
	$res['code'] = 1;
	$res['text'] = 'Oops! Unable to remove. Try again later!';
	goto RESPONSE;
} else {
	DB::table('notifications')->where('Company_ID', '=', $compID)->update(['Trash' => 1]);
	DB::table('projects')->where('Company_ID', '=', $compID)->update(['Trash' => 1]);
	DB::table('historys')->where('Company_ID', '=', $compID)->update(['Trash' => 1]);
	DB::table('answers')->where('Company_ID', '=', $compID)->update(['Trash' => 1]);
	DB::table('users')->where('User_ID', '=', $userID)->update(['Trash' => 1]);

	$res['code'] = 0;
	$res['text'] = 'Company data Successfully removed!';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
