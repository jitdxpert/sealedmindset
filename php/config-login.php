<?php
// ** MySQL Settings - You can get this info from your web host ** //
define('DB_NAME', 		'sealedmindset');   					//sealedmindset
define('DB_USER', 		'root');            					//sealedmindset
define('DB_PASSWORD', 	'');                					//sealedmindset
define('DB_HOST', 		'localhost');       					//localhost
define('DB_CHARSET', 	'utf8');
define('DB_COLLATE', 	'utf8_unicode_ci');
define('DB_PREFIX', 	'sm_');

$BASE_URL = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/';

// ** Application BASE URL ** //
define('SITE_TITLE', 	'Sealed Mindset');      			//Website Title
define('BASE_URL', 		$BASE_URL);    						//http://soumitrapaul.com/sealedmindset/
define('ADMIN_URL', 	BASE_URL . 'admin/');   			//http://soumitrapaul.com/sealedmindset/admin/

// ** Database Class API  ** //
include_once(dirname(__FILE__) . "/orm/boot.php");

// ** Pagination Class ** //
include(dirname(__FILE__) . "/Pagination.php");

// ** Helper Class File ** //
include(dirname(__FILE__) . "/functions.php");

// ** Session Start ** //
session_start();

// ** Application Environment ** //
define('DEBUG', true);

if ( DEBUG == true ) {
	ini_set( "display_errors", 1 );
} else {
	ini_set( "display_errors", 0 );
}
