<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['project']) && empty($_POST['department']) && empty($_POST['deadline']) && empty($_POST['goals'])) {
	$res['code'] = 4;
	$res['text'] = 'All fields are required.';
	goto RESPONSE;
}

$companyId  = $_SESSION['UserLoggedIn']['Company_ID'];
$masterId  	= $_SESSION['UserLoggedIn']['User_ID'];
$project    = addslashes($_POST['project']);
$department = addslashes($_POST['department']);
$deadline   = addslashes($_POST['deadline']);
$goals      = addslashes($_POST['goals']);

$CheckProjectSQL = DB::table('projects')->where('Project_Name', '=', $project)->where('Company_ID', '=', $_SESSION['UserLoggedIn']['Company_ID'])->first();
if(!empty($CheckProjectSQL)) {
	$res['code'] = 2;
	$res['text'] = 'Project is already exist with this name.';
	goto RESPONSE;
}

$projectID = DB::table('projects')->insertGetId(
	[
		'Company_ID' 		 		 => $companyId,
		'Master_ID'					 => $masterId,
		'Project_Name' 		 	 => $project,
		'Project_Department' => $department,
		'Project_Deadline'   => $deadline,
		'Project_Goals' 	 	 => $goals,
		'Project_CreatedOn'  => date('Y-m-d H:i:s')
	]
);

if($projectID) {
	$res['code'] = 0;
	$res['url']  = BASE_URL . 'project/' . safe_b64encode($projectID);
	$res['text'] = 'Project has been successfully created.';
	goto RESPONSE;
} else {
	$res['code'] = 1;
	$res['text'] = 'Something went wrong, please try again.';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
