<?php
include(dirname(__FILE__) . '/config.php');

$res 	= [];
$status = [];
$html 	= '';
$notificationAry = [];
$distinctHeaderAry = [];
$reference  = [];

$Project_ID = $_POST['Project_ID'];
$Project 	  = GetProjectById($Project_ID);
$Company_ID = $_SESSION['UserLoggedIn']['Company_ID'];
$User_ID    = $_SESSION['UserLoggedIn']['User_ID'];
$Tab_ID     = $_POST['Tab_ID'];
$headingAry = isset($_POST['Heading_ID']) ? $_POST['Heading_ID'] : [];
$answerAry  = isset($_POST['answer']) ? $_POST['answer'] : [];

foreach($answerAry as $qtnID=>$answer) {
	//$answer = str_replace("\n", "<br>", $answer);
	$AnswerDetail = DB::table('answers')
	->where('Project_ID', '=', $Project_ID)
	->where('User_ID', '=', $User_ID)
	->where('Tab_ID', '=', $Tab_ID)
	->where('Heading_ID', '=', $headingAry[$qtnID])
	->where('Question_ID', '=', $qtnID)
	->first();
	if(!empty($AnswerDetail)) {
		// UPDATE ANSWER SECTION
		if($AnswerDetail->Answer_Status == 'Reject') {
			$AnsUpdate = DB::table('answers')
			->where('Project_ID', '=', $Project_ID)
			->where('User_ID', '=', $User_ID)
			->where('Tab_ID', '=', $Tab_ID)
			->where('Heading_ID', '=', $headingAry[$qtnID])
			->where('Question_ID', '=', $qtnID)
			->update([
				'Answer' 		=> addslashes($answer),
				'Answer_Status' => 'Wait',
				'Answer_On' 	=> date('Y-m-d H:i:s')
			]);
			if(!$AnsUpdate) {
				$status[] = 1;
			} else {
				$Question = GetQuestionByQuestionID($qtnID);
				$Sender   = GetUserById($User_ID);
				$receiver = GetUserById($Project['Master_ID']);

				$eventName = $Sender['User_Name'].' again replied an answer of '.$receiver['User_Name'].' for question: <br>'.$Question['QuestionName'];
				DB::table('historys')->insert([
					'Project_ID'	=> $Project_ID,
					'Company_ID'	=> $Company_ID,
					'Tab_ID'			=> $Tab_ID,
					'Heading_ID'	=> $headingAry[$qtnID],
					'Question_ID'	=> $qtnID,
					'Sender_ID'		=> $Sender['User_ID'],
					'Receiver_ID'	=> $receiver['User_ID'],
					'Event_Name'	=> $eventName,
					'Answer'		=> addslashes($answer),
					'Answer_Status'	=> 'Reply',
					'Event_On'		=> date('Y-m-d H:i:s')
				]);

				SendNotification([
					'Project_ID'        => $Project_ID,
					'Company_ID'        => $Company_ID,
					'Sender_ID'         => $Sender['User_ID'],
					'User_ID'           => $receiver['User_ID'],
					'Tab_ID'            => $Tab_ID,
					'Heading_ID'        => $headingAry[$qtnID],
					'Question_ID'       => $qtnID,
					'Noti_Message_Type' => 'answer reply',
					'Noti_Message'      => '<b>'.$Sender['User_Name'].'</b> again replied answer for question: <br>'.$Question['QuestionName'],
					'Noti_PostedOn'     => date('Y-m-d H:i:s'),
				]);
				$status[] = 0;
			}
		} else {
			$AnsUpdate = DB::table('answers')
			->where('Project_ID', '=', $Project_ID)
			->where('User_ID', '=', $User_ID)
			->where('Tab_ID', '=', $Tab_ID)
			->where('Heading_ID', '=', $headingAry[$qtnID])
			->where('Question_ID', '=', $qtnID)
			->update([
				'Answer' 	=> addslashes($answer),
				'Answer_On' => date('Y-m-d H:i:s')
			]);
			if($AnsUpdate) {
				$status[] = 0;
			} else {
				$status[] = 1;
			}
		}
	} else {
		// INSERT ANSWER SECTION
		$answerID = DB::table('answers')->insertGetId([
			'Project_ID'  => $Project_ID,
			'Company_ID'  => $Company_ID,
			'User_ID'	  	=> $User_ID,
			'Tab_ID'	  	=> $Tab_ID,
			'Heading_ID'  => $headingAry[$qtnID],
			'Question_ID' => $qtnID,
			'Answer'	  	=> addslashes($answer),
			'Answer_On'	  => date('Y-m-d H:i:s')
		]);
		if(!$answerID) {
			$status[] = 1;
		} else {
			if($_SESSION['UserLoggedIn']['User_Type'] == 'request') {
				$answerUpdate = DB::table('answers')->where('Answer_ID', '=', $answerID)->update(['Answer_Status' => 'Wait']);
				$Sender   = GetUserById($User_ID);
				$receiver = GetUserById($Project['Master_ID']);
				$Question = GetQuestionByQuestionID($qtnID);
				if($answerUpdate) {
					$eventName = $Sender['User_Name'].' replied  an answer for <b>'.$Question['QuestionName'].'</b> with '.$receiver['User_Name'];
					DB::table('historys')->insert([
						'Project_ID'	=> $Project_ID,
						'Company_ID'	=> $Company_ID,
						'Tab_ID'			=> $Tab_ID,
						'Heading_ID'	=> $headingAry[$qtnID],
						'Question_ID'	=> $qtnID,
						'Sender_ID'		=> $Sender['User_ID'],
						'Receiver_ID'	=> $receiver['User_ID'],
						'Event_Name'	=> $eventName,
						'Answer'			=> addslashes($answer),
						'Answer_Status'	=> 'Reply',
						'Event_On'		=> date('Y-m-d H:i:s')
					]);
				}

				DB::table('requests')
				->where('Project_ID', '=', $Project_ID)
				->where('Company_ID', '=', $Company_ID)
				->where('User_ID', '=', $User_ID)
				->where('Tab_ID', '=', $Tab_ID)
				->where('Heading_ID', '=', $headingAry[$qtnID])
				->where('Question_ID', '=', $qtnID)
				->update(['Answer_ID' => $answerID]);

				$checkReplyExist = DB::table('answers')
				->where('Project_ID', '=', $Project_ID)
				->where('Company_ID', '=', $Company_ID)
				->where('User_ID', '=', $User_ID)
				->where('Tab_ID', '=', $Tab_ID)
				->where('Heading_ID', '=', $headingAry[$qtnID])
				->where('Question_ID', '=', $qtnID)
				->get();

				if(!empty($checkReplyExist)) {
					DB::table('answers')
					->where('Project_ID', '=', $Project_ID)
					->where('Company_ID', '=', $Company_ID)
					->where('User_ID', '=', $User_ID)
					->where('Tab_ID', '=', $Tab_ID)
					->where('Heading_ID', '=', $headingAry[$qtnID])
					->where('Question_ID', '=', $qtnID)
					->update(['Answer_Status' => 'Wait']);
				}

				SendNotification([
					'Project_ID'        => $Project_ID,
					'Company_ID'        => $Company_ID,
					'Sender_ID'         => $Sender['User_ID'],
					'User_ID'           => $receiver['User_ID'],
					'Tab_ID'            => $Tab_ID,
					'Heading_ID'        => $headingAry[$qtnID],
					'Question_ID'       => $qtnID,
					'Noti_Message_Type' => 'answer reply',
					'Noti_Message'      => '<b>'.$Sender['User_Name'].'</b> replied answer for question: <b>'.$Question['QuestionName'].'</b>',
					'Noti_PostedOn'     => date('Y-m-d H:i:s'),
				]);
			}
			$status[] = 0;
		}
	}
}

if(!in_array(1, $status)) {
	$res['code'] = 0;
	$res['text'] = 'Answer Successfully Stored';
	goto RESPONSE;
} else {
	$res['code'] = 1;
	$res['text'] = 'Some Answer faced error';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
