<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['editCompanyName']) && empty($_POST['editCompanyOwner']) && empty($_POST['editCompanyID']) && empty($_POST['editUserID'])) {
	$res['code'] = 7;
	$res['text'] = 'All fields are required.';
	goto RESPONSE;
}

$uid    = addslashes($_POST['editUserID']);
$cid    = addslashes($_POST['editCompanyID']);
$name   = addslashes($_POST['editCompanyName']);
$owner  = addslashes($_POST['editCompanyOwner']);
$type   = addslashes($_POST['editCompanyType']);
$pass   = addslashes($_POST['editUserPassword']);

$CheckUserSQL = DB::table('users')->where('User_ID', '=', $uid)->where('Company_ID', '=', $cid)->first();
if(empty($CheckUserSQL)) {
	$res['code'] = 4;
	$res['text'] = 'No user found in database. Please try again later.';
	goto RESPONSE;
}

if(!empty($pass)) {
	$update  = [
		'User_Name' 	 	 => $owner,
		'User_Password'	 => sha1($pass),
		'Profile_Type' 	 => $type
	];
	$subject  = 'Your SealedMindset password has been changed.';
	$body = '<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#FCFCFD url(' . BASE_URL . 'images/body-bg.png) repeat 0 0;border:1px solid rgba(0, 0, 0, 0.15);font-family:Verdana,sans-serif;">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="75" align="center" style="background:rgba(0, 0, 0, 0.15) repeat;border-bottom: 1px solid rgba(0, 0, 0, 0.15);">
							<img width="200" alt="" src="' . BASE_URL . 'images/email-logo.png" alt="SealedMindset" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding-left:20px;">
							<font color="#0e59ac" size="2.5">
								<br /><br /><span>Dear ' . $owner . ',<br /><br />Your account password has been reset- it is provided below. Please log in and change it to your desired password.</span>
							</font><br /><br /><br /><br />
						</td>
					</tr>
					<tr>
						<td>
							<hr>
							<br />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding-left:20px;">
							<font color="#0e59ac" size="3">
								<span>
									New Password: <strong>' . $pass . '</strong>
								</span>
							</font><br /><br />
							<font color="#161616" size="2">
								<span>Please keep this information safe and secure.</span>
							</font><br /><br />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding-left:20px;">
							<font color="#000" size="2">
								<span>In case if you have any questions, please send an Email to : <a style="color:#41a5e1;text-decoration:none;" href="mailto:support@sealedmindset.com">support@sealedmindset.com</a></span>
							</font><br /><br />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" style="padding-left:20px;">
							<font color="#000" size="2">
								<span>Regards,<br /><br />The <a href="'.BASE_URL.'">SealedMindset</a> Team.</span>
							</font><br /><br />
						</td>
					</tr>
					<tr>
						<td align="center">
							<font color="#aaa" size="2">
									<span>P.S: This is a system generated email. Please do not reply.</span><br /><br />
							</font>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>';
	$Send = Send_Mail($subject, $body, $owner, $CheckUserSQL->User_Email);
	if(!$Send) {
		$res['code'] = 1;
		$res['text'] = 'Sorry, unable to send mail to User.';
		goto RESPONSE;
	}
} else {
	$update  = [
		'User_Name' 	 => $owner,
		'Profile_Type' => $type
	];
}
$userSQL = DB::table('users')->where('User_ID', $uid)->update($update);
$compSQL = DB::table('companies')->where('Company_ID', $cid)->update([
	'Company_Name' 	=> $name,
	'Company_Owner'	=> $owner,
	'Profile_Type'	=> $type,
]);

$memberSQL = DB::table('users')->where('Company_ID', $cid)->get();
if(!empty($memberSQL)) {
	foreach($memberSQL as $member) {
		DB::table('users')->where('User_ID', $member->User_ID)->update(['Profile_Type' => $type]);
	}
}

if(!$userSQL && !$compSQL) {
	$res['code'] = 2;
	$res['text'] = 'Sorry, unable to update the account.';
	goto RESPONSE;
} else {
	$res['code'] = 0;
	$res['text'] = 'Company details successfully updated.';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
