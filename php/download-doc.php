<?php
include(dirname(__FILE__) . '/config.php');

$company = $_SESSION['UserLoggedIn']['Company_ID'];
$project = isset($_GET['p']) ? $_GET['p'] : 0;
$tab 	 = isset($_GET['t']) ? $_GET['t'] : 0;

if(!$company && !$project && !$tab) {
	die('Sorry! Unable to export the Document.');
}

$pdfContent = DB::table('pdf_content')->where('PDF_Tab_ID', '=', $tab)->first();
if(empty($pdfContent)) {
	die('Sorry! Unable to find content for the Document.');
}

$string = $pdfContent->PDF_Content_Doc;

$newString = GetShortCodeReplace($string, $project);
$pathName = dirname(dirname(__FILE__)).'/doc/';
$fileName = 'SealedMindset_'.strtotime(date('Y-m-d H:i:s')).'_'.safe_b64encode($project);


header("Content-Type: application/vnd.msword");
header('Content-Disposition: attachment; filename="'.$fileName.'.doc"');
echo $newString;