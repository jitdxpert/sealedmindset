<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
$allowed  = ['csv'];
print_r($_FILES);die;
$pathName = dirname(dirname(__FILE__)).'/csv/';
$fileName = 'SealedMindset_'.strtotime(date('Y-m-d H:i:s')).'.csv';

if(!isset($_FILES['csv']['name'])) {
	$res['code'] = 7;
	$res['text'] = 'CSV file is required.';
	goto RESPONSE;
}

$csvname = $_FILES['csv']['name'];
$ext = pathinfo($csvname, PATHINFO_EXTENSION);
if(!in_array($ext, $allowed) ) {
	$res['code'] = 8;
	$res['text'] = 'Invalid file uploaded.';
	goto RESPONSE;
}

echo $csvname;

if($csvname) {
	move_uploaded_file($_FILES['csv']['tmp_name'], $pathName.$fileName);
}

use League\Csv\Reader;

$reader = Reader::createFromPath($pathName.$fileName, 'r');
$results = $reader->fetch();
echo '<pre>';print_r($results);echo '</pre>';
//foreach ($results as $row) {
    //do something here
//}






die('END');
if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$res['code'] = 6;
	$res['text'] = 'Enter a valid email address.';
	goto RESPONSE;
}

$CheckCompanySQL = DB::table('users')->where('User_Email', '=', $email)->first();
if(!empty($CheckCompanySQL)) {
	$res['code'] = 4;
	$res['text'] = 'Company is already exist with this email address.';
	goto RESPONSE;
}

$companyID = DB::table('companies')->insertGetId(
	[
		'Company_Name'  		=> $name,
		'Company_Owner' 		=> $owner,
		'Company_Email' 		=> $email,
		'Profile_Type'			=> 'demo',
		'Company_CreatedOn'	=> date('Y-m-d H:i:s')
	]
);
if(!$companyID) {
	$res['code'] = 3;
	$res['text'] = 'Something went wrong, please try again.';
	goto RESPONSE;
}

$chars   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
$password  = substr(str_shuffle($chars), 0, 8);
$userSQL = DB::table('users')->insert(
	[
		'Company_ID'  	 => $companyID,
		'User_Name' 	 => $owner,
		'User_Email' 	 => $email,
		'User_Password'	 => sha1($password),
		'User_Type'		 => 'company',
		'User_CreatedOn' => date('Y-m-d H:i:s')

	]
);

if(!$userSQL) {
	$res['code'] = 2;
	$res['text'] = 'Sorry, unable to create Company account.';
	goto RESPONSE;
}

$subject  = 'Welcome to Sealed Mindset';
$body = '<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#FCFCFD url(' . BASE_URL . 'images/body-bg.png) repeat 0 0;border:1px solid rgba(0, 0, 0, 0.15);font-family:Verdana,sans-serif;">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="75" align="center" style="background:rgba(0, 0, 0, 0.15) repeat;border-bottom: 1px solid rgba(0, 0, 0, 0.15);">
						<img width="200" alt="" src="' . BASE_URL . 'images/email-logo.png" alt="SealedMindset" />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#0e59ac" size="2.5">
							<br /><br /><span>Dear ' . $owner . ',<br /><br />Welcome, you are invited to Sealed Mindset.<br />Please use below credentials to access your Sealed Mindset account and use the login link.</span>
						</font><br /><br /><br /><br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#161616" size="2">
							<a href="'.BASE_URL.'" target="_blank"><span>Login Here</span></a>
						</font><br />
						<font color="#0e59ac" size="3">
							<span>
								Username: <strong>' . $email . '</strong><br />
								Password: <strong>' . $password . '</strong>
							</span>
						</font><br /><br />
						<font color="#161616" size="2">
							<span>Please keep this information safe and secure.</span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td>
						<hr>
						<br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>In case if you have any questions, please send an Email to : <a style="color:#41a5e1;text-decoration:none;" href="mailto:support@sealedmindset.com">support@sealedmindset.com</a></span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" style="padding-left:20px;">
						<font color="#000" size="2">
							<span>Regards,<br /><br />The <a href="'.BASE_URL.'">SealedMindset</a> Team.</span>
						</font><br /><br />
					</td>
				</tr>
				<tr>
					<td align="center">
						<font color="#aaa" size="2">
								<span>P.S: This is a system generated email. Please do not reply.</span><br /><br />
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>';
$send = Send_Mail($subject, $body, $owner, $email);
if($send) {
	$res['code'] = 0;
	$res['text'] = 'Company has been successfully created.';
	goto RESPONSE;
} else {
	$res['code'] = 1;
	$res['text'] = 'Sorry, unable to send mail to Company.';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
