<?php
include(dirname(__FILE__) . '/config-login.php');

$res = [];
if(empty($_POST['username']) && empty($_POST['password'])) {
	$res['code'] = 6;
	$res['text'] = 'Email & Password are required.';
	goto RESPONSE;
}

$username = addslashes($_POST['username']);
$password = addslashes($_POST['password']);
if(!filter_var($username, FILTER_VALIDATE_EMAIL)) {
	$res['code'] = 5;
	$res['text'] = 'Enter a valid email address.';
	goto RESPONSE;
}

$user = DB::table('users')->where('User_Email', '=', $username)->where('Trash', '=', 0)->first();
if(!$user || $user->User_Password != sha1($password)) {
	$res['code'] = 3;
	$res['text'] = 'Entered wrong email or password.';
	goto RESPONSE;
}

if(($user->User_Type=='individual' || $user->User_Type=='request') && $user->Company_ID==0){
	$_SESSION['UserLoggedIn'] = [
		'Company_ID'   => 0,
		'Company_Name' => '',
		'Master_ID'		 => $user->Master_ID,
		'Profile_Type' => $user->Profile_Type,
		'User_ID'      => $user->User_ID,
		'User_Email'   => $user->User_Email,
		'User_Name'	   => $user->User_Name,
		'User_Type'	   => $user->User_Type
	];
	$res['code'] = 0;
	$res['text'] = BASE_URL . 'playbooks';
	goto RESPONSE;
}

$company = DB::table('companies')->where('Company_ID', '=', $user->Company_ID)->first();
if(!$company) {
	$res['code'] = 1;
	$res['text'] = 'Company does not exist.';
	goto RESPONSE;
} else {
	$_SESSION['UserLoggedIn'] = [
		'Company_ID'   => $company->Company_ID,
		'Company_Name' => $company->Company_Name,
		'Master_ID'		 => $user->Master_ID,
		'Profile_Type' => $user->Profile_Type,
		'User_ID'      => $user->User_ID,
		'User_Email'   => $user->User_Email,
		'User_Name'	   => $user->User_Name,
		'User_Type'	   => $user->User_Type
	];
	$res['code'] = 0;
	$res['text'] = BASE_URL . 'playbooks';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);
