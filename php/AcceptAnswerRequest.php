<?php
include(dirname(__FILE__) . '/config.php');

$res 		= [];
$AnswerID   = $_POST['AnsID'];
$acceptNote = addslashes($_POST['acceptnote']);
$Answer 	= DB::table('answers')->where('Answer_ID', '=', $AnswerID)->first();

$UpdateAnswerSql = DB::table('answers')->where('Answer_ID', '=', $AnswerID)->update(['Answer_Status' => 'Accept', 'Answer_Note' => $acceptNote]);

$DeleteOldData   = DB::table('answers')->where('Question_ID', '=', $Answer->Question_ID)->where('Heading_ID', '=', $Answer->Heading_ID)->where('Tab_ID', '=', $Answer->Tab_ID)->where('Project_ID', '=', $Answer->Project_ID)->where('User_ID', '!=', $Answer->User_ID)->delete();

if(!$UpdateAnswerSql) {
	$res['code'] = 1;
	$res['text'] = 'Server busy try again later.';
	goto RESPONSE;
}

$Question = GetQuestionByQuestionID($Answer->Question_ID);
$Sender   = GetUserById($_SESSION['UserLoggedIn']['User_ID']);
$receiver = GetUserById($Answer->User_ID);

$eventName = $Sender['User_Name'].' approved answer of '.$receiver['User_Name'].' for question: <br>'.$Question['QuestionName'];
DB::table('historys')->insert([
	'Project_ID'		 => $Answer->Project_ID,
	'Company_ID'		 => $Answer->Company_ID,
	'Tab_ID'			 => $Answer->Tab_ID,
	'Heading_ID'		 => $Answer->Heading_ID,
	'Question_ID'		 => $Answer->Question_ID,
	'Sender_ID'			 => $Sender['User_ID'],
	'Receiver_ID'	 	 => $receiver['User_ID'],
	'Event_Name'		 => $eventName,
	'Answer'			 => $Answer->Answer,
	'Answer_Status'		 => 'Accept',
	'Answer_Accept_Note' => $acceptNote,
	'Event_On'			 => date('Y-m-d H:i:s')
]);

SendNotification([
	'Project_ID'        => $Answer->Project_ID,
	'Company_ID'        => $Answer->Company_ID,
	'Sender_ID'         => $_SESSION['UserLoggedIn']['User_ID'],
	'User_ID'           => $Answer->User_ID,
	'Tab_ID'            => $Answer->Tab_ID,
	'Heading_ID'        => $Answer->Heading_ID,
	'Question_ID'       => $Answer->Question_ID,
	'Noti_Message_Type' => 'accept answer',
	'Noti_Message'      => '<b>'.$Sender['User_Name'].'</b> accept your answer for question: <br>'.$Question['QuestionName'],
	'Accept_Note'       => $acceptNote,
	'Noti_PostedOn'     => date('Y-m-d H:i:s'),
]);
$res['code'] = 0;
$res['text'] = 'Your answer acception successfully happen.';
goto RESPONSE;

RESPONSE:
echo json_encode($res);