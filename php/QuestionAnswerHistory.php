<?php
include(dirname(__FILE__) . '/config.php');

$html = '';
$res  = [];
$Project_ID   = $_POST['Project_ID'];
$Company_ID   = $_SESSION['UserLoggedIn']['Company_ID'];
$User_ID      = $_SESSION['UserLoggedIn']['User_ID'];
$User_Type    = $_SESSION['UserLoggedIn']['User_Type'];
$Tab_ID       = $_POST['Tab_ID'];
$Heading_ID   = $_POST['Heading_ID'];
$Question_ID  = $_POST['Question_ID'];

$historySql = DB::table('historys')
->where('Project_ID', '=', $Project_ID)
->where('Company_ID', '=', $Company_ID)
->where(function($query) use($User_ID){
	$query->where('Sender_ID', '=', $User_ID);
	->orWhere('Receiver_ID', $User_ID);
})
->where('Tab_ID', '=', $Tab_ID)
->where('Heading_ID', '=', $headingAry[$qtnID])
->where('Question_ID', '=', $qtnID);

if(empty($historySql)) {
	$res['html'] = '<p>No History found in this question.</p>';
	goto RESPONSE;
} else {
	foreach($historySql as $history) {
		if($history->Answer_Status == 'Pending') {
			$html .= '<p>'.$history->Event_Name.'</p>';
			$html .= '<p>'.GetHumanTime(strtotime($history->Event_On)).'</p>';
		} elseif($history->Answer_Status == 'Reply') {
			$html .= '<p>'.$history->Event_Name.'</p>';
			$html .= '<p>'.$history->Answer.'</p>';
			$html .= '<p>'.GetHumanTime(strtotime($history->Event_On)).'</p>';
		} elseif($history->Answer_Status == 'Reject') {
			$html .= '<p>'.$history->Event_Name.'</p>';
			$html .= '<p>'.$history->Answer_Reject_Note.'</p>';
			$html .= '<p>'.GetHumanTime(strtotime($history->Event_On)).'</p>';
		} else {
			$html .= '<p>'.$history->Event_Name.'</p>';
			$html .= '<p>'.$history->Answer.'</p>';
			$html .= '<p>'.$history->Answer_Accept_Note.'</p>';
			$html .= '<p>'.GetHumanTime(strtotime($history->Event_On)).'</p>';
		}
	}
	$res['html'] = $html;
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);