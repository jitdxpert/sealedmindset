<?php
include(dirname(__FILE__) . '/config.php');

$company = $_SESSION['UserLoggedIn']['Company_ID'];
$project = isset($_GET['p']) ? $_GET['p'] : 0;
$tab 	 = isset($_GET['t']) ? $_GET['t'] : 0;

if(!$company && !$project && !$tab) {
	die('Sorry! Unable to export the PDF.');
}

$pdfContent = DB::table('pdf_content')->where('PDF_Tab_ID', '=', $tab)->first();
if(empty($pdfContent)) {
	die('Sorry! Unable to find content for the PDF.');
}

$string = $pdfContent->PDF_Content;

$newString = GetShortCodeReplace($string, $project);
$newString = mb_convert_encoding($newString, 'HTML-ENTITIES', 'UTF-8');

$pathName = dirname(dirname(__FILE__)).'/pdf/';
$fileName = 'SealedMindset_'.strtotime(date('Y-m-d H:i:s')).'_'.safe_b64encode($project).'.pdf';

$dompdf = new Dompdf\Dompdf();
$dompdf->loadHtml($newString);
$dompdf->setPaper('A4');
$dompdf->render();

header('Content-type: application/pdf');
header('Content-Disposition: attachment; filename="'.$fileName.'.pdf"');
echo $dompdf->output();