<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
if(empty($_POST['username']) && empty($_POST['password']) ) {
	$res['code'] = 4;
	$res['text'] = 'Email & Password are required.';
	goto RESPONSE;
}

$username = addslashes($_POST['username']);
$password = addslashes($_POST['password']);
if(!filter_var($username, FILTER_VALIDATE_EMAIL)) {
	$res['code'] = 3;
	$res['text'] = 'Enter a valid email address.';
	goto RESPONSE;
}

$admin = DB::table('admin')->where('Admin_Email', '=', $username)->where('Admin_Password', '=', sha1($password))->first();
if(!$admin ) {
	$res['code'] = 1;
	$res['text'] = 'Entered wrong email or password.';
	goto RESPONSE;
} else {
	$_SESSION['Admin_ID']    = $admin->Admin_ID;
	$_SESSION['Admin_Name']  = $admin->Admin_Name;
	$_SESSION['Admin_Email'] = $admin->Admin_Email;
	$res['code']  = 0;
	$res['text']  = ADMIN_URL . 'companies';
	goto RESPONSE;
}

RESPONSE:
echo json_encode($res);