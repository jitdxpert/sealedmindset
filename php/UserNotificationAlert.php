<?php
include(dirname(__FILE__) . '/config.php');

$res 	 = [];
$html    = '';
$i       = 0;
$checkNotificationSql = DB::table('notifications')
->where('User_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])
->where('Noti_Status', '=', 'unread')
->orderBy('Noti_ID', 'DESC')
->get();

$notificationCount = count($checkNotificationSql);
if(empty($notificationCount)) {
	$html .= '<a id="notifications" href="'.BASE_URL.'notifications/" class="nav-link"><i class="fa fa-bell-o"></i></a>';
	$res['html'] = $html;
	goto RESPONSE;
}

$html .= '<a id="notifications" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell-o"></i>';
$html .= '<span class="badge bg-red">'.$notificationCount.'</span>';
$html .= '</a>';
$html .= '<ul aria-labelledby="notifications" class="dropdown-menu">';
	foreach($checkNotificationSql as $Notification) {
		$sender = GetUserById($Notification->Sender_ID);
		$senderName = $sender['User_Name'];
		$time = strtotime($Notification->Noti_PostedOn);
		if($Notification->Noti_Message_Type == 'question share') {
			$readStatus = $senderName.' shared a question with You.';
		} elseif($Notification->Noti_Message_Type == 'answer reply') {
			$readStatus = $senderName.' replied to your question.';
		} elseif($Notification->Noti_Message_Type == 'reject answer') {
			$readStatus = $senderName.' reject your answer.';
		} elseif($Notification->Noti_Message_Type == 'accept answer') {
			$readStatus = $senderName.' accept your answer.';
		} else {
			$readStatus = $senderName. ' sent a message to you.';
		}
		$html .= '<li>';
			$html .= '<a class="dropdown-item d-flex">';
				$html .= '<div class="msg-profile"><img class="img-fluid rounded-circle" src="https://www.gravatar.com/avatar/' . md5(strtolower(trim($sender['User_Email']))) . '?s=80" alt="' . $senderName . '" width="80" /></div>';
				$html .= '<div class="msg-body"><h3 class="h5">' . $readStatus . '</h3><i class="fa fa-clock-o"></i> <span>' . GetHumanTime($time) . ' ago</span></div>';
			$html .= '</a>';
		$html .= '</li>';
		if ( $i == 3 ) {
			break;
		}
		$i++;
	}
	$html .= '<li>';
		$html .= '<a href="' . BASE_URL . 'notifications/" class="dropdown-item all-notifications text-center"><strong>view all notifications</strong></a>';
    $html .= '</li>';
$html .= '</ul>';
$res['html'] = $html;
goto RESPONSE;

RESPONSE:
echo json_encode($res);
