<?php
include(dirname(__FILE__) . '/config.php');

$res = [];
$Company = DB::table('companies')->get();
if(!$Company) {
	$res['code'] = 2;
	$res['text'] = 'Something went wrong, please try again.';
	goto RESPONSE;
}

if(empty($Company)) {
	$res['code'] = 1;
	$res['text'] = 'No companies are available.';
	goto RESPONSE;
} else {
	$res['code'] = 0;
	$res['text'][] = $Company;
}

RESPONSE:
echo json_encode($res);