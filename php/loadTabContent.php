<?php
include(dirname(__FILE__) . '/config.php');

$Company_ID = $_SESSION['UserLoggedIn']['Company_ID'];
$User_ID    = $_SESSION['UserLoggedIn']['User_ID'];
$Project_ID = $_POST['projectId'];
$Tab_ID     = $_POST['tabId'];
$Heading_ID = $_POST['headId'];

$viewTypeSQL = DB::table('share')
->where('Project_ID', $Project_ID)
->where('User_ID', $User_ID)
->first();
if(!empty($viewTypeSQL)){
  $viewType = $viewTypeSQL->Shared_Type;
} else {
  $viewType = '';
}

if($viewType=='view'){
  $html = stripslashes(GetViewQuestionsByTabIDAndHeadingID($Tab_ID, $Heading_ID, $Project_ID));
} else {
  $html = stripslashes(GetQuestionsByTabIDAndHeadingID($Tab_ID, $Heading_ID, $Project_ID));
}

echo $html;
