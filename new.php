<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn'] ['User_ID'])) {
  redirect(BASE_URL);
}
if(!isset($_GET['project'])) {
  redirect(BASE_URL);
}

$projectID = safe_b64decode($_GET['project']);
$tabID = (isset($_GET['tab']) ) ? safe_b64decode($_GET['project']) : '';
if($projectID == "") {
  redirect(BASE_URL);
}

$page = 'Playbook';
require(dirname(__FILE__) . '/inc/html-header.php');
?>
<style>
html, body {height: 100%;}
.page {height: 100%;}
.page-content {height: calc(100% - 68px);}
.side-navbar {height: 100%;}
.content-inner {height: 100%;}
.new-playbook {height: calc(100% - 38px);overflow: hidden; padding: 0;}
.new-playbook .container-fluid {height: 100%;overflow: hidden; padding: 15px;}
.new-playbook .col-xs-12 {height: 100%;}
#QuestionBlock {height: 100%;}
#QuestionBlock .row {margin-bottom: 0;}
.new-playbook .nav-pills.nav-wizard {height: 38px; margin-bottom: 15px;}
.question-contain {height: calc(100% - 53px);}
.question-contain .col-md-12 {height: 100%;}
.question-contain .tab-content {height: 100%;}
.question-contain .main-pane {height: 100%;}
.question-contain .mainCard {margin-bottom: 15px;}
.question-contain .main-pane .tab-content {height: calc(100% - 47px);}
.question-contain .main-pane .tab-content .child-pane {height: 100%;}
.question-contain .main-pane .tab-content .child-pane form {height: 100%;}
.question-contain .main-pane .tab-content .child-pane .projectWrapper{height: 100%;position: relative;}
.row.buttonRow{height: 40px;position: absolute;right: 0;left: 0;bottom: 0;}
.first-row {height: calc(100% - 55px); margin-bottom: 15px !important;}
.first-row .col-md-4, .first-row .col-md-8 {height: 100%;}
.first-row .card {margin: 0;}
.first-row .card-header {padding: 10px 15px;}
.first-row .card-body {height: 100%; padding: 15px 0;overflow: hidden;}
.nano-content {padding: 15px;}
.answerArea {margin-top: 0;}
.embed-responsive-item{border: 1px solid #ddd;}
footer.main-footer {height: 38px;}
.scroller {
  text-align:center;
  cursor:pointer;
  display:none;
  padding:7px;
  padding-top:11px;
  white-space:no-wrap;
  vertical-align:middle;
  background-color:#fff;
}
.scroller-right{
  float:right;
}
.scroller-left {
  float:left;
}
.wrapper {
  position:relative;
  margin:0 auto;
  overflow:hidden;
  padding:5px;
  height:50px;
}
.list {
  position:absolute;
  left:0px;
  top:0px;
  min-width:100%;
  margin-left:12px;
  margin-top:0px;
}
.list li {
  display:table-cell;
  position:relative;
  text-align:center;
  cursor:grab;
  cursor:-webkit-grab;
  color:#efefef;
  vertical-align:middle;
}
</style>

<div class="page">
  <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
  <div class="page-content d-flex align-items-stretch">
    <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
    <div class="content-inner">
      <header class="page-header">
        <div class="container-fluid">
          <?php
           $projecttitle = DB::table('projects')->where('Project_ID', $projectID)->first();
          ?>
          <label class="playbook-title"><i class="icon-padnote"></i> <?php echo $projecttitle->Project_Name; ?></label>
        </div>
      </header>
      <section class="new-playbook">
        <div class="container-fluid">
          <div class="col-xs-12">
            <input type="hidden" id="projectId" value="<?php echo $projectID; ?>" />
            <div id="QuestionBlock">
              <div class="scroller scroller-left"><i class="fa fa-chevron-left"></i></div>
              <div class="scroller scroller-right"><i class="fa fa-chevron-right"></i></div>
              <div class="loader"></div>
              <div class="wrapper">
                <ul class="nav nav-pills list nav-wizard">
                  <?php $i = 0;
                  if($_SESSION['UserLoggedIn']['Profile_Type'] == 'demo') {
                    $TabsSQL = DB::table('tabs')->where('Tab_Type', 'demo')->get();
                  } else {
                    $TabsSQL = DB::table('tabs')->get();
                  }
                  if(!empty($TabsSQL)) {
                    foreach($TabsSQL as $TabsData) { ?>
                      <li class="nav-item">
                        <a data-tabid="<?php echo $TabsData->Tab_ID; ?>" class="main-tab-link nav-link <?php echo $i == 0 ? 'active' : ''; ?>" href="#<?php echo str_replace(' ', '-', strtolower($TabsData->Tab_Name)); ?>" role="tab" data-toggle="tab">
                          <?php echo $TabsData->Tab_Name; ?>
                        </a>
                      </li>
                      <?php
                      $i++;
                    }
                  }
                  ?>
                </ul>
              </div>
              <div class="row question-contain">
                <div class="col-md-12">
                  <?php $i = 0;
                  if($_SESSION['UserLoggedIn']['Profile_Type'] == 'demo') {
                    $TabsSQL = DB::table('tabs')->where('Tab_Type', 'demo')->get();
                  } else {
                    $TabsSQL = DB::table('tabs')->get();
                  }
                  if(!empty($TabsSQL)) { ?>
                    <div class="tab-content">
                      <?php
                      foreach($TabsSQL as $TabsData) {
                        $h = 0;
                        $HeadingsSQL1 = DB::table('headings')->where('Tab_ID', '=', $TabsData->Tab_ID)->get(); ?>
                        <div class="tab-pane main-pane <?php echo $i == 0 ? 'active' : ''; ?>" id="<?php echo str_replace(' ', '-', strtolower($TabsData->Tab_Name)); ?>">
                          <div class="mainCard">
                            <?php
                            $headCount = count($HeadingsSQL1);
                            if($headCount > 0) { ?>
                              <ul class="nav headingTabs">
                                <?php foreach($HeadingsSQL1 as $HeadingsData1) {
                                  $last= ($h == $headCount-1 ? $h : '');
                                  ?>
                                  <li class="nav-item">
                                    <a data-headid="<?php echo $HeadingsData1->Heading_ID; ?>" data-lastitemcheck="<?php echo $last; ?>" class="child-tab-link nav-link <?php echo $h == 0 ? 'active' : ''; ?>" href="#<?php echo str_replace(' ', '-', strtolower($HeadingsData1->Heading_Name)); ?>-<?php echo str_replace(' ', '-', strtolower($TabsData->Tab_Name)); ?>" role="tab" data-toggle="tab"><span></span><?php echo ($headCount>1) ? '<b></b>' : ''; ?></a>
                                  </li>
                                  <?php
                                  $h++;
                                }
                                ?>
                              </ul>
                              <?php
                            }
                            ?>
                          </div>
                          <?php $h = 0;
                          $HeadingsSQL2 = DB::table('headings')->where('Tab_ID', '=', $TabsData->Tab_ID)->get();
                          if(!empty($HeadingsSQL2)) { ?>
                            <div class="tab-content">
                              <?php foreach($HeadingsSQL2 as $HeadingsData2) { ?>
                                <div class="tab-pane <?php echo $h == 0 ? 'active' : ''; ?> child-pane" id="<?php echo str_replace(' ', '-', strtolower($HeadingsData2->Heading_Name)); ?>-<?php echo str_replace(' ', '-', strtolower($TabsData->Tab_Name)); ?>">
                                  <form class="tab_answer" id="<?php echo str_replace(' ', '-', strtolower($HeadingsData2->Heading_Name)); ?>-<?php echo str_replace(' ', '-', strtolower($TabsData->Tab_Name)); ?>-form" method="post" action="<?php echo BASE_URL; ?>php/PlaybookAnswerProcess">
                                    <input type="hidden" name="Project_ID" value="<?php echo $projectID; ?>" />
                                    <input type="hidden" name="Tab_ID" value="<?php echo $TabsData->Tab_ID; ?>" />
                                    <div class="projectWrapper" style="height: 100%;">
                                      <?php if($TabsData->Tab_ID==1 && $HeadingsData2->Heading_ID==1){ ?>
                                        <?php $html = GetQuestionsByTabIDAndHeadingID($TabsData->Tab_ID, $HeadingsData2->Heading_ID, $projectID); echo $html; ?>
                                      <?php } ?>
                                    </div>
                                  </form>
                                </div>
                                <?php
                                $h++;
                              } ?>
                            </div>
                            <?php
                          }
                          ?>
                        </div>
                        <?php
                        $i++;
                      } ?>
                    </div>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
    </div>
  </div>
</div>

<div class="modal right fade" id="RequestModal" tabindex="-1" role="dialog" aria-labelledby="RequestModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="RequestModalLabel">Send Answer Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php
      $html = '';
      $type = '';
      if($_SESSION['UserLoggedIn']['User_Type']=='company'){
        $membersSQL = DB::table('users')
        ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
        ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
        ->where('User_Type', 'request')
        ->get();
        $html .= '<select name="memberId" id="memberId" class="form-control" required>';
          $html .= '<option value="">Select Member</option>';
          foreach($membersSQL as $member) {
            if($member->User_Type == 'department') {
              $type = 'Department';
            } elseif($member->User_Type == 'user') {
              $type = 'User';
            } elseif($member->User_Type == 'request') {
              $type = 'Contributor';
            }
            $html .= '<option value="'.$member->User_ID.'">'.$member->User_Name.' ('.$type.')</option>';
          }
        $html .= '</select>';
      } elseif($_SESSION['UserLoggedIn']['User_Type']=='department'||$_SESSION['UserLoggedIn']['User_Type']=='user'){
        $membersSQL = DB::table('users')
        ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
        ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
        ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
        ->where('User_Type', 'request')
        ->get();
        $html .= '<select name="memberId" id="memberId" class="form-control" required>';
          $html .= '<option value="">Select Member</option>';
          foreach($membersSQL as $member) {
            if($member->User_Type == 'user') {
              $type = 'User';
            } elseif($member->User_Type == 'request') {
              $type = 'Contributor';
            }
            $html .= '<option value="'.$member->User_ID.'">'.$member->User_Name.' ('.$type.')</option>';
          }
        $html .= '</select>';
      } elseif($_SESSION['UserLoggedIn']['User_Type']=='individual'){
        $membersSQL = DB::table('users')
        ->where('User_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
        ->where('Master_ID', $_SESSION['UserLoggedIn']['User_ID'])
        ->where('User_Type', 'request')
        ->get();
        $html .= '<select name="memberId" id="memberId" class="form-control" required>';
          $html .= '<option value="">Select Member</option>';
          foreach($membersSQL as $member) {
            if($member->User_Type == 'request') {
              $type = 'Contributor';
            }
            $html .= '<option value="'.$member->User_ID.'">'.$member->User_Name.' ('.$type.')</option>';
          }
        $html .= '</select>';
      }
      ?>
      <form id="request-form" method="post" action="<?php echo BASE_URL; ?>php/SendAnswerRequest" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Members List<span class="text-danger">*</span></label>
            <?php echo $html; ?>
          </div>
          <input type="hidden" id="QtnID" name="QtnID" />
          <input type="hidden" id="project" name="project" value="<?php echo $projectID; ?>" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary requestButton">REQUEST ANSWER</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal right fade" id="AnswerRejectModal" tabindex="-1" role="dialog" aria-labelledby="AnswerRejectModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="RequestModalLabel">Reject Answer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="rejectAnswer-form" method="post" action="<?php echo BASE_URL; ?>php/RejectAnswerRequest" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Note<span class="text-danger">*</span></label>
            <textarea placeholder="Enter reason for Rejection" name="rejectnote" id="rejectnote" class="form-control" required ></textarea>
          </div>
          <input type="hidden" id="AnsID" name="AnsID" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary">Reject ANSWER</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal right fade" id="AnswerAcceptModal" tabindex="-1" role="dialog" aria-labelledby="AnswerAcceptModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="RequestModalLabel">Accept Answer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="acceptAnswer-form" method="post" action="<?php echo BASE_URL; ?>php/AcceptAnswerRequest" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Note<span class="text-danger">*</span></label>
            <textarea placeholder="Enter reason for Acception" name="acceptnote" id="acceptnote" class="form-control" required ></textarea>
          </div>
          <input type="hidden" id="AnsId" name="AnsID" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary">Accept ANSWER</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>
