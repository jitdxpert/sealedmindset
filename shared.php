<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID']) && $_SESSION['UserLoggedIn']['User_Type'] == 'request') {
  redirect(BASE_URL);
}
if(!isset($_GET['project'])) {
  redirect(BASE_URL);
}

$projectID = safe_b64decode($_GET['project']);
$tabID = (isset($_GET['tab']) ) ? safe_b64decode($_GET['project']) : '';
if($projectID == "") {
  redirect(BASE_URL);
}

$page = 'Playbooks';
require(dirname(__FILE__) . '/inc/html-header.php');
?>

<div class="page">
  <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
  <div class="page-content d-flex align-items-stretch">
    <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
    <div class="content-inner">
      <header class="page-header">
        <div class="container-fluid">
          <?php
           $projecttitle = DB::table('projects')->where('Project_ID', $projectID)->first();
          ?>
          <label class="playbook-title"><i class="icon-padnote"></i> <?php echo $projecttitle->Project_Name; ?></label>
        </div>
      </header>
      <section class="playbook">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <div class="col-xs-3">
                    <ul class="nav nav-tabs flex-column">
                      <?php
                      $i = 0;
                      $RequestSQL = DB::table('requests')
                      ->where('Company_ID', '=', $_SESSION['UserLoggedIn']['Company_ID'])
                      ->where('User_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])
                      ->where('Project_ID', '=', $projectID)
                      ->groupBy('Tab_ID')
                      ->get();
                      if(!empty($RequestSQL)) {
                        foreach($RequestSQL as $Request) {
                          $TabsSQL = DB::table('tabs')->where('Tab_ID', '=', $Request->Tab_ID)->get();
                          if(!empty($TabsSQL)) {
                            foreach($TabsSQL as $TabsData) { ?>
                              <li class="nav-item">
                                <a class="nav-link <?php echo $i == 0 ? 'active' : ''; ?>" href="#<?php echo str_replace(' ','-',strtolower($TabsData->Tab_Name)); ?>" data-toggle="tab">
                                  <?php echo $TabsData->Tab_Name; ?>
                                </a>
                              </li>
                              <?php
                              $i++;
                            }
                          }
                        }
                      }
                      ?>
                    </ul>
                  </div>
                  <div class="col-xs-9">
                    <div class="tab-content">
                      <?php
                      $i = 0;
                      $RequestSQL = DB::table('requests')
                      ->where('Company_ID', '=', $_SESSION['UserLoggedIn']['Company_ID'])
                      ->where('User_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])
                      ->where('Project_ID', '=', $projectID)
                      ->groupBy('Tab_ID')
                      ->get();
                      if(!empty($RequestSQL)) {
                        foreach($RequestSQL as $Request) {
                          $TabsSQL = DB::table('tabs')->where('Tab_ID', '=', $Request->Tab_ID)->get();
                          if(!empty($TabsSQL)) {
                            foreach($TabsSQL as $TabsData) { ?>
                              <div class="tab-pane <?php echo $i == 0 ? 'active' : ''; ?>" id="<?php echo str_replace(' ','-',strtolower($TabsData->Tab_Name)); ?>">
                                <form class="tab_answer" id="<?php echo str_replace(' ','-',strtolower($TabsData->Tab_Name)); ?>-form" method="post" action="<?php echo BASE_URL; ?>php/PlaybookAnswerProcess">
                                  <input type="hidden" name="Project_ID" value="<?php echo $projectID; ?>" >
                                  <input type="hidden" name="Tab_ID" value="<?php echo $TabsData->Tab_ID; ?>" >
                                  <?php $html = GetQuestionsByRequestTabID($TabsData->Tab_ID, $projectID); echo $html; ?>
                                </form>
                              </div>
                              <?php
                              $i++ ;
                            }
                          }
                        }
                      }
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
    </div>
  </div>
</div>

<div class="modal right fade" id="RequestModal" tabindex="-1" role="dialog" aria-labelledby="RequestModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="RequestModalLabel">Send Answer Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="request-form" method="post" action="<?php echo BASE_URL; ?>php/SendAnswerRequest" >
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label">Member Name<span class="text-danger">*</span></label>
            <input type="text" placeholder="Member Name" name="memberName" id="memberName" class="form-control" required />
          </div>
          <div class="form-group">
            <label class="form-control-label">Member Email<span class="text-danger">*</span></label>
            <input type="email" placeholder="Member Email" name="memberEmail" id="memberEmail" class="form-control" required />
          </div>
          <input type="hidden" id="QtnID" name="QtnID" />
          <input type="hidden" id="project" name="project" value="<?php echo $projectID; ?>" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
          <button type="submit" class="btn btn-primary">REQUEST ANSWER</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>
