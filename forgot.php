<?php
require(dirname(__FILE__) . '/php/config.php');

if(isset($_SESSION['UserLoggedIn']['Company_ID']) && isset($_SESSION['UserLoggedIn']['User_ID'])) {
    redirect(BASE_URL.'playbooks');
}

$page = 'Forgot Password';
require(dirname(__FILE__) . '/inc/html-header.php'); ?>

  <div class="page login-page">
    <div class="container d-flex align-items-center">
      <div class="form-holder has-shadow">
        <div class="row">
          <div class="col-lg-6">
            <div class="info d-flex align-items-center">
              <div class="content">
                <div class="logo">
                  <h1>Sealed Mindset</h1>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 bg-white">
            <div class="form d-flex align-items-center">
              <div class="content">
                <form id="forgot-form" method="post" action="<?php echo BASE_URL; ?>php/CompanyForgotProcess">
                  <div class="form-group">
                    <input id="login-username" type="email" name="username" required="" class="input-material">
                    <label for="login-username" class="label-material">User Name or Email</label>
                  </div>
                  <button type="submit" id="login" class="btn btn-primary">Submit</button>
                </form>
                <a href="<?php echo BASE_URL; ?>" class="forgot-pass">Back to Login</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="copyrights text-center">
      <p>Design by <a href="<?php echo BASE_URL; ?>" class="external">Sealed Mindset</a></p>
    </div>
  </div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>