<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID'])) {
    redirect(BASE_URL);
}

$page = 'Notifications';
require(dirname(__FILE__) . '/inc/html-header.php');
?>

  <div class="page">
    <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
    <div class="page-content d-flex align-items-stretch">
      <?php require(dirname(__FILE__) . '/inc/navbar.php'); ?>
      <div class="content-inner" ng-class="active ? 'active' : ''">
        <header class="page-header">
          <div class="container-fluid">
            <h2 class="no-margin-bottom">
              Notifications
            </h2>
          </div>
        </header>
        <section class="playbooks">
            <div class="container-fluid">
                <div id="page-content-wrapper">
                	<div class="notifications-page">
                        <div class="col-sm-12">
                            <div class="row">
                                <?php
                                $html = '';
                                DB::table('notifications')->where('User_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])->update(['Noti_Status' => 'read']);

                                $getNotiSQL = DB::table('notifications')->where('User_ID', '=', $_SESSION['UserLoggedIn']['User_ID'])->orderBy('Noti_PostedOn', 'DESC')->get();                                        
                                    if(!empty($getNotiSQL)) {
                                ?>
                                        <div class="col-sm-12">
                                        	<div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="table-container">
                                                        <table class="table table-filter">
                                                            <tbody>
                                                        		<?php
                                                                foreach($getNotiSQL as $Notification) {
                                                                    $sender = GetUserById($Notification->Sender_ID);
                                                                    $senderName = $sender['User_Name'];
                                                                    $time = strtotime($Notification->Noti_PostedOn);

                                                                    if($Notification->Noti_Message_Type == 'question share') {

                                                                        $html = '<tr>';
                                                                            $html .= '<td>';
                                                                                $html .= '<div class="media">';
                                                                                    $html .= '<a href="#" class="pull-left">';
                                                                                        $html .= '<img src="https://www.gravatar.com/avatar/' . md5(strtolower(trim($sender['User_Email']))) . '?s=55" alt="' . $senderName . '" class="media-photo" />';
                                                                                    $html .= '</a>';
                                                                                    $html .= '<div class="media-body">';
                                                                                        $html .= '<span class="media-meta pull-right">';
                                                                                            $html .= '<i class="fa fa-clock-o"></i>&nbsp;';
                                                                                            $html .= GetHumanTime($time). ' ago';
                                                                                        $html .= '</span>';                   
                                                                                        $html .= '<h4 class="title">';
                                                                                            $html .= $senderName;
                                                                                            $html .= '<span class="pull-right default">Question Shared</span>';
                                                                                        $html .= '</h4>';
                                                                                        $html .= '<p class="summary">';
                                                                                            $html .= GetShortCodeReplace($Notification->Noti_Message, $Notification->Project_ID);
                                                                                        $html .= '</p>';
                                                                                $html .= '</div>';
                                                                            $html .= '</td>';
                                                                        $html .= '</tr>';

                                                                    } else if($Notification->Noti_Message_Type == 'answer reply') {

                                                                        $html = '<tr>';
                                                                            $html .= '<td>';
                                                                                $html .= '<div class="media">';
                                                                                    $html .= '<a href="#" class="pull-left">';
                                                                                        $html .= '<img src="https://www.gravatar.com/avatar/' . md5(strtolower(trim($sender['User_Email']))) . '?s=55" alt="' . $senderName . '" class="media-photo" />';
                                                                                    $html .= '</a>';
                                                                                    $html .= '<div class="media-body">';
                                                                                        $html .= '<span class="media-meta pull-right">';
                                                                                            $html .= '<i class="fa fa-clock-o"></i>&nbsp;';
                                                                                            $html .= GetHumanTime($time). ' ago';
                                                                                        $html .= '</span>';                   
                                                                                        $html .= '<h4 class="title">';
                                                                                            $html .= $senderName;
                                                                                            $html .= '<span class="pull-right warning">Answer Replied</span>';
                                                                                        $html .= '</h4>';
                                                                                        $html .= '<p class="summary">';
                                                                                            $html .= GetShortCodeReplace($Notification->Noti_Message, $Notification->Project_ID);
                                                                                        $html .= '</p>';
                                                                                $html .= '</div>';
                                                                            $html .= '</td>';
                                                                        $html .= '</tr>';

                                                                    } else if($Notification->Noti_Message_Type == 'reject answer') {

                                                                        $html = '<tr>';
                                                                            $html .= '<td>';
                                                                                $html .= '<div class="media">';
                                                                                    $html .= '<a href="#" class="pull-left">';
                                                                                        $html .= '<img src="https://www.gravatar.com/avatar/' . md5(strtolower(trim($sender['User_Email']))) . '?s=55" alt="' . $senderName . '" class="media-photo" />';
                                                                                    $html .= '</a>';
                                                                                    $html .= '<div class="media-body">';
                                                                                        $html .= '<span class="media-meta pull-right">';
                                                                                            $html .= '<i class="fa fa-clock-o"></i>&nbsp;';
                                                                                            $html .= GetHumanTime($time). ' ago';
                                                                                        $html .= '</span>';                   
                                                                                        $html .= '<h4 class="title">';
                                                                                            $html .= $senderName;
                                                                                            $html .= '<span class="pull-right danger">Answer Rejected</span>';
                                                                                        $html .= '</h4>';
                                                                                        $html .= '<p class="summary">';
                                                                                            $html .= GetShortCodeReplace($Notification->Noti_Message, $Notification->Project_ID);
                                                                                        $html .= '</p>';
                                                                                $html .= '</div>';
                                                                            $html .= '</td>';
                                                                        $html .= '</tr>';

                                                                    } else if($Notification->Noti_Message_Type == 'accept answer') {

                                                                        $html = '<tr>';
                                                                            $html .= '<td>';
                                                                                $html .= '<div class="media">';
                                                                                    $html .= '<a href="#" class="pull-left">';
                                                                                        $html .= '<img src="https://www.gravatar.com/avatar/' . md5(strtolower(trim($sender['User_Email']))) . '?s=55" alt="' . $senderName . '" class="media-photo" />';
                                                                                    $html .= '</a>';
                                                                                    $html .= '<div class="media-body">';
                                                                                        $html .= '<span class="media-meta pull-right">';
                                                                                            $html .= '<i class="fa fa-clock-o"></i>&nbsp;';
                                                                                            $html .= GetHumanTime($time). ' ago';
                                                                                        $html .= '</span>';                   
                                                                                        $html .= '<h4 class="title">';
                                                                                            $html .= $senderName;
                                                                                            $html .= '<span class="pull-right success">Answer Accepted</span>';
                                                                                        $html .= '</h4>';
                                                                                        $html .= '<p class="summary">';
                                                                                            $html .= GetShortCodeReplace($Notification->Noti_Message, $Notification->Project_ID);
                                                                                        $html .= '</p>';
                                                                                $html .= '</div>';
                                                                            $html .= '</td>';
                                                                        $html .= '</tr>';

                                                                    } else {

                                                                        $html = '<tr>';
                                                                            $html .= '<td>';
                                                                                $html .= '<div class="media">';
                                                                                    $html .= '<a href="#" class="pull-left">';
                                                                                        $html .= '<img src="https://www.gravatar.com/avatar/' . md5(strtolower(trim($sender['User_Email']))) . '?s=55" alt="' . $senderName . '" class="media-photo" />';
                                                                                    $html .= '</a>';
                                                                                    $html .= '<div class="media-body">';
                                                                                        $html .= '<span class="media-meta pull-right">';
                                                                                            $html .= '<i class="fa fa-clock-o"></i>&nbsp;';
                                                                                            $html .= GetHumanTime($time). ' ago';
                                                                                        $html .= '</span>';                   
                                                                                        $html .= '<h4 class="title">';
                                                                                            $html .= $senderName;
                                                                                            $html .= '<span class="pull-right default">Notification</span>';
                                                                                        $html .= '</h4>';
                                                                                        $html .= '<p class="summary">';
                                                                                            $html .= GetShortCodeReplace($Notification->Noti_Message, $Notification->Project_ID);
                                                                                        $html .= '</p>';
                                                                                $html .= '</div>';
                                                                            $html .= '</td>';
                                                                        $html .= '</tr>';

                                                                    }
                                                                    echo $html;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <?php } else { ?>
                                    	<div class="col-sm-12">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="table-container">
                                                        <table class="table table-filter">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <h4 class="item-title text-center">Currently, you have no notifications.</h4>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
      </div>
    </div>
  </div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>