<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID'])) {
  redirect(BASE_URL);
}

$page = 'Playbooks';
require(dirname(__FILE__) . '/inc/html-header.php');
?>

<div class="page">
  <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
  <div class="page-content d-flex align-items-stretch">
    <?php require(dirname(__FILE__) . '/inc/navbar.php');
    if(isset($_GET["q"])){
      $q = $_GET["q"];
    } else {
      $q= '';
    }
    ?>
    <div class="content-inner">
      <div class="loader hide"></div>
      <header class="page-header">
        <div class="container-fluid">
          <h2 class="no-margin-bottom">
            Marketplace
          </h2>
        </div>
      </header>
        <section class="playbooks">
          <div class="container-fluid">
            <?php
              $ProjectSQL = DB::table('projects')
              ->where('Company_ID', '!=', $_SESSION['UserLoggedIn']['Company_ID'])
              ->where('Master_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
              ->where(function($searchquery) use($q){
                $searchquery->where('Project_Name', 'like', '%'.$q.'%');
              })
              ->count();
              if(!empty($ProjectSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                if($q){
                  $targetpage = 'playbooks?q='.$q;
                } else {
                  $targetpage = 'playbooks';
                }
                $total_pages = $ProjectSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination

                $ProjectFetchSQL = DB::table('projects')
                ->where('Company_ID', '!=', $_SESSION['UserLoggedIn']['Company_ID'])
                ->where('Master_ID', '!=', $_SESSION['UserLoggedIn']['User_ID'])
                ->where(function($searchquery) use($q){
                  $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                })
                ->orderBy('Project_ID', 'DESC')
                ->offset($start)
                ->limit($limit)
                ->get();
                 ?>
                  <div class="parent">
                    <div class="child-row">
                      <div class="row">
                        <?php foreach($ProjectFetchSQL as $Project) { ?>
                        <div class="col-md-6 playbook-list">
                          <div class="articles card left">
                            <div class="card-close">
                              <?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?>
                            </div>
                            <div class="card-post">
                              <span>
                              <a class="importRequestBtn" title="Import Playbook" data-project="<?php echo $Project->Project_ID; ?>">
                                <i class="fa fa-download"></i>
                              </a>
                            </span>
                          </div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                              <div class="item d-flex playbook-list-department"><?php echo stripslashes($Project->Project_Department); ?></div>
                            </div>
                          </div>
                        </div>
                        <?php
                      } ?>
                      </div>
                    </div>
                  </div>

                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:60px"> <?=$pagination?></div>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No projects are available.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            ?>
          </div>
        </section>
      <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
    </div>
  </div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>
