var siteurl  = $('#site_url').val();
var adminurl = $('#admin_url').val();

$(document).ready(function () {
  'use strict';

  $('body').tooltip({
    selector: '[data-toggle="tooltip"]'
  });

  var date = new Date();
  date.setDate(date.getDate()-1);

  $("input[name=deadline]").datepicker({
    startDate: date
  });

  if($('.nano').length) {
    $('.nano').nanoScroller({ alwaysVisible: true });
    $('.loader').addClass('hide');
  }

  // Bootstrap Nitify default settings
  $.notifyDefaults({
    type: 'success',
    allow_dismiss: false,
    placement: {
      from: "bottom",
      align: "left"
    },
    z_index: 1053,
  });

  $('.showMoreNoti').on('click', function(e) {
    e.preventDefault();

    var sid = $(this).data('sid');
    if($('.notiWrap_'+sid).hasClass('show')) {
      $('.notiWrap_'+sid).removeClass('show').addClass('hide');
      $('.notiWrap_'+sid).slideUp(1000);
    } else {
      $('.notiWrap_'+sid).removeClass('hide').addClass('show');
      $('.notiWrap_'+sid).slideDown(1000);
    }
  });

  // View Playbook Button
  $('.view-playbook').on('click', function(){
    var url = $(this).data('url');
    window.location.href = url;
  });

  // Search Box
  $('#search').on('click', function(e) {
    e.preventDefault();

    $('.search-box').fadeIn();
  });
  $('.dismiss').on('click', function(e) {
    e.preventDefault();

    $('.search-box').fadeOut();
  });

  // Card Close
  $('.card-close a.remove').on('click', function(e) {
    e.preventDefault();

    $(this).parents('.card').fadeOut();
  });

  // Adding fade effect to dropdowns
  $('.dropdown').on('show.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).fadeIn();
  });
  $('.dropdown').on('hide.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).fadeOut();
  });

  // Notification Alert
  if($('#notification-area').length) {
    setInterval(function() {
      $.ajax({
        url     : siteurl+'php/UserNotificationAlert',
        type    : 'POST',
        dataType: 'json',
        success : function(data) {
          $('#notification-area').html(data.html);
        },
        error   : function(xhr, err) {
          console.log(xhr, err);
        }
      });
    }, 5000);
  }

  // TinyMCE Editor
  if($('.tab_answer').length) {
    setInterval(function() {
      tinymce.init({
        selector: '.nonEditableMCE',
        height: 150,
        theme: 'modern',
        plugins: 'preview fullpage searchreplace autolink directionality visualblocks link charmap hr insertdatetime lists textcolor wordcount contextmenu colorpicker textpattern',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat',
        image_advtab: true,
        readonly: true,
        content_css: [
          '//fonts.googleapis.com/css?family=Poppins:300,400,700'
        ]
      });

      tinymce.init({
        selector: '.editableMCE',
        height: 150,
        theme: 'modern',
        plugins: 'preview fullpage searchreplace autolink directionality visualblocks link charmap hr insertdatetime lists textcolor wordcount contextmenu colorpicker textpattern',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat',
        image_advtab: true,
        content_css: [
          '//fonts.googleapis.com/css?family=Poppins:300,400,700'
        ]
      });

      tinymce.init({
        selector: '.editableLargeMCE',
        height: 300,
        theme: 'modern',
        plugins: 'preview fullpage searchreplace autolink directionality visualblocks link charmap hr insertdatetime lists textcolor wordcount contextmenu colorpicker textpattern',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat',
        image_advtab: true,
        content_css: [
          '//fonts.googleapis.com/css?family=Poppins:300,400,700'
        ]
      });
    }, 1000);
  }


  // Company form validation
  $('#company-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        regExp: "^[a-zA-Z1-9]"
      },
      owner: {
        required: true,
        minlength: 6,
        regExp: "^[a-zA-Z1-9]"
      },
      email: {
        required: true,
        email: true,
        remote: {
          url: siteurl+'php/CheckEmailExist',
          type: "post"
        }
      },
      type: {
        required: true
      },
      pass: {
        required: true,
        minlength: 6,
      }
    },
    messages: {
      name: {
        required: 'Please enter company name',
        minlength: $.validator.format("Please enter at least {0} characters"),
        regExp: "No special characters or space allowed in beginning"
      },
      owner: {
        required: 'Please enter owner name',
        minlength: $.validator.format("Please enter at least {0} characters"),
        regExp: "No special characters or space allowed in beginning"
      },
      email: {
        required: 'Please enter owner email',
        email: 'Please enter a valid email',
        remote: 'Email address already exist'
      },
      type: {
        required: 'Please select profile type'
      },
      pass: {
        required: 'Please enter owner password',
        minlength: $.validator.format("Please enter at least {0} characters")
      }
    }
  });
  $(document).on('submit', '#company-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#CompanyModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Edit Company
  $('.EditCompany').on('click', function(e) {
    e.preventDefault();

    var uid   = $(this).data('uid');
    var cid   = $(this).data('cid');
    var name  = $(this).data('name');
    var owner = $(this).data('owner');
    var type  = $(this).data('type');

    $('#editUserID').val(uid);
    $('#editCompanyID').val(cid);
    $('#editCompanyName').val(name);
    $('#editCompanyOwner').val(owner);
    $('#editCompanyType').val(type);
  });
  $(document).on('submit', '#edit-company-form', function(e) {
    e.preventDefault();
    $('.loader').removeClass('hide');
    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#EditCompanyModal').modal('hide');
            $('.loader').addClass('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $('.loader').addClass('hide');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $('.loader').addClass('hide');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Reomve Company
  $('.TrashCompany').on('click', function(e) {
    e.preventDefault();

    var companyID = $(this).data('id');
    $('#removeCompanyID').val(companyID);
  });

  // Delete Company
  $('.RemoveCompany').on('click', function(e) {
    e.preventDefault();

    var companyID = $(this).data('id');
    $('#deleteCompanyID').val(companyID);
  });
  $(document).on('submit', '#remove-company-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#TrashCompanyModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Restore Company
  $('.RestoreCompany').on('click', function(e) {
    e.preventDefault();

    var companyID = $(this).data('id');
    $('#restoreCompanyID').val(companyID);
  });
  $(document).on('submit', '#restore-company-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#RestoreCompanyModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Company CSV form validation
  $('#company-csv-form').validate({
    rules: {
      csv: {
        required: true
      }
    },
    messages: {
      csv: {
        required: 'Please select CSV for upload'
      }
    }
  });
  // $(document).on('submit', '#company-csv-form', function(e) {
  //     e.preventDefault();
  //
  //     $.ajax({
  //         url     : $(this).attr('action'),
  //         type    : $(this).attr('method'),
  //         dataType: 'json',
  //         data    : $(this).serialize(),
  //         success : function(data) {
  //             if ( data.code == 0 ) {
  //                 $.notify({
  //                     message: data.text
  //                 },{
  //                     type: 'success'
  //                 });
  //                 setTimeout(function() {
  //                     window.location.reload();
  //                 }, 500);
  //             } else {
  //                 $.notify({
  //                     message: data.text
  //                 },{
  //                     type: 'danger'
  //                 });
  //             }
  //         },
  //         error   : function(xhr, err) {
  //             console.log(xhr, err);
  //         }
  //     });
  //     return false;
  // });


  // Individual form validation
  $('#individual-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        regExp: "^[a-zA-Z1-9]"
      },
      email: {
        required: true,
        email: true,
        remote: {
          url: siteurl+'php/CheckEmailExist',
          type: "post"
        }
      },
      profile_type: {
        required: true
      },
      pass: {
        required: true,
        minlength: 6,
      }
    },
    messages: {
      name: {
        required: 'Please enter full name',
        minlength: $.validator.format("Please enter at least {0} characters"),
        regExp: "No special characters or space allowed in beginning"
      },
      email: {
        required: 'Please enter email address',
        email: 'Please enter a valid email',
        remote: 'Email address already exist'
      },
      profile_type: {
        required: 'Please select profile type'
      },
      pass: {
        required: 'Please enter a password',
        minlength: $.validator.format("Please enter at least {0} characters")
      }
    }
  });
  $(document).on('submit', '#individual-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#IndividualModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Edit Individual
  $('.EditIndividual').on('click', function(e) {
    e.preventDefault();

    var uid   = $(this).data('uid');
    var name  = $(this).data('name');
    var type  = $(this).data('type');

    $('#editIndividualID').val(uid);
    $('#editIndividualName').val(name);
    $('#editIndividualProfile_type').val(type);
  });
  $(document).on('submit', '#edit-individual-form', function(e) {
    e.preventDefault();
    $('.loader').removeClass('hide');
    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#EditIndividualModal').modal('hide');
            $('.loader').addClass('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $('.loader').addClass('hide');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $('.loader').addClass('hide');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Reomve Individual
  $('.TrashIndividual').on('click', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    $('#removeIndividualUserID').val(id);
  });
  $(document).on('submit', '#remove-individual-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#TrashIndividualModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Restore Individual
  $('.RestoreIndividual').on('click', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    $('#restoreIndividualUserID').val(id);
  });
  $(document).on('submit', '#restore-individual-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#RestoreIndividualModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Add User
  $('#user-form').validate({
    rules: {
      name: {
        required: true,
        minlength: 2,
        regExp: "^[a-zA-Z1-9]"
      },
      email: {
        required: true,
        email: true,
        remote: {
          url: siteurl+'php/CheckEmailExist',
          type: "post"
        }
      },
      type: {
        required: true
      }
    },
    messages: {
      name: {
        required: 'Please enter name',
        minlength: $.validator.format("Please enter at least {0} characters"),
        regExp: "No special characters or space allowed in beginning"
      },
      email: {
        required: 'Please enter email',
        email: 'Please enter a valid email',
        remote: 'Email address already exist'
      },
      type: {
        required: 'Please select type'
      }
    }
  });
  $(document).on('submit', '#user-form', function(e) {
    e.preventDefault();
    $('.loader').removeClass('hide');
    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#NewUserModal').modal('hide');
            $('.loader').addClass('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else {
          $('.loader').addClass('hide');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Edit User
  $('.EditUser').on('click', function(e) {
    e.preventDefault();

    var uid  = $(this).data('id');
    var name = $(this).data('name');
    // var type = $(this).data('type');

    $('#editMemberID').val(uid);
    $('#editMemberName').val(name);
    // $('#editMemberType').val(type);
  });
  $(document).on('submit', '#edit-user-form', function(e) {
    e.preventDefault();
    $('.loader').removeClass('hide');
    $('.editButton').attr('disabled', 'disabled');
    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('.editButton').removeAttr('disabled');
            $('#EditUserModal').modal('hide');
            $('.loader').addClass('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $('.loader').addClass('hide');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('.editButton').removeAttr('disabled');
        } else {
          $('.loader').addClass('hide');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('.editButton').removeAttr('disabled');
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });



  // Remove User
  $('.TrashUser').on('click', function(e) {
    e.preventDefault();

    var userID = $(this).data('id');
    $('#removeUserID').val(userID);
  });
  $(document).on('submit', '#remove-user-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#TrashUserModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  $(document).on('submit', '#delete-user-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#TrashUserModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Restore User
  $('.RestoreUser').on('click', function(e) {
    e.preventDefault();

    var userID = $(this).data('id');
    $('#restoreUserID').val(userID);
  });
  $(document).on('submit', '#restore-user-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#RestoreUserModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Reomove Department
  $('.TrashDepartment').on('click', function(e) {
    e.preventDefault();

    var userID = $(this).data('id');
    $('#removeDepartmentID').val(userID);
  });
  $(document).on('submit', '#delete-department-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#TrashDepartmentModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Restore Department
  $('.RestoreDepartment').on('click', function(e) {
    e.preventDefault();

    var userID = $(this).data('id');
    $('#restoreDepartmentID').val(userID);
  });
  $(document).on('submit', '#restore-department-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('#RestoreDepartmentModal').modal('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Export Playbook
  $('.exportRequestBtn').on('click', function(e) {
    e.preventDefault();

    var project = $(this).data('project');
    $('#projectId').val(project);
  });
  $(document).on('submit', '#play-export-form', function(e) {
    e.preventDefault();
    $('.exportButton').attr('disabled', 'disabled');
    $('.loader').removeClass('hide');
    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('.exportButton').removeAttr('disabled');
            $('#ExportModal').modal('hide');
            $('.loader').addClass('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $('.exportButton').removeAttr('disabled');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('.loader').addClass('hide');
        } else {
          $('.exportButton').removeAttr('disabled');
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('.loader').addClass('hide');
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Share Playbook
  $('.playRequestBtn').on('click', function(e) {
    e.preventDefault();
    var project = $(this).data('project');
    var creator = $(this).data('creator');
    var xmlhttp;
    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("memberId").innerHTML= this.responseText;
        document.getElementById("shareType").innerHTML='<option value="">Select Member First</option>';
        $('#project').val(project);
      }
    };
    xmlhttp.open("GET","/php/getsharableusers.php?creator="+creator,true);
    xmlhttp.send();
  });

  $('#memberId').on('change',function(){
    var id = $('#memberId').val();
    if(id != ''){
      var xmlhttp;
      if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
      } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var type = this.responseText;
          if(type == 'request'){
            document.getElementById("shareType").innerHTML='<option value="">Share Type</option><option value="view">View</option>';
          } else{
            document.getElementById("shareType").innerHTML='<option value="">Share Type</option><option value="edit">Edit</option><option value="view">View</option>';
          }
        }
      };
      xmlhttp.open("GET","/php/getmemberType.php?id="+id,true);
      xmlhttp.send();
    } else{
      document.getElementById("shareType").innerHTML='<option value="">Select Member First</option>';
    }
  });


  $(document).on('submit', '#play-request-form', function(e) {
    e.preventDefault();
    $('.shareButton').attr('disabled', 'disabled');
    $('.loader').removeClass('hide');
    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if(data.code == 0) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            $('.shareButton').removeAttr('disabled');
            $('#PlaybookModal').modal('hide');
            $('.loader').addClass('hide');
          }, 500);
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else if(data.code == 1) {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('.shareButton').removeAttr('disabled');
          $('.loader').addClass('hide');
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('.shareButton').removeAttr('disabled');
          $('.loader').addClass('hide');
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // Login  form validation
  $('#login-form').validate({
    messages: {
      username: 'please enter your username',
      password: 'please enter your password'
    }
  });
  $(document).on('submit', '#login-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          window.location.href = data.text;
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });

  // $(document).on('click', '.exporttBtn', function(e) {
  //     e.preventDefault();
  //
  //     var TabID   = $(this).data('tab');
  //     var company = $(this).data('company');
  //     var project = $(this).data('project');
  //     var dataString = 'company=' + company + '&project=' + project + '&tab=' + TabID;
  //
  //     $.ajax({
  //         url     : siteurl + 'php/ExportPDF',
  //         type    : 'POST',
  //         dataType: 'json',
  //         data    : dataString,
  //         success : function(data) {
  //             if(data.code == 0) {
  //                 window.open(data.pdf, '_blank');
  //                 //window.location.href = data.pdf;
  //             } else {
  //                 $.notify({
  //                     message: data.text
  //                 },{
  //                     type: 'danger'
  //                 });
  //             }
  //         },
  //         error   : function(xhr, err) {
  //             console.log(xhr, err);
  //         }
  //     });
  //     return false;
  // });
  //
  // $(document).on('click', '.exporttBtnDoc', function(e) {
  //     e.preventDefault();
  //
  //     var TabID   = $(this).data('tab');
  //     var company = $(this).data('company');
  //     var project = $(this).data('project');
  //     var dataString = 'company=' + company + '&project=' + project + '&tab=' + TabID;
  //
  //     $.ajax({
  //         url     : siteurl + 'php/ExportDOC',
  //         type    : 'POST',
  //         dataType: 'json',
  //         data    : dataString,
  //         success : function(data) {
  //             if(data.code == 0) {
  //                 window.open(data.doc, '_blank');
  //                 //window.location.href = data.doc;
  //             } else {
  //                 $.notify({
  //                     message: data.text
  //                 },{
  //                     type: 'danger'
  //                 });
  //             }
  //         },
  //         error   : function(xhr, err) {
  //             console.log(xhr, err);
  //         }
  //     });
  //     return false;
  // });


  // Forgot form validation
  $('#forgot-form').validate({
    messages: {
      username: 'please enter your username',
    }
  });
  $(document).on('submit', '#forgot-form', function(e) {
    e.preventDefault();

    $('#login').attr('disabled', 'disabled');
    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          $('#login').removeAttr('disabled');
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('#login').removeAttr('disabled');
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Playbook form validation
  $('#playbook-form').validate({
    rules: {
      project: {
        required: true,
        minlength: 2,
        regExp: "^[a-zA-Z1-9]"
      },
      department: {
        required: true
      },
      deadline: {
        required: true
      },
      goals: {
        required: true,
        minlength: 2,
        maxlength: 1200,
        regExp: "^[a-zA-Z1-9]"
      }
    },
    messages: {
      project: {
        required: 'Please enter project name',
        minlength: $.validator.format("Please enter at least {0} characters"),
        regExp: "No special characters or space allowed in beginning"
      },
      department: {
        required: 'Please choose department'
      },
      deadline: {
        required: 'Please enter deadline'
      },
      goals: {
        required: 'Please define goals',
        minlength: $.validator.format("Please enter at least {0} characters"),
        maxlength: $.validator.format("Please enter no more than {0} characters"),
        regExp: "No special characters or space allowed in beginning"
      }
    }
  });
  $(document).on('submit', '#playbook-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            window.location.href = data.url;
          }, 500);
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });



  // Project Creation validation
  $('#request-form').validate({
    messages: {
      memberId: 'please select member from list'
    }
  });
  $(document).on('submit', '#request-form', function(e) {
    e.preventDefault();
    $('.requestButton').attr('disabled', 'disabled');

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          $('#request-form')[0].reset();
          $('#RequestModal').modal('hide');
          $('.requestButton').removeAttr('disabled');
          $('.main-pane.active').find('.child-tab-link.active').click();
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
          $('.requestButton').removeAttr('disabled');
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Answer Reject validation
  $('#rejectAnswer-form').validate({
    messages: {
      rejectnote: 'please enter reject note',
    }
  });
  $(document).on('submit', '#rejectAnswer-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          $('#rejectAnswer-form')[0].reset();
          $('#AnswerRejectModal').modal('hide');
          $('.main-pane.active').find('.child-tab-link.active').click();
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Answer Accept validation
  $('#acceptAnswer-form').validate({
    messages: {
      rejectnote: 'please enter accept note',
    }
  });
  $(document).on('submit', '#acceptAnswer-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          $('#acceptAnswer-form')[0].reset();
          $('#AnswerAcceptModal').modal('hide');
          $('.main-pane.active').find('.child-tab-link.active').click();
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Change Password validation
  $('#change-password-form').validate({
    rules: {
      old_password: {
        required: true
      },
      new_password: {
        required: true,
        minlength: 6,
        maxlength: 20
      },
      con_password: {
        required: true,
        minlength: 6,
        maxlength: 20,
        equalTo: "#new_password"
      }
    },
    messages: {
      old_password: 'Please enter Old Password',
      new_password: {
        required: 'Please enter New Password',
        minlength: $.validator.format("Please enter at least {0} characters"),
        maxlength: $.validator.format("Please enter no more than {0} characters"),
      },
      con_password: {
        required: 'Please enter Confirm Password',
        minlength: $.validator.format("Please enter at least {0} characters"),
        maxlength: $.validator.format("Please enter no more than {0} characters"),
        equalTo: 'New & Confirm Password missmatch'
      }
    }
  });
  $(document).on('submit', '#change-password-form', function(e) {
    e.preventDefault();

    $.ajax({
      url     : $(this).attr('action'),
      type    : $(this).attr('method'),
      dataType: 'json',
      data    : $(this).serialize(),
      success : function(data) {
        if ( data.code == 0 ) {
          $.notify({
            message: data.text
          },{
            type: 'success'
          });
          setTimeout(function() {
            window.location.reload();
          }, 1000);
        } else {
          $.notify({
            message: data.text
          },{
            type: 'danger'
          });
        }
      },
      error   : function(xhr, err) {
        console.log(xhr, err);
      }
    });
    return false;
  });


  // Sidebar Functionality
  $('#toggle-btn').on('click', function(e) {
    e.preventDefault();

    $(this).toggleClass('active');

    $('.side-navbar').toggleClass('shrinked');
    $('.content-inner').toggleClass('active');

    if ( $(window).outerWidth() > 1183 ) {
      if ($('#toggle-btn').hasClass('active')) {
        $('.navbar-header .brand-small').hide();
        $('.navbar-header .brand-big').show();
      } else {
        $('.navbar-header .brand-small').show();
        $('.navbar-header .brand-big').hide();
      }
    }

    if ( $(window).outerWidth() < 1183 ) {
      $('.navbar-header .brand-small').show();
    }
  });


  // Transition Placeholders
  $('input.input-material').on('focus', function() {
    $(this).siblings('.label-material').addClass('active');
  });
  $('input.input-material').on('blur', function() {
    $(this).siblings('.label-material').removeClass('active');

    if ( $(this).val() !== '' ) {
      $(this).siblings('.label-material').addClass('active');
    } else {
      $(this).siblings('.label-material').removeClass('active');
    }
  });


  // External links to new window
  $('.external').on('click', function(e) {
    e.preventDefault();

    window.open($(this).attr("href"));
  });


  $('.tab_answer').each(function(index, elem) {

    var formID = $(this).attr('id').replace(/\s/g, '-').replace(/,/g, '').replace(/\?/g, '');

    $(document).on('submit', '#' + formID, function(e) {
      e.preventDefault();

      var req = 0;
      var $fields = $('#' + formID).find('.form-control');
      $fields.each(function(e, i) {
        if($(this).val() == false) {
          req = 1;
          $(this).addClass('has-error');
        } else {
          $(this).removeClass('has-error');
        }
      });

      var $fields = $('#' + formID).find('input[type="radio"]');
      $fields.each(function(e, i) {
        var name = $(this).attr('name');
        if($('input[name="' + name + '"]:checked').length == 0){
          req = 1;
          $(this).addClass('has-error');
        } else {
          $(this).removeClass('has-error');
        }
      });

      if(req) {
        $.notify({
          message: 'Fill the information before submit.'
        },{
          type: 'danger'
        });
        return false;
      }

      $('.loader').removeClass('hide');
      var submitBtn = $('#' + formID).find('button[type="submit"]');
      submitBtn.attr('disabled', 'disabled');

      var nextChild = $('.main-pane.active').find('.child-pane.active').next('.child-pane').length;
      if(nextChild) {
        var projectWrapper = $('#' + formID).parent().next('.child-pane').find('.projectWrapper');
      } else {
        var projectWrapper = $('#' + formID).parents('.main-pane.active').next('.main-pane').find('.child-pane:first').find('.projectWrapper');
      }

      var parent         = $('#' + formID).parents('.main-pane.active').attr('id');
      var nextParent     = $('#' + parent).next('.main-pane').attr('id');
      var formCount      = $('#' + parent).find('form').length;

      $.ajax({
        url     : $('#' + formID).attr('action'),
        type    : $('#' + formID).attr('method'),
        dataType: 'json',
        data    : $('#' + formID).serialize(),
        success : function(data) {
          if(data.code == 0) {
            $.notify({
              message: data.text
            },{
              type: 'success'
            });

            if((index+1) == $('.tab_answer').length) {
              window.location.href = siteurl+'playbooks';
            }

            if(nextChild) {
              nextTab();
            } else {
              $('.nav-wizard li a[href="#'+nextParent+'"]').click();
            }
            submitBtn.removeAttr('disabled');
          } else {
            $.notify({
              message: data.text
            },{
              type: 'danger'
            });
            $('.loader').addClass('hide');
          }
        },
        error   : function(xhr, err) {
          console.log(xhr,err);
        }
      });
      return false;
    });
  });

  $(document).on('click', '.main-tab-link', function(e){
    e.preventDefault();

    $('.loader').removeClass('hide');
    var projectId = $('#projectId').val();
    var tabId = $(this).data('tabid');
    var headId = $('.main-pane.active').find('.child-tab-link.active').data('headid');
    var last ='';
    loadTabContent(tabId, headId, projectId, last);
  });

  $(document).on('click', '.child-tab-link', function(e){
    e.preventDefault();

    $('.loader').removeClass('hide');
    var projectId = $('#projectId').val();
    var tabId = $('.main-tab-link.active').data('tabid');
    var headId = $(this).data('headid');
    var last = $(this).data('lastitemcheck');
    loadTabContent(tabId, headId, projectId, last);
  });

  $(document).on('click', '.videoSkipBtn', function(e) {
    e.preventDefault();

    $(this).parents('.main-pane').find('.headingTabs').find('li:first-child a').click();
  });

  $(function () {
    $('[data-toggle="popover"]').popover({
      trigger: 'hover'
    })
  });
});

if($('.list').length) {
  var hidWidth;
  var scrollBarWidths = 25;

  var widthOfList = function(){
    var itemsWidth = 0;
    $('.list li').each(function() {
      var itemWidth = $(this).outerWidth();
      itemsWidth+=itemWidth;
    });
    return itemsWidth;
  };

  var widthOfHidden = function(){
    return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
  };

  var getLeftPosi = function(){
    return $('.list').position().left;
  };

  var reAdjust = function(){
    if (($('.wrapper').outerWidth()) < widthOfList()) {
      $('.scroller-right').show();
    } else {
      $('.scroller-right').hide();
    }

    if (getLeftPosi()<0) {
      $('.scroller-left').show();
    } else {
      $('.item').animate({left:"-="+getLeftPosi()+"px"},'slow');
      $('.scroller-left').hide();
    }
  }

  reAdjust();

  $(window).on('resize',function(e){
    reAdjust();
  });

  $(document).on('click', '.scroller-right', function() {
    $('.scroller-left').fadeIn('slow');
    $('.scroller-right').fadeOut('slow');

    $('.list').animate({left:"+="+widthOfHidden()+"px"},'slow',function(){

    });
  });

  $(document).on('click', '.scroller-left', function() {
    $('.scroller-right').fadeIn('slow');
    $('.scroller-left').fadeOut('slow');

    $('.list').animate({left:"-="+getLeftPosi()+"px"},'slow',function(){

    });
  });
}

function loadTabContent(tabId, headId, projectId, last){
  $.ajax({
    url     : siteurl + 'php/loadTabContent',
    type    : 'POST',
    data    : {
      tabId: tabId,
      headId: headId,
      projectId: projectId
    },
    success : function(data) {
      $('.main-pane.active').find('.child-pane.active').find('.projectWrapper').html(data);
      $('.nano').nanoScroller({ alwaysVisible: true });
      $('.loader').addClass('hide');
      if(last != ''){
        $(".exporttBtnDoc").css("display", "unset");
        $(".exporttBtn").css("display", "unset");
      }
    }
  });
}

function setQuestionID(qtnID) {
  $('#QtnID').val(qtnID);
}

function setAnswerID(ansID) {
  $('#AnsID').val(ansID);
}
function setAcceptAnswerID(ansID) {
  $('#AnsId').val(ansID);
}

function nextTab() {
  $('#QuestionBlock').find('.main-pane.active').find('.mainCard').find('.nav-link.active').parent('li').next('li').find('a').trigger('click');
}

function prevTab() {
  if($('#QuestionBlock').find('.main-pane.active').find('.mainCard').find('.nav-link.active').parent('li').prev('li').hasClass('nav-item')){
    $('#QuestionBlock').find('.main-pane.active').find('.mainCard').find('.nav-link.active').parent('li').prev('li').find('a').trigger('click');
  } else {
    $('#QuestionBlock').find('.nav-wizard').find('.nav-link.active').parent('li').prev('li').find('a').trigger('click');
  }
}
