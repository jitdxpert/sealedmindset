<?php
require(dirname(__FILE__) . '/php/config.php');

if(!isset($_SESSION['UserLoggedIn']['Company_ID']) && !isset($_SESSION['UserLoggedIn']['User_ID'])) {
  redirect(BASE_URL);
}

$page = 'Playbooks';
require(dirname(__FILE__) . '/inc/html-header.php');
?>

<div class="page">
  <?php require(dirname(__FILE__) . '/inc/header.php'); ?>
  <div class="page-content d-flex align-items-stretch">
    <?php
      require(dirname(__FILE__) . '/inc/navbar.php');
      if(isset($_GET["q"])){
        $q = $_GET["q"];
      } else {
        $q= '';
      }
    ?>
    <div class="content-inner">
      <header class="page-header">
        <div class="container-fluid">
          <h2 class="no-margin-bottom">
            Playbooks
          </h2>
        </div>
      </header>
      <?php if ($_SESSION['UserLoggedIn']['User_Type'] == 'individual') { ?>
        <section class="playbooks">
          <div class="container-fluid">
            <?php
            if($_SESSION['UserLoggedIn']['User_Type'] == 'individual') {
              $projectIds = DB::table('share')
              ->where('User_ID', $_SESSION['UserLoggedIn']['User_ID'])
              ->pluck('Project_ID');

              $ProjectSQL = DB::table('projects')
              ->whereIn('Project_ID', $projectIds)
              ->where(function($searchquery) use($q){
                $searchquery->where('Project_Name', 'like', '%'.$q.'%');
              })
              ->count();

              if(!empty($ProjectSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                if($q){
                  $targetpage = 'request?q='.$q;
                } else {
                  $targetpage = 'request';
                }
                $total_pages = $ProjectSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                $ProjectFetchSQL = DB::table('projects')
                ->whereIn('Project_ID', $projectIds)
                ->where(function($searchquery) use($q){
                  $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                })
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($ProjectFetchSQL as $Project) { ?>
                  <div class="parent">
                    <div class="child-row">
                      <div class="row">
                        <div class="col-md-9">
                          <div class="articles card left">
                            <div class="card-close">
                              <?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?>
                            </div>
                            <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                              <?php echo GetSharedUser($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="articles card right">
                            <div class="card-body no-padding">
                              <?php echo GetAllTabFull($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:60px"> <?=$pagination?></div>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No projects are available.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            } else {
              $RequestSQL = DB::table('requests')
              ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
              ->where('User_ID', $_SESSION['UserLoggedIn']['User_ID'])
              ->groupBy('Project_ID')
              ->get();
              if(!empty($RequestSQL)) {
                foreach($RequestSQL as $RequestData) {
                  $ProjectSQL = DB::table('projects')
                  ->where('Project_ID', $RequestData->Project_ID)
                  ->where('Company_ID', $_SESSION['UserLoggedIn']['Company_ID'])
                  ->get();
                  if(!empty($ProjectSQL)) {
                    foreach($ProjectSQL as $Project) { ?>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="articles card">
                            <div class="card-close"><?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?></div>
                            <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                            </div>
                            <div class="item d-flex">
                              <button class="view-playbook" data-url="<?php echo BASE_URL; ?>shared?project=<?php echo safe_b64encode($Project->Project_ID); ?>">View Playbook</button>
                              <!-- <button class="view-playbook" data-url="<?php echo BASE_URL; ?>project/<?php echo safe_b64encode($Project->Project_ID); ?>">View Playbook</button> -->
                            </div>
                          </div>
                        </div>

                      </div>
                      <?php
                    }
                  } else { ?>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">No projects are available.</div>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                }
              } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No projects are available.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
            ?>
          </div>
        </section>
      <?php } else { ?>
        <section class="playbooks">
          <div class="container-fluid">
            <?php
            if($_SESSION['UserLoggedIn']['User_Type'] == 'department'||$_SESSION['UserLoggedIn']['User_Type'] == 'user'||$_SESSION['UserLoggedIn']['User_Type'] == 'request') {
              $projectIds = DB::table('share')
              ->where('User_ID', $_SESSION['UserLoggedIn']['User_ID'])
              ->pluck('Project_ID');

              $ProjectSQL = DB::table('projects')
              ->whereIn('Project_ID', $projectIds)
              ->where(function($searchquery) use($q){
                $searchquery->where('Project_Name', 'like', '%'.$q.'%');
              })
              ->count();
              if(!empty($ProjectSQL)) {
                // Pagination
                $paginationObj = new Pagination;
                if($q){
                  $targetpage = 'request?q='.$q;
                } else {
                  $targetpage = 'request';
                }
                $total_pages = $ProjectSQL;
                $limit = 10;
                $paginationOutput = $paginationObj->paginate($targetpage, $total_pages, $limit);
                $pagination = $paginationOutput['Pagination'];
                $start = $paginationOutput['Start'];
                // Pagination
                $ProjectFetchSQL = DB::table('projects')
                ->whereIn('Project_ID', $projectIds)
                ->where(function($searchquery) use($q){
                  $searchquery->where('Project_Name', 'like', '%'.$q.'%');
                })
                ->offset($start)
                ->limit($limit)
                ->get();
                foreach($ProjectFetchSQL as $Project) { ?>
                  <div class="parent">
                    <div class="child-row">
                      <div class="row">
                        <div class="col-md-9">
                          <div class="articles card left">
                            <div class="card-close">
                              <?php echo date('l, jS F Y \a\t g:i A', strtotime($Project->Project_CreatedOn)); ?>
                            </div>
                            <div class="card-post"><?php echo stripslashes($Project->Project_Department); ?></div>
                            <div class="card-header d-flex">
                              <h2 class="h3"><?php echo stripslashes($Project->Project_Name); ?></h2>
                            </div>
                            <div class="card-body no-padding">
                              <div class="item d-flex project-goal">
                                <div class="text">
                                  <p><?php echo stripslashes($Project->Project_Goals); ?></p>
                                </div>
                              </div>
                              <?php echo GetSharedUser($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="articles card right">
                            <div class="card-body no-padding">
                              <?php echo GetAllTabFull($Project->Project_ID); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                <!-- ........................PAGINATION.............................. -->
                <div style="margin-top:60px"> <?=$pagination?></div>
                <!-- ........................PAGINATION.............................. -->
              <?php } else { ?>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-body">No projects are available.</div>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
            ?>
          </div>
        </section>
      <?php } ?>

      <?php require(dirname(__FILE__) . '/inc/footer.php'); ?>
    </div>
  </div>
</div>

<?php require(dirname(__FILE__) . '/inc/html-footer.php'); ?>
